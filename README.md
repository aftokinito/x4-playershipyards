# Player Owned Ship Production #

Player Owned Ship Production (aka Afto's Player Shipyards) is a mod that allows players to build their own Shipyards, Wharves and Equipment Docks using the same tools already available in the game, namely the station and ship configurator UIs.
In order to be able to build the required station modules, players must first research the Build Module tech at their HQ and then scan the AI's build modules (XL/L/M/S Fabrication/Maintenance Bays) to get the required blueprints.

After acquiring the necessary blueprints, players will be able to build those modules at any of their stations EXCEPT for the HQ (this is already enforced in vanilla since it breaks the HQ's map icon otherwise).
Once the build modules have been built, a shiptrader NPC will automatically be added to the station (might take a few minutes) that will enable the ship building/upgrading/repairing functionality as well as changing the station's icon to the appropriate one.

The ship configurator UI has been modified to fix the issue that Egosoft encountered when dealing with player owned shipyards (GetContainerBuilderMacros returning garbage because the player faction has no licenses) and players can now build any ship they have a blueprint for.
At the start of the game, the player won't have any ship blueprint learned and, therefore, the player won't be able to build any ship in their own facilities.
Players can acquire new ship blueprints in the usual way, by talking to the Faction Representative and purchasing the corresponding blueprint from the list of the available ones.
Some blueprints require the player to have certain license but these requires are shared with those of the regular ship building mechanic so, as a rule of thumb, if the player can build certain ship from the corresponding faction's facilities, the blueprint of that ship should also be available for purchase.

### Installation ###

1. Download the latest version from the [downloads page](https://bitbucket.org/aftokinito/x4-playershipyards/downloads/playershipyards.zip) 
2. Extract the contents of the ZIP file into the extensions folder of your game's ROOT  
(example: ```C:\Program Files (x86)\Steam\steamapps\common\X4 Foundations\extensions\```)
3. Optionally, install any of the compatible addons.

### Addons ###

* [[MOD] Mobile Ship Production](https://forum.egosoft.com/viewtopic.php?&t=409289)
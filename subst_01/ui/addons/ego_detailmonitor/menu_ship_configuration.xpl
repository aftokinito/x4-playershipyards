﻿-- section == cArch_configureships
-- param == { 0, 0, container, mode, modeparam, immediate }

-- modes:	"purchase",	param:	{}
--			"upgrade",			{ selectableships }
--			"modify",			{ [ paintonly = false, selectedships = {} ] }

-- ffi setup
local ffi = require("ffi")
local C = ffi.C
ffi.cdef[[
	typedef uint64_t BuildTaskID;
	typedef uint64_t NPCSeed;
	typedef uint64_t UniverseID;
	typedef struct {
		const char* macro;
		const char* ware;
		uint32_t amount;
		uint32_t capacity;
	} AmmoData;
	typedef struct {
		BuildTaskID id;
		UniverseID buildingcontainer;
		UniverseID component;
		const char* macro;
		const char* factionid;
		UniverseID buildercomponent;
		int64_t price;
		bool ismissingresources;
		uint32_t queueposition;
	} BuildTaskInfo;
	typedef struct {
		const char* type;
		const char* ware;
		const char* macro;
		int amount;
	} EquipmentWareInfo;
	typedef struct {
		const char* PropertyType;
		float MinValueFloat;
		float MaxValueFloat;
		uint32_t MinValueUINT;
		uint32_t MaxValueUINT;
		uint32_t BonusMax;
		float BonusChance;
	} EquipmentModInfo;
	typedef struct {
		const char* name;
		const char* icon;
	} LicenceInfo;
	typedef struct {
		size_t queueidx;
		const char* state;
		const char* statename;
		const char* orderdef;
		size_t actualparams;
		bool enabled;
		bool isinfinite;
		bool issyncpointreached;
		bool istemporder;
	} Order;
	typedef struct {
		const char* id;
		const char* name;
		const char* desc;
		uint32_t amount;
		uint32_t numtiers;
		bool canhire;
	} PeopleInfo;
	typedef struct {
		const char* name;
		int32_t skilllevel;
		uint32_t amount;
	} RoleTierData;
	typedef struct {
		UniverseID context;
		const char* group;
		UniverseID component;
	} ShieldGroup;
	typedef struct {
		const char* max;
		const char* current;
	} SoftwareSlot;
	typedef struct {
		const char* macro;
		const char* ware;
		const char* productionmethodid;
	} UIBlueprint;
	typedef struct {
		const char* Name;
		const char* RawName;
		const char* Ware;
		uint32_t Quality;
		const char* PropertyType;
		float ForwardThrustFactor;
		float StrafeThrustFactor;
		float RotationThrustFactor;
		float BoostThrustFactor;
		float BoostDurationFactor;
		float BoostAttackTimeFactor;
		float BoostReleaseTimeFactor;
		float BoostChargeTimeFactor;
		float BoostRechargeTimeFactor;
		float TravelThrustFactor;
		float TravelStartThrustFactor;
		float TravelAttackTimeFactor;
		float TravelReleaseTimeFactor;
		float TravelChargeTimeFactor;
	} UIEngineMod;
	typedef struct {
		const char* Name;
		const char* RawName;
		const char* Ware;
		uint32_t Quality;
	} UIEquipmentMod;
	typedef struct {
		const char* macro;
		uint32_t amount;
		bool optional;
	} UILoadoutAmmoData;
	typedef struct {
		const char* macro;
		const char* path;
		const char* group;
		uint32_t count;
		bool optional;
	} UILoadoutGroupData;
	typedef struct {
		const char* macro;
		const char* upgradetypename;
		size_t slot;
		bool optional;
	} UILoadoutMacroData;
	typedef struct {
		const char* ware;
	} UILoadoutSoftwareData;
	typedef struct {
		const char* macro;
		bool optional;
	} UILoadoutVirtualMacroData;
	typedef struct {
		uint32_t numweapons;
		uint32_t numturrets;
		uint32_t numshields;
		uint32_t numengines;
		uint32_t numturretgroups;
		uint32_t numshieldgroups;
		uint32_t numammo;
		uint32_t numunits;
		uint32_t numsoftware;
	} UILoadoutCounts;
	typedef struct {
		UILoadoutMacroData* weapons;
		uint32_t numweapons;
		UILoadoutMacroData* turrets;
		uint32_t numturrets;
		UILoadoutMacroData* shields;
		uint32_t numshields;
		UILoadoutMacroData* engines;
		uint32_t numengines;
		UILoadoutGroupData* turretgroups;
		uint32_t numturretgroups;
		UILoadoutGroupData* shieldgroups;
		uint32_t numshieldgroups;
		UILoadoutAmmoData* ammo;
		uint32_t numammo;
		UILoadoutAmmoData* units;
		uint32_t numunits;
		UILoadoutSoftwareData* software;
		uint32_t numsoftware;
		UILoadoutVirtualMacroData thruster;
	} UILoadout;
	typedef struct {
		const char* id;
		const char* name;
		const char* iconid;
		bool deleteable;
	} UILoadoutInfo;
	typedef struct {
		const char* upgradetype;
		size_t slot;
	} UILoadoutSlot;
	typedef struct {
		float HullValue;
		float ShieldValue;
		float BurstDPS;
		float SustainedDPS;
		float ForwardSpeed;
		float BoostSpeed;
		float TravelSpeed;
		float YawSpeed;
		float PitchSpeed;
		float RollSpeed;
		float ForwardAcceleration;
		uint32_t NumDocksShipMedium;
		uint32_t NumDocksShipSmall;
		uint32_t ShipCapacityMedium;
		uint32_t ShipCapacitySmall;
		uint32_t CrewCapacity;
		uint32_t ContainerCapacity;
		uint32_t SolidCapacity;
		uint32_t LiquidCapacity;
		uint32_t UnitCapacity;
		uint32_t MissileCapacity;
		uint32_t CountermeasureCapacity;
		uint32_t DeployableCapacity;
		float RadarRange;
	} UILoadoutStatistics;
	typedef struct {
		const char* Name;
		const char* RawName;
		const char* Ware;
		uint32_t Quality;
		uint32_t Amount;
	} UIPaintMod;
	typedef struct {
		const char* Name;
		const char* RawName;
		const char* Ware;
		uint32_t Quality;
		const char* PropertyType;
		float CapacityFactor;
		float RechargeDelayFactor;
		float RechargeRateFactor;
	} UIShieldMod;
	typedef struct {
		const char* Name;
		const char* RawName;
		const char* Ware;
		uint32_t Quality;
		const char* PropertyType;
		float MassFactor;
		float DragFactor;
		float MaxHullFactor;
		float RadarRangeFactor;
		uint32_t AddedUnitCapacity;
		uint32_t AddedMissileCapacity;
		uint32_t AddedCountermeasureCapacity;
		uint32_t AddedDeployableCapacity;
	} UIShipMod;
	typedef struct {
		const char* ware;
		const char* macro;
		int amount;
	} UIWareInfo;
	typedef struct {
		const char* Name;
		const char* RawName;
		const char* Ware;
		uint32_t Quality;
		const char* PropertyType;
		float DamageFactor;
		float CoolingFactor;
		float ReloadFactor;
		float SpeedFactor;
		float LifeTimeFactor;
		float MiningFactor;
		float StickTimeFactor;
		float ChargeTimeFactor;
		float BeamLengthFactor;
		uint32_t AddedAmount;
		float RotationSpeedFactor;
	} UIWeaponMod;
	typedef struct {
		const char* macro;
		const char* category;
		uint32_t amount;
	} UnitData;
	typedef struct {
		const char* path;
		const char* group;
	} UpgradeGroup;
	typedef struct {
		UniverseID currentcomponent;
		const char* currentmacro;
		const char* slotsize;
		uint32_t count;
		uint32_t operational;
		uint32_t total;
	} UpgradeGroupInfo;
	typedef struct {
		uint32_t current;
		uint32_t capacity;
		uint32_t optimal;
		uint32_t available;
		uint32_t maxavailable;
		double timeuntilnextupdate;
	} WorkForceInfo;
	typedef struct {
		UniverseID shipid;
		const char* macroname;
		UILoadout loadout;
		uint32_t amount;
	} UIBuildOrderList;
	void AddBuildTask(UniverseID containerid, UniverseID defensibleid, const char* macroname, UILoadout uiloadout, int64_t price, CrewTransferInfo crewtransfer, bool immediate);
	bool CanBuildLoadout(UniverseID containerid, UniverseID defensibleid, const char* macroname, const char* loadoutid);
	bool CanContainerEquipShip(UniverseID containerid, UniverseID shipid);
	bool CheckWeaponModCompatibility(UniverseID weaponid, const char* wareid);
	uint32_t CreateOrder(UniverseID controllableid, const char* orderid, bool defaultorder);
	void ClearMapBehaviour(UniverseID holomapid);
	void ClearSelectedMapMacroSlots(UniverseID holomapid);
	void DismantleEngineMod(UniverseID objectid);
	void DismantleShieldMod(UniverseID defensibleid, UniverseID contextid, const char* group);
	void DismantleShipMod(UniverseID shipid);
	void DismantleWeaponMod(UniverseID weaponid);
	bool EnableOrder(UniverseID controllableid, size_t idx);
	EquipmentModInfo GetEquipmentModInfo(const char* wareid);
	void GenerateShipLoadout(UILoadout* result, UniverseID containerid, UniverseID shipid, const char* macroname, float level);
	void GenerateShipLoadoutCounts(UILoadoutCounts* result, UniverseID containerid, UniverseID shipid, const char* macroname, float level);
	uint32_t GetAllCountermeasures(AmmoData* result, uint32_t resultlen, UniverseID defensibleid);
	uint32_t GetAllLaserTowers(AmmoData* result, uint32_t resultlen, UniverseID defensibleid);
	uint32_t GetAllMines(AmmoData* result, uint32_t resultlen, UniverseID defensibleid);
	uint32_t GetAllMissiles(AmmoData* result, uint32_t resultlen, UniverseID defensibleid);
	uint32_t GetAllRoles(PeopleInfo* result, uint32_t resultlen);
	uint32_t GetAllNavBeacons(AmmoData* result, uint32_t resultlen, UniverseID defensibleid);
	uint32_t GetAllResourceProbes(AmmoData* result, uint32_t resultlen, UniverseID defensibleid);
	uint32_t GetAllSatellites(AmmoData* result, uint32_t resultlen, UniverseID defensibleid);
	uint32_t GetAllUnits(UnitData* result, uint32_t resultlen, UniverseID defensibleid, bool onlydrones);
	uint32_t GetAvailableEquipment(EquipmentWareInfo* result, uint32_t resultlen, UniverseID containerid, const char* classid);
	uint32_t GetAvailableEquipmentMods(UIEquipmentMod* result, uint32_t resultlen);
	uint32_t GetBlueprints(UIBlueprint* result, uint32_t resultlen, const char* set, const char* category, const char* macroname);
	double GetBuildDuration(UniverseID containerid, UIBuildOrderList order);
	uint32_t GetBuildTasks(BuildTaskInfo* result, uint32_t resultlen, UniverseID containerid, bool isinprogress, bool includeupgrade);
	int64_t GetBuildWarePrice(UniverseID containerid, const char* warename);
	const char* GetComponentClass(UniverseID componentid);
	const char* GetComponentName(UniverseID componentid);
	uint32_t GetContainerBuilderMacros(const char** result, uint32_t resultlen, UniverseID containerid);
	UILoadoutStatistics GetCurrentLoadoutStatistics(UniverseID shipid);
	uint32_t GetDamagedSubComponents(UniverseID* result, uint32_t resultlen, UniverseID objectid);
	uint32_t GetDefaultMissileStorageCapacity(const char* macroname);
	uint32_t GetDefaultCountermeasureStorageCapacity(const char* macroname);
	uint32_t GetDefensibleDeployableCapacity(UniverseID defensibleid);
	uint32_t GetDockedShips(UniverseID* result, uint32_t resultlen, UniverseID dockingbayorcontainerid, const char* factionid);
	bool GetInstalledEngineMod(UniverseID objectid, UIEngineMod* enginemod);
	bool GetInstalledPaintMod(UniverseID objectid, UIPaintMod* paintmod);
	bool GetInstalledShieldMod(UniverseID defensibleid, UniverseID contextid, const char* group, UIShieldMod* shieldmod);
	bool GetInstalledWeaponMod(UniverseID weaponid, UIWeaponMod* weaponmod);
	uint32_t GetInventoryPaintMods(UIPaintMod* result, uint32_t resultlen);
	bool GetLicenceInfo(LicenceInfo* result, const char* factionid, const char* licenceid);
	void GetLoadout(UILoadout* result, UniverseID defensibleid, const char* macroname, const char* loadoutid);
	uint32_t GetLoadoutCounts(UILoadoutCounts* result, UniverseID defensibleid, const char* macroname, const char* loadoutid);
	uint32_t GetLoadoutsInfo(UILoadoutInfo* result, uint32_t resultlen, UniverseID componentid, const char* macroname);
	UILoadoutStatistics GetLoadoutStatistics(UniverseID shipid, const char* macroname, UILoadout uiloadout);
	const char* GetLocalizedText(const uint32_t pageid, uint32_t textid, const char*const defaultvalue);
	const char* GetMacroClass(const char* macroname);
	uint32_t GetMacroDeployableCapacity(const char* macroname);
	uint32_t GetMacroMissileCapacity(const char* macroname);
	UILoadoutStatistics GetMaxLoadoutStatistics(UniverseID shipid, const char* macroname);
	uint32_t GetMissileCargo(UIWareInfo* result, uint32_t resultlen, UniverseID containerid);
	uint32_t GetMissingBuildResources(UIWareInfo* result, uint32_t resultlen);
	uint32_t GetMissingLoadoutResources(UIWareInfo* result, uint32_t resultlen);
	uint32_t GetNumAllCountermeasures(UniverseID defensibleid);
	uint32_t GetNumAllLaserTowers(UniverseID defensibleid);
	uint32_t GetNumAllMines(UniverseID defensibleid);
	uint32_t GetNumAllMissiles(UniverseID defensibleid);
	uint32_t GetNumAllRoles(void);
	uint32_t GetNumAllNavBeacons(UniverseID defensibleid);
	uint32_t GetNumAllResourceProbes(UniverseID defensibleid);
	uint32_t GetNumAllSatellites(UniverseID defensibleid);
	uint32_t GetNumAllUnits(UniverseID defensibleid, bool onlydrones);
	uint32_t GetNumAvailableEquipment(UniverseID containerid, const char* classid);
	uint32_t GetNumAvailableEquipmentMods();
	uint32_t GetNumBuildTasks(UniverseID containerid, bool isinprogress, bool includeupgrade);
	uint32_t GetNumContainerBuilderMacros(UniverseID containerid);
	uint32_t GetNumDockedShips(UniverseID dockingbayorcontainerid, const char* factionid);
	uint32_t GetNumInventoryPaintMods(void);
	uint32_t GetNumLoadoutsInfo(UniverseID componentid, const char* macroname);
	uint32_t GetNumMissileCargo(UniverseID containerid);
	uint32_t GetNumMissingBuildResources(UniverseID containerid, UIBuildOrderList* orders, uint32_t numorders);
	uint32_t GetNumMissingLoadoutResources(UniverseID containerid, UIBuildOrderList* orders, uint32_t numorders);
	uint32_t GetNumShieldGroups(UniverseID defensibleid);
	uint32_t GetNumSoftwarePredecessors(const char* softwarename);
	uint32_t GetNumSoftwareSlots(UniverseID controllableid, const char* macroname);
	uint32_t GetNumSubComponents(UniverseID containerid);
	uint32_t GetNumSuitableBuildProcessors(UniverseID containerid, UniverseID objectid, const char* macroname);
	uint32_t GetNumUnitCargo(UniverseID containerid, bool onlydrones);
	size_t GetNumUpgradeSlots(UniverseID destructibleid, const char* macroname, const char* upgradetypename);
	size_t GetNumVirtualUpgradeSlots(UniverseID objectid, const char* macroname, const char* upgradetypename);
	const char* GetObjectIDCode(UniverseID objectid);
	uint32_t GetPeople(PeopleInfo* result, uint32_t resultlen, UniverseID controllableid);
	uint32_t GetPeopleCapacity(UniverseID controllableid, const char* macroname, bool includecrew);
	bool GetPickedMapMacroSlot(UniverseID holomapid, UniverseID defensibleid, UniverseID moduleid, const char* macroname, bool ismodule, UILoadoutSlot* result);
	UniverseID GetPlayerOccupiedShipID(void);
	const char* GetPlayerPaintTheme(void);
	bool GetPlayerPaintThemeMod(UniverseID objectid, const char* macroname, UIPaintMod* paintmod);
	uint32_t GetPurchasableCargo(UniverseID containerid, const char*);
	int64_t GetRepairPrice(UniverseID componenttorepairid, UniverseID containerid);
	uint32_t GetRoleTierNPCs(NPCSeed* result, uint32_t resultlen, UniverseID controllableid, const char* role, int32_t skilllevel);
	uint32_t GetRoleTiers(RoleTierData* result, uint32_t resultlen, UniverseID controllableid, const char* role);
	bool GetShieldGroup(ShieldGroup* result, UniverseID defensibleid, UniverseID destructibleid);
	uint32_t GetShieldGroups(ShieldGroup* result, uint32_t resultlen, UniverseID defensibleid);
	const char* GetSlotSize(UniverseID defensibleid, UniverseID moduleid, const char* macroname, bool ismodule, const char* upgradetypename, size_t slot);
	const char* GetSoftwareMaxCompatibleVersion(UniverseID controllableid, const char* macroname, const char* softwarename);
	uint32_t GetSoftwarePredecessors(const char** result, uint32_t resultlen, const char* softwarename);
	uint32_t GetSoftwareSlots(SoftwareSlot* result, uint32_t resultlen, UniverseID controllableid, const char* macroname);
	uint32_t GetUnitCargo(UIWareInfo* result, uint32_t resultlen, UniverseID containerid, bool onlydrones);
	UpgradeGroupInfo GetUpgradeGroupInfo(UniverseID destructibleid, const char* macroname, const char* path, const char* group, const char* upgradetypename);
	const char* GetUpgradeSlotCurrentMacro(UniverseID objectid, UniverseID moduleid, const char* upgradetypename, size_t slot);
	UniverseID GetUpgradeSlotCurrentComponent(UniverseID destructibleid, const char* upgradetypename, size_t slot);
	UpgradeGroup GetUpgradeSlotGroup(UniverseID destructibleid, const char* macroname, const char* upgradetypename, size_t slot);
	const char* GetVirtualUpgradeSlotCurrentMacro(UniverseID defensibleid, const char* upgradetypename, size_t slot);
	WorkForceInfo GetWorkForceInfo(UniverseID containerid, const char* raceid);
	bool IsNextStartAnimationSkipped(bool reset);
	bool InstallEngineMod(UniverseID objectid, const char* wareid);
	bool InstallPaintMod(UniverseID objectid, const char* wareid, bool useinventory);
	bool InstallShieldMod(UniverseID defensibleid, UniverseID contextid, const char* group, const char* wareid);
	bool InstallShipMod(UniverseID shipid, const char* wareid);
	bool InstallWeaponMod(UniverseID weaponid, const char* wareid);
	bool IsAmmoMacroCompatible(const char* weaponmacroname, const char* ammomacroname);
	bool IsDeployableMacroCompatible(UniverseID containerid, const char* macroname, const char* deployablemacroname);
	bool IsSoftwareDefault(UniverseID controllableid, const char* macroname, const char* softwarename);
	bool IsUnitMacroCompatible(UniverseID containerid, const char* macroname, const char* unitmacroname);
	bool IsUpgradeMacroCompatible(UniverseID objectid, UniverseID moduleid, const char* macroname, bool ismodule, const char* upgradetypename, size_t slot, const char* upgrademacroname);
	bool IsVirtualUpgradeMacroCompatible(UniverseID defensibleid, const char* macroname, const char* upgradetypename, size_t slot, const char* upgrademacroname);
	bool RemoveLoadout(const char* source, const char* macroname, const char* localid);
	void SaveLoadout(const char* macroname, UILoadout uiloadout, const char* source, const char* id, bool overwrite, const char* name, const char* desc);
	void SetMapPaintMod(UniverseID holomapid, const char* wareid);
	void SetSelectedMapMacroSlot(UniverseID holomapid, UniverseID defensibleid, UniverseID moduleid, const char* macroname, bool ismodule, const char* upgradetypename, size_t slot);
	void ShowObjectConfigurationMap(UniverseID holomapid, UniverseID defensibleid, UniverseID moduleid, const char* macroname, bool ismodule, UILoadout uiloadout);
	void StartPanMap(UniverseID holomapid);
	void StartRotateMap(UniverseID holomapid);
	bool StopPanMap(UniverseID holomapid);
	bool StopRotateMap(UniverseID holomapid);
	void UpdateObjectConfigurationMap(UniverseID holomapid, UniverseID defensibleid, UniverseID moduleid, const char* macroname, bool ismodule, UILoadout uiloadout);
	uint32_t GetWares(const char** result, uint32_t resultlen, const char* tags, bool research, const char* licenceownerid, const char* exclusiontags);
	uint32_t GetNumWares(const char* tags, bool research, const char* licenceownerid, const char* exclusiontags);
]]

local utf8 = require("utf8")

local menu = {
	name = "ShipConfigurationMenu",
	currentIdx = 0,
	expandedModSlots = {},
	expandedUpgrades = {},
	captainSelected = true,
}

local config = {
	mainLayer = 5,
	infoLayer = 4,
	contextLayer = 3,
	classorder = { "ship_xl", "ship_l", "ship_m", "ship_s", "ship_xs" },
	leftBar = {
		{ name = ReadText(1001, 1103),	icon = "shipbuildst_engine",		mode = "engine",		iscapship = false },
		{ name = ReadText(1001, 8520),	icon = "shipbuildst_enginegroups",	mode = "enginegroup",	iscapship = true },
		{ name = ReadText(1001, 8001),	icon = "shipbuildst_thruster",		mode = "thruster" },
		{ name = ReadText(1001, 1317),	icon = "shipbuildst_shield",		mode = "shield" },
		{ name = ReadText(1001, 2663),	icon = "shipbuildst_weapon",		mode = "weapon" },
		{ name = ReadText(1001, 1319),	icon = "shipbuildst_turret",		mode = "turret",		iscapship = false },
		{ name = ReadText(1001, 7901),	icon = "shipbuildst_turretgroups",	mode = "turretgroup",	iscapship = true },
		{ name = ReadText(1001, 87),	icon = "shipbuildst_software",		mode = "software" },
		{ spacing = true },
		{ name = ReadText(1001, 8003),	icon = "shipbuildst_consumable",	mode = "consumables" },
		{ name = ReadText(1001, 80),	icon = "shipbuildst_crew",			mode = "crew" },
		{ spacing = true },
		{ name = ReadText(1001, 3000),	icon = "shipbuildst_repair",		mode = "repair" },
	},
	leftBarMods = {
		{ name = ReadText(1001, 8038),	icon = "shipbuildst_chassis",		mode = "shipmods",		upgrademode = "ship",	modclass = "ship" },
		{ name = ReadText(1001, 6600),	icon = "shipbuildst_weapon",		mode = "weaponmods",	upgrademode = "weapon",	modclass = "weapon" },
		{ name = ReadText(1001, 8004),	icon = "shipbuildst_turret",		mode = "turretmods",	upgrademode = "turret",	modclass = "weapon" },
		{ name = ReadText(1001, 8515),	icon = "shipbuildst_shield",		mode = "shieldmods",	upgrademode = "shield",	modclass = "shield" },
		{ name = ReadText(1001, 8028),	icon = "shipbuildst_engine",		mode = "enginemods",	upgrademode = "engine",	modclass = "engine" },
		{ name = ReadText(1001, 8510),	icon = "shipbuildst_paint",			mode = "paintmods",		upgrademode = "paint",	modclass = "paint" },
	},
	dropDownTextProperties = {
		halign = "center",
		font = Helper.standardFont,
		fontsize = Helper.scaleFont(Helper.standardFont, Helper.standardFontSize),
		color = Helper.color.white,
		x = 0,
		y = 0
	},
	stateKeys = {
		{ "object", "UniverseID" },
		{ "macro" },
		{ "class" },
		{ "upgradetypeMode" },
		{ "currentSlot" },
		{ "upgradeplan" },
		{ "crew" },
		{ "editingshoppinglist" },
		{ "searchtext" },
		{ "loadoutName" },
		{ "captainSelected", "bool" },
		{ "validLicence", "bool" },
	},
	dropdownRatios = {
		class = 0.7,
		ship = 1.3,
	},
	stats = {
		{ id = "HullValue",					name = ReadText(1001, 8048),	unit = ReadText(1001, 118),	type = "float",	accuracy = 0 },
		{ id = "ShieldValue",				name = ReadText(1001, 8049),	unit = ReadText(1001, 118),	type = "float",	accuracy = 0 },
		{ id = "RadarRange",				name = ReadText(1001, 8068),	unit = ReadText(1001, 108),	type = "float",	accuracy = 0 },
		{ id = "BurstDPS",					name = ReadText(1001, 8073),	unit = ReadText(1001, 119),	type = "float",	accuracy = 0 },
		{ id = "SustainedDPS",				name = ReadText(1001, 8074),	unit = ReadText(1001, 119),	type = "float",	accuracy = 0 },
		{ id = "" },
		{ id = "ForwardSpeed",				name = ReadText(1001, 8051),	unit = ReadText(1001, 113),	type = "float",	accuracy = 0 },
		{ id = "ForwardAcceleration",		name = ReadText(1001, 8069),	unit = ReadText(1001, 111),	type = "float",	accuracy = 0 },
		{ id = "BoostSpeed",				name = ReadText(1001, 8052),	unit = ReadText(1001, 113),	type = "float",	accuracy = 0 },
		{ id = "TravelSpeed",				name = ReadText(1001, 8053),	unit = ReadText(1001, 113),	type = "float",	accuracy = 0 },
		{ id = "YawSpeed",					name = ReadText(1001, 8054),	unit = ReadText(1001, 117),	type = "float",	accuracy = 1 },
		{ id = "PitchSpeed",				name = ReadText(1001, 8055),	unit = ReadText(1001, 117),	type = "float",	accuracy = 1 },
		{ id = "RollSpeed",					name = ReadText(1001, 8056),	unit = ReadText(1001, 117),	type = "float",	accuracy = 1 },
		{ id = "" },
		-- new column
		{ id = "ContainerCapacity",			name = ReadText(1001, 8058),	unit = ReadText(1001, 110),	type = "UINT",	accuracy = 0 },
		{ id = "SolidCapacity",				name = ReadText(1001, 8059),	unit = ReadText(1001, 110),	type = "UINT",	accuracy = 0 },
		{ id = "LiquidCapacity",			name = ReadText(1001, 8060),	unit = ReadText(1001, 110),	type = "UINT",	accuracy = 0 },
		{ id = "" },
		{ id = "NumDocksShipMedium",		name = ReadText(1001, 8524),	unit = "",					type = "UINT",	accuracy = 0 },
		{ id = "NumDocksShipSmall",			name = ReadText(1001, 8525),	unit = "",					type = "UINT",	accuracy = 0 },
		{ id = "ShipCapacityMedium",		name = ReadText(1001, 8526),	unit = "",					type = "UINT",	accuracy = 0 },
		{ id = "ShipCapacitySmall",			name = ReadText(1001, 8527),	unit = "",					type = "UINT",	accuracy = 0 },
		{ id = "" },
		{ id = "CrewCapacity",				name = ReadText(1001, 8057),	unit = "",					type = "UINT",	accuracy = 0 },
		{ id = "UnitCapacity",				name = ReadText(1001, 8061),	unit = "",					type = "UINT",	accuracy = 0 },
		{ id = "MissileCapacity",			name = ReadText(1001, 8062),	unit = "",					type = "UINT",	accuracy = 0 },
		{ id = "DeployableCapacity",		name = ReadText(1001, 8064),	unit = "",					type = "UINT",	accuracy = 0 },
		{ id = "CountermeasureCapacity",	name = ReadText(1001, 8063),	unit = "",					type = "UINT",	accuracy = 0 },
	},
	scaleSize = 2,
	deployableOrder = {
		["satellite"]		= 1,
		["navbeacon"]		= 2,
		["resourceprobe"]	= 3,
		["lasertower"]		= 4,
		["mine"]			= 5,
		[""]				= 6,
	},
	maxStatusRowCount = 9,
}

local L = {
	["Reset view"] = ffi.string(C.GetLocalizedText(1026, 7911, ReadText(1026, 7902))),
}

local function init()
	Menus = Menus or {}
	
	local oldMenu
	local oldMenu
	for _, otherMenu in pairs(Menus) do
		if otherMenu.name == "ShipConfigurationMenu" then
			oldMenu = otherMenu
			break
		end
	end
	
	if oldMenu then
		UnregisterEvent("show"..oldMenu.name, oldMenu.showMenuCallback)
	end
	
	table.insert(Menus, menu)
	if Helper then
		Helper.registerMenu(menu)
	end

	menu.shoppinglist = {}
end

function menu.cleanup()
	menu.isReadOnly = nil
	menu.container = nil
	menu.containerowner = nil
	menu.object = nil
	menu.macro = nil
	menu.class = nil
	menu.upgradewares = {}
	menu.groups = {}
	menu.slots = {}
	menu.ammo = {}
	menu.software = {}
	menu.upgradeplan = {}
	menu.selectableships = {}
	menu.selectableshipsbyclass = {}
	menu.shoppinglist = {}
	menu.shoppinglisttotal = 0
	menu.total = nil
	menu.activatemap = nil
	menu.editingshoppinglist = nil
	menu.updateMoney = nil
	menu.searchtext = ""
	menu.loadoutName = ""
	menu.loadout = nil
	menu.inventory = {}
	menu.modwares = {}
	menu.captainSelected = true
	menu.initialLoadoutStatistics = {}
	menu.currentIdx = 0
	menu.immediate = nil
	menu.installedPaintMod = nil
	menu.selectedPaintMod = nil
	menu.validLicence = nil
	menu.tasks = {}
	menu.contextMode = nil

	menu.repairplan = {}
	menu.damagedcomponents = {}
	menu.repairslots = {}
	menu.totalrepairprice = nil

	if menu.holomap ~= 0 then
		C.RemoveHoloMap()
		menu.holomap = 0
	end

	menu.frameworkData = {}
	menu.slotData = {}
	menu.planData = {}
	menu.statsData = {}
	menu.titleData = {}
	menu.mapData = {}

	menu.leftbartable = nil
	menu.slottable = nil
	menu.plantable = nil
	menu.titlebartable = nil
	menu.map = nil

	menu.canundo = nil
	menu.canredo = nil

	menu.topRows = {}
	menu.selectedRows = {}
	menu.selectedCols = {}
	
	UnregisterAddonBindings("ego_detailmonitor", "undo")
end

-- button scripts

function menu.isModSlotExpanded(type, slot)
	return menu.expandedModSlots[type .. slot]
end

function menu.expandModSlot(type, slot, row)
	if menu.expandedModSlots[type .. slot] then
		menu.expandedModSlots[type .. slot] = nil
	else
		menu.expandedModSlots[type .. slot] = true
	end

	menu.selectedRows.slots = row
	menu.refreshMenu()
end

function menu.isUpgradeExpanded(idx, ware, category)
	return menu.expandedUpgrades[idx .. ware .. category]
end

function menu.expandUpgrade(idx, ware, category, row)
	if menu.expandedUpgrades[idx .. ware .. category] then
		menu.expandedUpgrades[idx .. ware .. category] = nil
	else
		menu.expandedUpgrades[idx .. ware .. category] = true
	end

	menu.selectedRows.plan = row
	menu.refreshMenu()
end

function menu.buttonSelectSlot(slot, row, col)
	if menu.currentSlot ~= slot then
		menu.currentSlot = slot
	end
	
	menu.selectMapMacroSlot()

	menu.topRows.slots = GetTopRow(menu.slottable)
	menu.selectedRows.slots = row
	menu.selectedCols.slots = col
	menu.refreshMenu()
end

function menu.buttonSelectUpgradeMacro(type, slot, macro, row, col, keepcontext)
	if not keepcontext then
		menu.closeContextMenu()
	end

	local upgradetype = Helper.findUpgradeType(type)

	if (upgradetype.supertype == "macro") or (upgradetype.supertype == "virtualmacro") then
		if macro ~= menu.upgradeplan[type][slot] then
			if upgradetype.mergeslots then
				for i, slotdata in ipairs(menu.slots[type]) do
					if menu.mode == "upgrade" then
						if slotdata.component then
							menu.removeRepairedComponent(menu.object, slotdata.component)
						end
					end
					menu.upgradeplan[type][i] = macro
				end
			else
				if menu.mode == "upgrade" then
					if menu.slots[type][slot].component then
						menu.removeRepairedComponent(menu.object, menu.slots[type][slot].component)
					end
				end
				menu.upgradeplan[type][slot] = macro
			end

			menu.selectedRows.slots = row
			menu.selectedCols.slots = col
			menu.refreshMenu()
		end
	end

	if menu.holomap and (menu.holomap ~= 0) then
		Helper.callLoadoutFunction(menu.upgradeplan, nil, function (loadout, _) return C.UpdateObjectConfigurationMap(menu.holomap, menu.object, 0, menu.macro, false, loadout) end)
	end

	if keepcontext then
		menu.topRows.context = GetTopRow(menu.contexttable)
		menu.selectedRows.context = keepcontext
		menu.displayContextMenu()
	end
end

function menu.checkboxSelectCaptain(row)
	menu.captainSelected = true

	menu.selectedRows.slots = row
	menu.refreshMenu()
end

function menu.checkboxSelectSoftware(type, slot, software, row, keepcontext)
	if not keepcontext then
		menu.closeContextMenu()
	end

	local upgradetype = Helper.findUpgradeType(type)

	if software ~= menu.upgradeplan[type][slot] then
		menu.upgradeplan[type][slot] = software
	else
		if menu.software[type][slot].defaultsoftware ~= 0 then
			menu.upgradeplan[type][slot] = menu.software[type][slot].possiblesoftware[menu.software[type][slot].defaultsoftware]
		else
			menu.upgradeplan[type][slot] = ""
		end
	end

	menu.selectedRows.slots = row
	menu.refreshMenu()

	if keepcontext then
		menu.topRows.context = GetTopRow(menu.contexttable)
		menu.selectedRows.context = keepcontext
		menu.displayContextMenu()
	end
end

function menu.buttonSelectRepair(component, row, col, keepcontext)
	local objectstring = tostring(menu.object)
	local componentstring = tostring(component)

	menu.repairplan[objectstring] = menu.repairplan[objectstring] or {}
	if menu.repairplan[objectstring][componentstring] then
		menu.repairplan[objectstring][componentstring] = nil
	else
		menu.repairplan[objectstring][componentstring] = true
	end

	menu.selectedRows.slots = row
	menu.selectedCols.slots = col
	menu.refreshMenu()
end

function menu.removeRepairedComponent(object, component)
	local objectstring = tostring(object)
	local componentstring = tostring(component)

	if menu.repairplan[objectstring] then
		menu.repairplan[objectstring][componentstring] = nil
	end
end

function menu.buttonSelectGroupUpgrade(type, group, macro, row, col, keepcontext)
	if not keepcontext then
		menu.closeContextMenu()
	end

	local upgradetype = Helper.findUpgradeType(type)

	if (upgradetype.supertype == "group") then
		if macro ~= menu.upgradeplan[type][group].macro then
			for i, slotdata in ipairs(menu.slots[upgradetype.grouptype]) do
				local groupinfo = C.GetUpgradeSlotGroup(menu.object, menu.macro, upgradetype.grouptype, i)
				if upgradetype.mergeslots or ((groupinfo.path == menu.upgradeplan[type][group].path) and (groupinfo.group == menu.upgradeplan[type][group].group)) then
					if menu.mode == "upgrade" then
						if slotdata.component then
							menu.removeRepairedComponent(menu.object, slotdata.component)
						end
					end
				end
			end

			-- handle already installed equipment
			local haslicence = menu.checkLicence(macro)
			if (macro == menu.groups[group][upgradetype.grouptype].currentmacro) and (not haslicence) then
				menu.upgradeplan[type][group].count = math.min(menu.upgradeplan[type][group].count, menu.groups[group][upgradetype.grouptype].count)
			end

			menu.upgradeplan[type][group].macro = macro
			if (macro ~= "") and (menu.upgradeplan[type][group].count == 0) then
				menu.upgradeplan[type][group].count = 1
			elseif (macro == "") and (menu.upgradeplan[type][group].count ~= 0) then
				menu.upgradeplan[type][group].count = 0
			end

			if upgradetype.pseudogroup then
				for i, slotdata in ipairs(menu.slots[upgradetype.grouptype]) do
					local groupinfo = C.GetUpgradeSlotGroup(menu.object, menu.macro, upgradetype.grouptype, i)
					if upgradetype.mergeslots or ((groupinfo.path == menu.upgradeplan[type][group].path) and (groupinfo.group == menu.upgradeplan[type][group].group)) then
						menu.upgradeplan[upgradetype.grouptype][i] = macro
					end
				end
			end

			menu.selectedRows.slots = row
			menu.selectedCols.slots = col
			menu.refreshMenu()
		end
	end

	if menu.holomap and (menu.holomap ~= 0) then
		Helper.callLoadoutFunction(menu.upgradeplan, nil, function (loadout, _) return C.UpdateObjectConfigurationMap(menu.holomap, menu.container, menu.object, menu.macro, true, loadout) end)
	end

	if keepcontext then
		menu.topRows.context = GetTopRow(menu.contexttable)
		menu.selectedRows.context = keepcontext
		menu.displayContextMenu()
	end
end

function menu.buttonTitleSave()
	if menu.contextMode and (menu.contextMode.mode == "saveLoadout") then
		menu.closeContextMenu()
	else
		menu.displayContextFrame("saveLoadout", menu.titleData.dropdownWidth + menu.titleData.height + Helper.borderSize, menu.titleData.offsetX + 2 * (menu.titleData.dropdownWidth + Helper.borderSize), menu.titleData.offsetY + menu.titleData.height + Helper.borderSize)
	end
end

function menu.buttonSave(overwrite)
	local loadoutid
	if overwrite then
		loadoutid = menu.loadout
	end

	Helper.closeDropDownOptions(menu.titlebartable, 1, 1)
	Helper.closeDropDownOptions(menu.titlebartable, 1, 2)
	if menu.mode ~= "modify" then
		Helper.closeDropDownOptions(menu.titlebartable, 1, 3)
	end
	local macro = (menu.macro ~= "") and menu.macro or GetComponentData(ConvertStringToLuaID(tostring(menu.object)), "macro")
	if macro ~= nil then
		Helper.callLoadoutFunction(menu.upgradeplan, nil, function (loadout, _) return C.SaveLoadout(macro, loadout, "local", loadoutid or "player", loadoutid ~= nil, menu.loadoutName, "") end)
		menu.getPresetLoadouts()
	end
	menu.closeContextMenu()
	menu.refreshTitleBar()
end

function menu.buttonDismantleMod(type, component, context, group)
	local fficurrentloadoutstats = C.GetCurrentLoadoutStatistics(menu.object)
	menu.initialLoadoutStatistics = Helper.convertLoadoutStats(fficurrentloadoutstats)

	if (type == "weapon") or (type == "turret") then
		C.DismantleWeaponMod(component)
	elseif type == "engine" then
		C.DismantleEngineMod(component)
	elseif type == "shield" then
		C.DismantleShieldMod(component, context, group)
	elseif type == "ship" then
		C.DismantleShipMod(component)
	end
	menu.prepareModWares()
	menu.refreshMenu()
end

function menu.buttonInstallMod(type, component, ware, price, context, group)
	if GetPlayerMoney() >= price then
		TransferPlayerMoneyTo(price, menu.container)

		local fficurrentloadoutstats = C.GetCurrentLoadoutStatistics(menu.object)
		menu.initialLoadoutStatistics = Helper.convertLoadoutStats(fficurrentloadoutstats)

		if (type == "weapon") or (type == "turret") then
			C.InstallWeaponMod(component, ware)
		elseif type == "engine" then
			C.InstallEngineMod(component, ware)
		elseif type == "shield" then
			C.InstallShieldMod(component, context, group, ware)
		elseif type == "ship" then
			C.InstallShipMod(component, ware)
		end
		menu.prepareModWares()
		menu.refreshMenu()
	end
end

function menu.buttonContextEncyclopedia(selectedUpgrade)
	local upgradetype = Helper.findUpgradeType(selectedUpgrade.type)

	if (upgradetype.supertype == "macro") or (upgradetype.supertype == "virtualmacro") or (upgradetype.supertype == "group") then
		local library = GetMacroData(selectedUpgrade.macro, "infolibrary")
		Helper.closeMenuAndOpenNewMenu(menu, "EncyclopediaMenu", { 0, 0, upgradetype.emode, library, selectedUpgrade.macro })
		menu.cleanup()
	elseif upgradetype.supertype == "software" then
		Helper.closeMenuAndOpenNewMenu(menu, "EncyclopediaMenu", { 0, 0, upgradetype.emode, "software", selectedUpgrade.software })
		menu.cleanup()
	elseif upgradetype.supertype == "ammo" then
		local library = GetMacroData(selectedUpgrade.macro, "infolibrary")
		if upgradetype.emode then
			local emode = upgradetype.emode
			if library == "mines" then
				emode = "Weapons"
			end
			Helper.closeMenuAndOpenNewMenu(menu, "EncyclopediaMenu", { 0, 0, emode, library, selectedUpgrade.macro })
			menu.cleanup()
		end
	end
end

function menu.buttonInteract(selectedData, button, row, col, posx, posy)
	menu.selectedUpgrade = selectedData
	local x, y = GetLocalMousePosition()
	if x == nil then
		-- gamepad case
		x = posx
		y = -posy
	end
	menu.displayContextFrame("equipment", Helper.scaleX(200), x + Helper.viewWidth / 2, Helper.viewHeight / 2 - y)
end

function menu.slidercellSelectAmount(type, slot, macro, row, value)
	menu.closeContextMenu()

	local upgradetype = Helper.findUpgradeType(type)

	if (upgradetype.supertype == "ammo") then
		menu.upgradeplan[type][macro] = value
		menu.selectedRows.slots = row
	end
end

function menu.slidercellSelectCrewAmount(slot, tier, row, value)
	menu.noupdate = true

	local oldwanted = menu.crew.roles[slot].wanted
	menu.crew.roles[slot].wanted = value

	local change = value - oldwanted
	if change > 0 then
		-- adding crew
		menu.crew.roles[slot].tiers[tier].wanted = menu.crew.roles[slot].tiers[tier].wanted + change
		-- first use npcs from the unassigned pool
		if #menu.crew.unassigned > 0 then
			if #menu.crew.unassigned >= change then
				-- first reassign npcs from the correct role
				for i = #menu.crew.unassigned, 1, -1 do
					if menu.crew.unassigned[i].role == menu.crew.roles[slot].id then
						table.insert(menu.crew.roles[slot].tiers[tier].currentnpcs, menu.crew.unassigned[i].npc)
						table.insert(menu.crew.transferdetails, { npc = menu.crew.unassigned[i].npc, newrole = menu.crew.roles[slot].id })
						table.remove(menu.crew.unassigned, i)
						change = change - 1
						if change == 0 then
							break
						end
					end
				end
				if change > 0 then
					-- if that wasn't enough, use all unassigned npcs
					for i = #menu.crew.unassigned, 1, -1 do
						table.insert(menu.crew.roles[slot].tiers[tier].currentnpcs, menu.crew.unassigned[i].npc)
						table.insert(menu.crew.transferdetails, { npc = menu.crew.unassigned[i].npc, newrole = menu.crew.roles[slot].id })
						table.remove(menu.crew.unassigned, i)
						change = change - 1
						if change == 0 then
							break
						end
					end
				end
			else
				change = change - #menu.crew.unassigned
				for _, entry in ipairs(menu.crew.unassigned) do
					table.insert(menu.crew.roles[slot].tiers[tier].currentnpcs, entry.npc)
					table.insert(menu.crew.transferdetails, { npc = entry.npc, newrole = menu.crew.roles[slot].id })
				end
				menu.crew.unassigned = {}
			end
		end
		-- if any are left add newly hired npcs
		if change > 0 then
			menu.crew.hired = menu.crew.hired + change
			local found = false
			for i, entry in ipairs(menu.crew.hireddetails) do
				if entry.newrole == menu.crew.roles[slot].id then
					menu.crew.hireddetails[i].amount = menu.crew.hireddetails[i].amount + change
					found = true
					break
				end
			end
			if not found then
				table.insert(menu.crew.hireddetails, { newrole = menu.crew.roles[slot].id, amount = change })
			end
		end
	else
		-- removing crew
		-- first remove newly hired crew
		for i, entry in ipairs(menu.crew.hireddetails) do
			if entry.newrole == menu.crew.roles[slot].id then
				if entry.amount >= -change then
					menu.crew.hireddetails[i].amount = menu.crew.hireddetails[i].amount + change
					menu.crew.roles[slot].tiers[tier].wanted = menu.crew.roles[slot].tiers[tier].wanted + change
					if menu.crew.hireddetails[i].amount == 0 then
						table.remove(menu.crew.hireddetails, i)
					end
					menu.crew.hired = menu.crew.hired + change
					change = 0
				else
					menu.crew.hired = menu.crew.hired - entry.amount
					change = change + entry.amount
					menu.crew.roles[slot].tiers[tier].wanted = menu.crew.roles[slot].tiers[tier].wanted - entry.amount
					table.remove(menu.crew.hireddetails, i)
				end
				break
			end
		end
		-- if any are left move them to unassigned
		if change < 0 then
			for i = 1, -change do
				local npc
				if #menu.crew.roles[slot].tiers[tier].currentnpcs > 0 then
					npc = table.remove(menu.crew.roles[slot].tiers[tier].currentnpcs)
					menu.crew.roles[slot].tiers[tier].wanted = menu.crew.roles[slot].tiers[tier].wanted - 1
				else
					for j = 1, #menu.crew.roles[slot].tiers do
						if #menu.crew.roles[slot].tiers[j].currentnpcs > 0 then
							npc = table.remove(menu.crew.roles[slot].tiers[j].currentnpcs)
							menu.crew.roles[slot].tiers[j].wanted = menu.crew.roles[slot].tiers[j].wanted - 1
							break
						end
					end
				end
				if npc then
					table.insert(menu.crew.unassigned, { npc = npc, role = menu.crew.roles[slot].id })
				else
					DebugError("Could not find npc to remove. [Florian]")
				end
			end
		end
	end

	menu.selectedRows.slots = row
end

function menu.slidercellSelectGroupAmount(type, group, row, keepcontext, value)
	if not keepcontext then
		menu.closeContextMenu()
	end

	local upgradetype = Helper.findUpgradeType(type)

	if (upgradetype.supertype == "group") then
		if value ~= menu.upgradeplan[type][group].count then
			menu.upgradeplan[type][group].count = value

			menu.selectedRows.slots = row
		end
	end

	if menu.holomap and (menu.holomap ~= 0) then
		Helper.callLoadoutFunction(menu.upgradeplan, nil, function (loadout, _) return C.UpdateObjectConfigurationMap(menu.holomap, menu.object, 0, menu.macro, false, loadout) end)
	end

	if keepcontext then
		menu.topRows.context = GetTopRow(menu.contexttable)
		menu.selectedRows.context = keepcontext
		menu.displayContextMenu()
	end
end

function menu.onSliderCellConfirm(slidercellID)
	menu.refreshMenu()
	menu.noupdate = nil
end

function menu.checkCommanderRepairOrders(shipidstring)
	local commander = GetCommander(ConvertStringTo64Bit(shipidstring))
	if commander then
		local commanderidstring = tostring(ConvertIDTo64Bit(commander))
		if menu.repairplan[commanderidstring] then
			return true
		else
			return menu.checkCommanderRepairOrders(commanderidstring)
		end
	end

	return false
end

function menu.getLastUnprocessedSubordinate(shipidstring, list_processed)
	local ship = ConvertStringToLuaID(tostring(shipidstring))
	local subordinates = GetSubordinates(ship)
	local subordinate = nil

	for _, locsubordinate in pairs(subordinates) do
		local skip = nil
		for _, eval in pairs(list_processed) do
			if tostring(locsubordinate) == tostring(eval) then
				skip = true
				break
			end
		end
		if not skip then
			if locsubordinate then
				local locsubordinatestring = tostring(ffi.new("UniverseID", ConvertStringTo64Bit(tostring(locsubordinate))))
				local result = menu.getLastUnprocessedSubordinate(locsubordinatestring, list_processed)
				subordinate = result[1]
				if not subordinate then
					subordinate = locsubordinate
				end
			end
		else
			skip = nil
		end
	end

	--print("returning " .. tostring(subordinate))
	return {subordinate, list_processed}
end

function menu.processRepairsFor(shipidstring, orderindex)
	local ship = ConvertStringTo64Bit(shipidstring)
	local subordinates = GetSubordinates(ConvertStringToLuaID(tostring(shipidstring)))
	local subordinateorders = {}
	local list_processed = {}

	while #subordinates > 0 do
		local result = menu.getLastUnprocessedSubordinate(shipidstring, list_processed)
		local subordinate = result[1]
		list_processed = result[2]

		if subordinate then
			table.insert(list_processed, subordinate)
			for i = #subordinates, 1, -1 do
				if tostring(subordinate) == tostring(subordinates[i]) then
					table.remove(subordinates, i)
					break
				end
			end
			local subordinateidstring = tostring(ffi.new("UniverseID", ConvertStringTo64Bit(tostring(subordinate))))
			local param2 = nil
			local param3 = {}
			if menu.repairplan[subordinateidstring] then
				for componentidstring, _ in pairs(menu.repairplan[subordinateidstring]) do
					if componentidstring ~= "processed" then
						if componentidstring == subordinateidstring then
							param2 = 100
						else
							table.insert(param3, ConvertStringToLuaID(componentidstring))
						end
					end
				end
				subordinateorders[subordinate] = { ConvertStringToLuaID(tostring(menu.container)), param2, param3, false, 0 }
				menu.repairplan[subordinateidstring]["processed"] = true
			end
		else
			break
		end
	end

	local shipluaid = ConvertStringToLuaID(tostring(shipidstring))

	-- param 1 == destination (component)
	SetOrderParam(shipluaid, orderindex, 1, nil, ConvertStringToLuaID(tostring(menu.container)))

	local componentcounter = 0
	for componentidstring, _ in pairs(menu.repairplan[shipidstring]) do
		if componentidstring ~= "processed" then
			if componentidstring == shipidstring then
				SetOrderParam(shipluaid, orderindex, 2, nil, 100)
			else
				if componentcounter < 1 then
					-- NB: if we fail to initialize the list before adding things to it, game crashes at ScriptValue::Clear() with a nullptr
					SetOrderParam(shipluaid, orderindex, 3, nil, {})
				end
				componentcounter = componentcounter + 1
				-- param 3 == damagedcomponents (list)
				SetOrderParam(shipluaid, orderindex, 3, componentcounter, ConvertStringToLuaID(componentidstring))
			end
		end
	end

	-- param 4 == repairall (bool)
	SetOrderParam(shipluaid, orderindex, 4, nil, false)

	-- param 5 == acceptedcost (money)
	SetOrderParam(shipluaid, orderindex, 5, nil, menu.totalrepairprice)

	-- param 6 == urgent (bool); by default, repairs ordered by the player are always urgent
	SetOrderParam(shipluaid, orderindex, 6, nil, true)

	-- param 7 == blacklist_stations (list)

	-- param 8 == subordinaterepairorders (table)
	SetOrderParam(shipluaid, orderindex, 8, nil, subordinateorders)

	-- param 9 == subordinateorders (list) (used for undocking subordinates)

	-- param 10 == debugchance (int)
	--SetOrderParam(shipluaid, orderindex, 10, nil, 100)

	menu.repairplan[shipidstring]["processed"] = true

	if not C.EnableOrder(ship, orderindex) then
		print("ERROR: Order to initiate repairs for " .. ffi.string(C.GetComponentName(ship)) .. " was not enabled.")
	end
end

function menu.buttonConfirm()
	local playermoney = GetPlayerMoney()
	if (playermoney - menu.shoppinglisttotal) >= 0 then
		TransferPlayerMoneyTo(menu.shoppinglisttotal, menu.container)

		if (menu.mode == "purchase") or (menu.mode == "upgrade") then
			for i, entry in ipairs(menu.shoppinglist) do
				if i ~= menu.editingshoppinglist then
					if (entry.object ~= 0) and menu.repairplan[tostring(entry.object)] and (not menu.repairplan[tostring(entry.object)]["processed"]) then
						local skip = menu.checkCommanderRepairOrders(tostring(entry.object))
						if not skip then
							--print("pilot: " .. tostring(GetComponentData(ConvertStringTo64Bit(tostring(entry.object)), "pilot")) .. ", cond: " .. tostring( (entry.object == C.GetPlayerOccupiedShipID()) or not GetComponentData(ConvertStringTo64Bit(tostring(entry.object)), "pilot") ) .. " cond1: " .. tostring(entry.object == C.GetPlayerOccupiedShipID()) .. " cond2: " .. tostring(not GetComponentData(ConvertStringTo64Bit(tostring(entry.object)), "pilot")))
							if (C.GetTopLevelContainer(entry.object) == menu.container) then
								local damagedcomponents = {}
								for componentidstring, _ in pairs(menu.repairplan[tostring(entry.object)]) do
									if componentidstring ~= "processed" and componentidstring ~= tostring(entry.object) then
										table.insert(damagedcomponents, ConvertStringToLuaID(componentidstring))
									end
								end

								-- signal received in build.shiptrader
								-- param = "'repairs_initiate'", param2 = $shiptoberepaired, param3 = [hullpercent(int), damagedcomponents(list)]
								--print("signalling " .. ffi.string(C.GetComponentName(menu.container)) .. " to repair " .. ffi.string(C.GetComponentName(entry.object)) .. " " .. tostring(entry.object))
								SignalObject(menu.container, "repairs_initiate", ConvertStringToLuaID(tostring(entry.object)), {100, damagedcomponents})
							else
								local orderindex = C.CreateOrder(entry.object, "Repair", false)
								menu.processRepairsFor(tostring(entry.object), orderindex)

								-- in this case, signal is sent from order.repair
								if not C.EnableOrder(entry.object, orderindex) then
									print("ERROR: Order to initiate repairs for " .. ffi.string(C.GetComponentName(entry.object)) .. " was not enabled.")
								end
							end
						end
					end

					if entry.hasupgrades then
						for i = 1, entry.amount do
							Helper.callLoadoutFunction(entry.upgradeplan, entry.crew, function (loadout, crewtransfer) return C.AddBuildTask(menu.container, entry.object, entry.macro, loadout, entry.price, crewtransfer, menu.immediate) end)
						end
					end
				end
			end
		elseif menu.mode == "modify" then
			-- TODO
		end

		menu.closeMenu("back")
	else
		menu.displayMenu()
	end
end

function menu.buttonSelectPaintMod(entry, row, col)
	menu.selectedPaintMod = entry
	C.SetMapPaintMod(menu.holomap, entry.ware)
	
	menu.selectedRows.slots = row
	menu.selectedCols.slots = col
	menu.refreshMenu()
end

function menu.buttonInstallPaintMod()
	if menu.modeparam[1] then
		for _, ship in pairs(menu.modeparam[2]) do
			local change = false
			local paintmod = ffi.new("UIPaintMod")
			if C.GetInstalledPaintMod(ship, paintmod) then
				if menu.selectedPaintMod.ware ~= ffi.string(paintmod.Ware) then
					change = true
				end
			else
				change = true
			end
			if change then
				C.InstallPaintMod(ship, menu.selectedPaintMod.ware, not menu.selectedPaintMod.isdefault)
			end
		end
	else
		C.InstallPaintMod(menu.object, menu.selectedPaintMod.ware, not menu.selectedPaintMod.isdefault)
	end

	menu.selectedRows.slots = Helper.currentTableRow[menu.slottable]
	menu.selectedCols.slots = Helper.currentTableCol[menu.slottable]
	
	menu.mapstate = ffi.new("HoloMapState")
	C.GetMapState(menu.holomap, menu.mapstate)
	menu.prepareModWares()
	menu.getDataAndDisplay()
end

function menu.dropdownShipClass(_, class)
	if class ~= menu.class then
		menu.class = class or ""
		if menu.mode == "purchase" then
			menu.validLicence = false
			menu.macro = ""
		elseif (menu.mode == "upgrade") or (menu.mode == "modify") then
			menu.object = menu.selectableshipsbyclass[class][1]

			if (menu.mode == "modify") then
				local fficurrentloadoutstats = C.GetCurrentLoadoutStatistics(menu.object)
				menu.initialLoadoutStatistics = Helper.convertLoadoutStats(fficurrentloadoutstats)
			end
		end
		menu.getDataAndDisplay()
	end
end

function menu.dropdownShip(_, shipid)
	if menu.mode == "purchase" then
		local oldmacro = menu.macro
		menu.macro = shipid or ""
		if menu.macro ~= oldmacro then
			menu.currentSlot = 1
			menu.captainSelected = false
			menu.validLicence = menu.checkLicence(menu.macro, true)
			menu.getDataAndDisplay()
		end
	elseif (menu.mode == "upgrade") or (menu.mode == "modify") then
		local oldobject = menu.object
		menu.object = ffi.new("UniverseID", ConvertStringTo64Bit(shipid))
		
		if menu.object ~= oldobject then
			if menu.editingshoppinglist then
				table.remove(menu.shoppinglist, menu.editingshoppinglist)
			end

			if (menu.mode == "modify") then
				local fficurrentloadoutstats = C.GetCurrentLoadoutStatistics(menu.object)
				menu.initialLoadoutStatistics = Helper.convertLoadoutStats(fficurrentloadoutstats)
			end

			local found = false
			for idx, entry in ipairs(menu.shoppinglist) do
				if menu.object == entry.object then
					found = true

					menu.editingshoppinglist = idx
					menu.object = entry.object
					menu.macro = entry.macro
					menu.getDataAndDisplay(entry.upgradeplan, entry.crew, true, true)
					break
				end
			end
		
			if not found then
				menu.editingshoppinglist = nil

				menu.currentSlot = 1
				menu.getDataAndDisplay(nil, nil, nil, true)
			end
		end
	end
end

function menu.dropdownLoadout(_, loadoutid)
	if loadoutid ~= nil then
		if menu.loadout ~= loadoutid then
			menu.loadout = loadoutid
			local preset
			for _, loadout in ipairs(menu.loadouts) do
				if loadout.id == menu.loadout then
					menu.loadoutName = loadout.name
					if loadout.preset then
						preset = loadout.preset
						menu.loadout = nil
						menu.loadoutName = ""
					end
					break
				end
			end
			local loadout, crew
			if preset then
				loadout = Helper.getLoadoutHelper(C.GenerateShipLoadout, C.GenerateShipLoadoutCounts, menu.container, menu.object, menu.macro, preset)
				if menu.mode == "purchase" then
					local intendedcrew = preset * menu.crew.capacity
					local intendedcrewperrole = math.floor(intendedcrew / #menu.crew.roles)

					crew = {
						roles = {},
						unassigned = {},
						hired = 0,
						hireddetails = {},
						fired = {},
					}

					for i, entry in ipairs(menu.crew.roles) do
						crew.roles[i] = { tiers = { [1] = {} } }
						crew.roles[i].wanted = intendedcrewperrole
						crew.roles[i].tiers[1].wanted = intendedcrewperrole

						crew.hired = crew.hired + intendedcrewperrole
						table.insert(crew.hireddetails, { newrole = entry.id, amount = intendedcrewperrole })
					end
				end
			else
				loadout = Helper.getLoadoutHelper(C.GetLoadout, C.GetLoadoutCounts, menu.object, menu.macro, loadoutid)
			end
			local upgradeplan = Helper.convertLoadout(menu.object, menu.macro, loadout, menu.software)
			menu.captainSelected = true
			menu.getDataAndDisplay(upgradeplan, crew)
		end
	end
end

function menu.dropdownLoadoutRemoved(_, loadoutid)
	local macro = (menu.macro ~= "") and menu.macro or GetComponentData(ConvertStringToLuaID(tostring(menu.object)), "macro")
	C.RemoveLoadout("local", macro, loadoutid)
	if loadoutid == menu.loadout then
		menu.loadout = nil
		menu.loadoutName = nil
	end
	for i, loadout in ipairs(menu.loadouts) do
		if loadout.id == loadoutid then
			table.remove(menu.loadouts, i)
			break
		end
	end
end

function menu.dropdownSelectMacro(type, slot, macro)
	local upgradetype = Helper.findUpgradeType(type)

	if (upgradetype.supertype == "macro") or (upgradetype.supertype == "virtualmacro") or (upgradetype.supertype == "software") then
		if macro ~= menu.upgradeplan[type][slot] then
			if upgradetype.mergeslots then
				for i, _ in ipairs(menu.slots[type]) do
					menu.upgradeplan[type][i] = macro
				end
			else
				menu.upgradeplan[type][slot] = macro
			end

			menu.refreshMenu()
		end
	end

	if menu.holomap and (menu.holomap ~= 0) then
		Helper.callLoadoutFunction(menu.upgradeplan, nil, function (loadout, _) return C.UpdateObjectConfigurationMap(menu.holomap, menu.object, 0, menu.macro, false, loadout) end)
	end
end

function menu.onDropDownActivated()
	menu.closeContextMenu()
end

function menu.buttonAddPurchase(hasupgrades, hasrepairs)
	menu.closeContextMenu()

	table.insert(menu.shoppinglist, { object = menu.object, macro = menu.macro, hasupgrades = hasupgrades, upgradeplan = menu.upgradeplan, crew = menu.crew, amount = 1, price = menu.total, duration = menu.duration, warnings = menu.warnings })
	menu.object = 0
	menu.macro = ""
	menu.getDataAndDisplay()
end

function menu.buttonConfirmPurchaseEdit(hasupgrades, hasrepairs)
	menu.closeContextMenu()

	if (not hasupgrades) and (not hasrepairs) then
		table.remove(menu.shoppinglist, menu.editingshoppinglist)
	else
		menu.shoppinglist[menu.editingshoppinglist].hasupgrades = hasupgrades
		menu.shoppinglist[menu.editingshoppinglist].upgradeplan = menu.upgradeplan
		menu.shoppinglist[menu.editingshoppinglist].crew = menu.crew
		menu.shoppinglist[menu.editingshoppinglist].price = menu.total
		menu.shoppinglist[menu.editingshoppinglist].duration = menu.duration
		menu.shoppinglist[menu.editingshoppinglist].warnings = menu.warnings
	end

	menu.object = 0
	menu.macro = ""
	menu.editingshoppinglist = nil
	menu.getDataAndDisplay()
end

function menu.buttonChangePurchaseAmount(idx, change)
	local entry = menu.shoppinglist[idx]
	if change > 0 then
		entry.amount = entry.amount + change
	elseif change < 0 then
		if entry.amount > change + 1 then
			entry.amount = entry.amount + change
		end
	end
	menu.refreshMenu()
end

function menu.buttonEditPurchase(idx)
	menu.closeContextMenu()

	local entry = menu.shoppinglist[idx]
	menu.editingshoppinglist = idx
	menu.object = entry.object
	menu.macro = entry.macro
	menu.getDataAndDisplay(entry.upgradeplan, entry.crew, true, true)
end

function menu.buttonRemovePurchase(idx)
	table.remove(menu.shoppinglist, idx)
	menu.refreshMenu()
end

function menu.buttonLeftBar(mode, row, overrideMode, overrideSlot)
	menu.closeContextMenu()

	menu.prevUpgradetypeMode = menu.upgradetypeMode
	if (menu.upgradetypeMode == overrideMode) and (menu.currentSlot == overrideSlot) then
		menu.upgradetypeMode = nil
	else
		AddUITriggeredEvent(menu.name, mode, menu.upgradetypeMode == mode and "off" or "on")
		if menu.upgradetypeMode == mode then
			PlaySound("ui_negative_back")
			menu.upgradetypeMode = nil
		else
			menu.setdefaulttable = true
			PlaySound("ui_positive_select")
			menu.upgradetypeMode = overrideMode or mode
		end
	end
	menu.currentSlot = overrideSlot or 1

	if menu.upgradetypeMode then
		menu.selectMapMacroSlot()
	else
		C.ClearSelectedMapMacroSlots(menu.holomap)
	end
	
	menu.displayMenu(true)
end

function menu.deactivateUpgradeMode()
	menu.prevUpgradetypeMode = menu.upgradetypeMode
	PlaySound("ui_negative_back")
	menu.upgradetypeMode = nil
	menu.currentSlot = 1
	C.ClearSelectedMapMacroSlots(menu.holomap)
	menu.displayMenu()
end

function menu.buttonModCategory(category, row, col)
	if category ~= menu.modCategory then
		menu.modCategory = category

		menu.topRows.slots = GetTopRow(menu.slottable)
		menu.selectedRows.slots = row
		menu.selectedCols.slots = col
		menu.displayMenu()
	end
end

function menu.getModQuality(category)
	for i, entry in ipairs(Helper.modQualities) do
		if entry.category == category then
			return i
		end
	end
end

function menu.buttonClearEditbox(row)
	Helper.cancelEditBoxInput(menu.slottable, row, 1)
	menu.searchtext = ""

	menu.refreshMenu()
end

function menu.buttonResetCrew()
	menu.crew.fired = {}
	menu.crew.hired = 0
	menu.crew.hireddetails = {}
	menu.crew.transferdetails = {}
	menu.crew.unassigned = {}
	for i, entry in ipairs(menu.crew.roles) do
		menu.crew.roles[i].wanted = entry.total
		for j, tier in ipairs(entry.tiers) do
			for _, npc in ipairs(menu.crew.roles[i].tiers[j].npcs) do
				table.insert(menu.crew.roles[i].tiers[j].currentnpcs, npc)
			end
			menu.crew.roles[i].tiers[j].wanted = tier.total
		end
	end

	menu.refreshMenu()
end

function menu.buttonFireCrew()
	for _, entry in ipairs(menu.crew.unassigned) do
		table.insert(menu.crew.fired, entry.npc)
	end
	menu.crew.unassigned = {}

	menu.refreshMenu()
end

-- editbox scripts
function menu.editboxSearchUpdateText(_, text, textchanged)
	if textchanged then
		menu.searchtext = text
	end

	menu.refreshMenu()
end

function menu.editboxLoadoutNameUpdateText(_, text)
	menu.loadoutName = text
	menu.loadout = nil
end

-- Menu member functions

function menu.hotkey(action)
	if action == "INPUT_ACTION_ADDON_DETAILMONITOR_UNDO" then
		menu.undoHelper(true)
	elseif action == "INPUT_ACTION_ADDON_DETAILMONITOR_REDO" then
		menu.undoHelper(false)
	end
end

function menu.undoHelper(undo)
	if undo then
		-- TODO
	else
		-- TODO
	end
	menu.displayMenu()
end

function menu.refreshMenu()
	if not menu.topRows.slots then
		menu.topRows.slots = GetTopRow(menu.slottable)
	end
	if not menu.selectedRows.slots then
		menu.selectedRows.slots = Helper.currentTableRow[menu.slottable]
	end

	menu.displayMenu()
end

function menu.findUpgradeMacro(loctype, macro)
	if type(menu.upgradewares[loctype]) == "table" then
		for i, upgradeware in ipairs(menu.upgradewares[loctype]) do
			if upgradeware.macro == macro then
				return i
			end
		end
	end
end

function menu.onShowMenu(state)
	menu.damagedcomponents = {}
	
	-- layout
	menu.scaleSize = Helper.scaleX(config.scaleSize)
	menu.frameworkData = {
		sidebarWidth = Helper.scaleX(Helper.sidebarWidth),
		offsetX = Helper.frameBorder,
		offsetY = Helper.frameBorder + 20,
		scaleWidth = 3 * menu.scaleSize,
	}
	menu.slotData = {
		width = math.floor(0.25 * Helper.viewWidth) - menu.frameworkData.sidebarWidth - menu.frameworkData.offsetX - 2 * Helper.borderSize,
		offsetX = menu.frameworkData.sidebarWidth + menu.frameworkData.scaleWidth + menu.frameworkData.offsetX + 3 * Helper.borderSize,
		offsetY = Helper.frameBorder + Helper.borderSize,
	}
	menu.planData = {
		width = math.floor(0.25 * Helper.viewWidth) - menu.frameworkData.sidebarWidth - menu.frameworkData.offsetX - 2 * Helper.borderSize,
		offsetY = Helper.frameBorder + Helper.borderSize,
	}
	menu.statsData = {
		width =  Helper.viewWidth - 2 * menu.slotData.offsetX - menu.slotData.width - menu.planData.width - 6 * Helper.borderSize,
		offsetX = menu.slotData.offsetX + menu.slotData.width + 3 * Helper.borderSize,
		offsetY = Helper.frameBorder,
	}
	menu.titleData = {
		width =  Helper.viewWidth - 2 * menu.slotData.offsetX - menu.slotData.width - menu.planData.width - 4 * Helper.borderSize,
		height = Helper.scaleY(40),
		offsetX = menu.slotData.offsetX + menu.slotData.width + 2 * Helper.borderSize,
		offsetY = Helper.frameBorder,
	}
	menu.titleData.dropdownWidth = math.floor((menu.titleData.width - 4 * (menu.titleData.height + Helper.borderSize) - 2 * Helper.borderSize) / 3)
	menu.planData.offsetX = menu.titleData.offsetX + menu.titleData.width + 2 * Helper.borderSize
	menu.mapData = {
		width = Helper.viewWidth,
		height = Helper.viewHeight,
		offsetX = 0,
		offsetY = 0
	}
	menu.subHeaderRowHeight = Helper.scaleY(26)

	menu.headerTextProperties = {
		font = Helper.headerRow1Font,
		fontsize = Helper.scaleFont(Helper.headerRow1Font, Helper.headerRow1FontSize),
		x = Helper.scaleX(Helper.headerRow1Offsetx),
		y = math.floor((menu.titleData.height - Helper.scaleY(Helper.headerRow1Height)) / 2 + Helper.scaleY(Helper.headerRow1Offsety)),
		minRowHeight = menu.titleData.height,
		scaling = false,
		cellBGColor = { r = 0, g = 0, b = 0, a = 0 },
		titleColor = Helper.defaultSimpleBackgroundColor,
	}
	
	menu.subHeaderTextProperties = {
		font = Helper.headerRow1Font,
		fontsize = Helper.scaleFont(Helper.headerRow1Font, Helper.headerRow1FontSize),
		x = Helper.scaleX(Helper.headerRow1Offsetx),
		minRowHeight = menu.subHeaderRowHeight,
		scaling = false,
		cellBGColor = { r = 0, g = 0, b = 0, a = 0 },
		titleColor = Helper.defaultSimpleBackgroundColor,
	}
	
	menu.subHeaderSliderCellTextProperties = {
		font = Helper.headerRow1Font,
		fontsize = Helper.scaleFont(Helper.headerRow1Font, Helper.headerRow1FontSize),
		x = Helper.scaleX(Helper.headerRow1Offsetx),
		scaling = false,
	}

	-- parameters
	menu.mode = menu.param[4]
	if menu.param[3] == nil then
		if menu.mode == "upgrade" then
			menu.isReadOnly = true
		end
	else
		menu.container = ConvertIDTo64Bit(menu.param[3])
		menu.containerowner = GetComponentData(menu.container, "owner")
	end
	menu.modeparam = {}
	if type(menu.param[5]) == "table" then
		if menu.param[5][1] == 1 then
			menu.modeparam[1] = true
			menu.modeparam[2] = {}
			for _, ship in pairs(menu.param[5][2]) do
				table.insert(menu.modeparam[2], ConvertIDTo64Bit(ship))
			end
		else
			for _, ship in pairs(menu.param[5]) do
				table.insert(menu.modeparam, ConvertIDTo64Bit(ship))
			end
		end
	end
	menu.immediate = false
	if menu.param[6] then
		menu.immediate = menu.param[6]
	end

	if menu.mode == "modify" then
		--menu.frameworkData.offsetY = 2 * (menu.titleData.height + Helper.borderSize) + menu.slotData.offsetY
		menu.titleData.dropdownWidth = math.floor((menu.titleData.width - 3 * (menu.titleData.height + Helper.borderSize) - Helper.borderSize) / 2)
	end

	-- prepare ships
	if menu.mode == "purchase" then
		menu.availableshipmacros = {}

		if menu.containerowner == "player" then
			local availableshipwares = {}
			menu.availableshipmacros = {}
			
			local numblueprints = C.GetNumBlueprints("", "", "")
			local blueprints = ffi.new("UIBlueprint[?]", numblueprints)
			numblueprints = C.GetBlueprints(blueprints, numblueprints, "", "", "")
			local blueprintlist = {}
			for i = 0, numblueprints-1 do
				blueprintlist[ffi.string(blueprints[i].ware)] = true
			end
			
			local isShipyard = GetComponentData(menu.container, "isshipyard")
			local isWharf = GetComponentData(menu.container, "iswharf")
			local isShip = C.IsComponentClass(menu.container, "ship")
			
			local numwares = C.GetNumWares("ship", false, "", "") --no licenseowner returns ALL ships, as it should!
			local wares = ffi.new("const char*[?]", numwares)
			numwares = C.GetWares(wares, numwares, "ship", false, "", "") --noplayerblueprint for last param for release
			for i = 0, numwares - 1 do
				table.insert(availableshipwares, ffi.string(wares[i]))
			end
			
			for i = 1, #availableshipwares do
				local shipware = availableshipwares[i]
				local macro = GetWareData(shipware, "component")
				if blueprintlist[shipware] then
					local class = ffi.string(C.GetMacroClass(macro))
					
					if (isShipyard and (class == "ship_l" or class == "ship_xl")) or ((isWharf or isShip) and (class == "ship_xs" or class == "ship_s" or class == "ship_m" )) then
						table.insert(menu.availableshipmacros, macro)
					end
				end
			end
		else
			local n = C.GetNumContainerBuilderMacros(menu.container)
			local buf = ffi.new("const char*[?]", n)
			n = C.GetContainerBuilderMacros(buf, n, menu.container)
			for i = 0, n - 1 do
				table.insert(menu.availableshipmacros, ffi.string(buf[i]))
			end
		end

		menu.object = 0
		if not menu.macro then
			menu.macro = ""
		end
		menu.class = ""

		menu.availableshipmacrosbyclass = {}
		for _, macro in ipairs(menu.availableshipmacros) do
			local class = ffi.string(C.GetMacroClass(macro))
			if menu.availableshipmacrosbyclass[class] then
				table.insert(menu.availableshipmacrosbyclass[class], macro)
			else
				menu.availableshipmacrosbyclass[class] = { macro }
			end
		end
	elseif menu.mode == "upgrade" then
		menu.selectableships = {}
		if #menu.modeparam > 0 then
			for _, ship in pairs(menu.modeparam) do
				--print("adding " .. ffi.string(C.GetComponentName(ship)) .. " " .. tostring(ship) .. " to list of ships.")
				table.insert(menu.selectableships, ffi.new("UniverseID", ConvertStringTo64Bit(ship)))
			end
		elseif not menu.isReadOnly then
			Helper.ffiVLA(menu.selectableships, "UniverseID", C.GetNumDockedShips, C.GetDockedShips, menu.container, "player")
		end

		for i = #menu.selectableships, 1, -1 do
			if (not menu.isReadOnly) and (not C.CanContainerEquipShip(menu.container, menu.selectableships[i])) then
				table.remove(menu.selectableships, i)
			elseif ffi.string(C.GetOwnerDetails(menu.selectableships[i]).factionID) ~= "player" then
				table.remove(menu.selectableships, i)
			elseif menu.selectableships[i] == C.GetPlayerOccupiedShipID() then
				menu.object = menu.selectableships[i]
			end
		end
		table.sort(menu.selectableships, Helper.sortUniverseIDName)

		if not menu.object then
			menu.object = menu.selectableships[1]
		end
		menu.macro = ""

		menu.class = ""
		menu.repairplan = {}
		if menu.object then
			menu.class = ffi.string(C.GetComponentClass(menu.object))
			menu.damagedcomponents = menu.determineNeededRepairs(menu.object)
		else
			menu.object = 0
		end

		menu.selectableshipsbyclass = {}
		for _, ship in ipairs(menu.selectableships) do
			local class = ffi.string(C.GetComponentClass(ship))
			if menu.selectableshipsbyclass[class] then
				table.insert(menu.selectableshipsbyclass[class], ship)
			else
				menu.selectableshipsbyclass[class] = { ship }
			end
		end
	elseif menu.mode == "modify" then
		menu.selectableships = {}
		if menu.modeparam[1] then
			for _, ship in pairs(menu.modeparam[2]) do
				--print("adding " .. ffi.string(C.GetComponentName(ship)) .. " " .. tostring(ship) .. " to list of ships.")
				table.insert(menu.selectableships, ship)
			end
			for i = #menu.selectableships, 1, -1 do
				if ffi.string(C.GetOwnerDetails(menu.selectableships[i]).factionID) ~= "player" then
					table.remove(menu.selectableships, i)
				elseif menu.selectableships[i] == C.GetPlayerOccupiedShipID() then
					menu.object = menu.selectableships[i]
				end
			end
		else
			Helper.ffiVLA(menu.selectableships, "UniverseID", C.GetNumDockedShips, C.GetDockedShips, menu.container, "player")
			for i = #menu.selectableships, 1, -1 do
				if not C.CanContainerEquipShip(menu.container, menu.selectableships[i]) then
					table.remove(menu.selectableships, i)
				elseif ffi.string(C.GetOwnerDetails(menu.selectableships[i]).factionID) ~= "player" then
					table.remove(menu.selectableships, i)
				elseif menu.selectableships[i] == C.GetPlayerOccupiedShipID() then
					menu.object = menu.selectableships[i]
				end
			end
		end

		table.sort(menu.selectableships, Helper.sortUniverseIDName)

		if not menu.object then
			menu.object = menu.selectableships[1]
		end
		menu.macro = ""

		menu.class = ""
		if menu.object then
			menu.class = ffi.string(C.GetComponentClass(menu.object))

			local fficurrentloadoutstats = C.GetCurrentLoadoutStatistics(menu.object)
			menu.initialLoadoutStatistics = Helper.convertLoadoutStats(fficurrentloadoutstats)
		else
			menu.object = 0
		end

		menu.selectableshipsbyclass = {}
		for _, ship in ipairs(menu.selectableships) do
			local class = ffi.string(C.GetComponentClass(ship))
			if menu.selectableshipsbyclass[class] then
				table.insert(menu.selectableshipsbyclass[class], ship)
			else
				menu.selectableshipsbyclass[class] = { ship }
			end
		end

		menu.prepareModWares()
	end
	
	menu.searchtext = ""
	menu.loadoutName = ""
	if (menu.mode == "purchase") or (menu.mode == "upgrade") then
		menu.upgradetypeMode = "engine"
	elseif menu.mode == "modify" then
		menu.upgradetypeMode = "shipmods"
	end
	menu.currentSlot = 1
	menu.loadouts = {}

	menu.modCategory = "basic"

	menu.topRows = {}
	menu.selectedRows = {}
	menu.selectedCols = {}

	local upgradeplan, crew
	if state then
		upgradeplan, crew = menu.onRestoreState(state)
	end
	
	menu.displayMainFrame()

	AddUITriggeredEvent(menu.name, menu.upgradetypeMode)

	menu.getDataAndDisplay(upgradeplan, crew, nil, true)
	
	RegisterAddonBindings("ego_detailmonitor", "undo")
	Helper.setKeyBinding(menu, menu.hotkey)
end

function menu.onShowMenuSound()
	if not C.IsNextStartAnimationSkipped(false) then
		PlaySound("ui_config_ship_open")
	else
		PlaySound("ui_menu_changed")
	end
end

function menu.displayMainFrame()
	Helper.removeAllWidgetScripts(menu, config.mainLayer)

	menu.mainFrame = Helper.createFrameHandle(menu, {
		layer = config.mainLayer,
		standardButtons = { back = true, close = true },
		width = Helper.viewWidth,
		height = Helper.viewHeight,
		x = 0,
		y = 0,
	})

	-- title bar
	menu.createTitleBar(menu.mainFrame)

	-- construction map
	menu.mainFrame:addRenderTarget({width = menu.mapData.width, height = menu.mapData.height, x = menu.mapData.offsetX, y = menu.mapData.offsetY, scaling = false, alpha = 100})

	menu.mainFrame:display()
end

function menu.displayLeftBar(frame)
	local leftBar = config.leftBar
	if menu.mode == "modify" then
		leftBar = config.leftBarMods
	end
	local spacingHeight = menu.frameworkData.sidebarWidth / 4

	local ftable = frame:addTable(2, { tabOrder = 2, width = menu.frameworkData.sidebarWidth + menu.frameworkData.scaleWidth + Helper.borderSize, height = 0, x = menu.frameworkData.offsetX, y = menu.frameworkData.offsetY, scaling = false, reserveScrollBar = false })
	ftable:setColWidth(1, menu.frameworkData.scaleWidth)

	local found = true
	for _, entry in ipairs(leftBar) do
		if entry.spacing then
			local row = ftable:addRow(false, { fixed = true, bgColor = Helper.color.transparent })
			row[1]:setColSpan(2):createIcon("mapst_seperator_line", { width = menu.frameworkData.sidebarWidth + menu.frameworkData.scaleWidth + Helper.borderSize, height = spacingHeight })
		else
			local condition = true
			if entry.iscapship ~= nil then
				local iscapship
				if menu.container then
					iscapship = GetComponentData(menu.container, "isshipyard") and not C.IsComponentClass(menu.container, "ship")
				else
					iscapship = C.IsComponentClass(menu.object, "ship_l") or C.IsComponentClass(menu.object, "ship_xl")
				end
				condition = entry.iscapship == iscapship
			end

			if condition then
				local active = false
				local selected = entry.mode == menu.upgradetypeMode
				local prevSelected = entry.mode == menu.prevUpgradetypeMode
				local missing = false
				local overrideMode, overrideSlot
				local count, total = 0, 0

				local upgradetype = Helper.findUpgradeType(entry.mode)

				if menu.mode == "modify" then
					if entry.upgrademode == "paint" then
						active = true
					elseif (entry.upgrademode == "ship") or (menu.slots[entry.upgrademode] and (menu.slots[entry.upgrademode].count > 0)) then
						active = not menu.modeparam[1]
					end
				elseif entry.mode == "repair" then
					if #menu.damagedcomponents > 0 then
						active = true
						total = #menu.damagedcomponents
						if menu.repairplan and menu.repairplan[tostring(menu.object)] then
							for componentidstring in pairs(menu.repairplan[tostring(menu.object)]) do
								count = count + 1
							end
						end
					end
				elseif (entry.mode == "enginegroup") or (entry.mode == "turretgroup") then
					if next(menu.groups) then
						active = true
						for _, upgradetype in ipairs(Helper.upgradetypes) do
							if upgradetype.supertype == "group" then
								for slot, group in pairs(menu.upgradeplan[upgradetype.type]) do
									if (menu.groups[slot][upgradetype.grouptype].total > 0) then
										if upgradetype.mergeslots then
											count = count + ((menu.upgradeplan[upgradetype.type][slot].count > 0) and 1 or 0)
											total = total + 1
										else
											count = count + menu.upgradeplan[upgradetype.type][slot].count
											total = total + menu.groups[slot][upgradetype.grouptype].total
										end
									end
								end
							end
						end
					end
					for _, upgradetype in ipairs(Helper.upgradetypes) do
						if upgradetype.supertype == "group" then
							if upgradetype.allowempty == false then
								for slot, group in pairs(menu.upgradeplan[upgradetype.type]) do
									if (group.macro == "") and (menu.groups[slot][upgradetype.grouptype].total > 0) then
										missing = true
										break
									end
								end
							end
						end
					end
				elseif entry.mode == "software" then
					if menu.software[entry.mode] and (#menu.software[entry.mode] > 0) then
						active = true
						total = #menu.software[entry.mode]
						for slot, ware in ipairs(menu.upgradeplan[entry.mode]) do
							if ware ~= "" then
								count = count + 1
							end
						end
						for slot, slotdata in ipairs(menu.software[upgradetype.type]) do
							if #slotdata.possiblesoftware > 0 then
								if (slotdata.defaultsoftware ~= 0) and (menu.upgradeplan[upgradetype.type][slot] == "") then
									missing = true
									break
								end
							end
						end
					end
				elseif entry.mode == "consumables" then
					for _, upgradetype in ipairs(Helper.upgradetypes) do
						if upgradetype.supertype == "ammo" then
							if next(menu.ammo[upgradetype.type]) then
								local ammocount, capacity = menu.getAmmoUsage(upgradetype.type)
								for macro, amount in pairs(menu.upgradeplan[upgradetype.type]) do
									if (ammocount > 0) or ((capacity > 0) and menu.isAmmoCompatible(upgradetype.type, macro)) then
										active = true
										count = count + ammocount
										total = total + capacity
										break
									end
								end

								if ((ammocount > 0) or (capacity > 0)) and active then
									if ammocount > capacity then
										missing = true
									end
								end
							end
						end
					end
				elseif entry.mode == "crew" then
					if menu.crew.capacity > 0 then
						active = true
						count = menu.crew.total + menu.crew.hired + (menu.captainSelected and 1 or 0)
						total = menu.crew.capacity + 1
						if not menu.captainSelected then
							missing = true
						end
						if #menu.crew.unassigned > 0 then
							missing = true
						end
					end
				elseif upgradetype and (upgradetype.supertype == "macro") then
					if menu.slots[entry.mode] and (menu.slots[entry.mode].count > 0) then
						active = true
						total = menu.slots[entry.mode].count
						for slot, macro in ipairs(menu.upgradeplan[entry.mode]) do
							if macro ~= "" then
								count = count + 1
							end
						end
					else
						-- override engine category to point to group with engines
						if upgradetype.type == "engine" then
							if next(menu.groups) then
								active = true
								overrideMode = "enginegroup"
								for slot, groupdata in pairs(menu.groups) do
									if groupdata[upgradetype.type].total > 0 then
										overrideSlot = slot
										break
									end
								end
							end
						end
					end
					if upgradetype.allowempty == false then
						for slot, macro in pairs(menu.upgradeplan[upgradetype.type]) do
							if macro == "" then
								missing = true
								break
							end
						end
					end
				else
					if menu.slots[entry.mode] and (#menu.slots[entry.mode] > 0) then
						active = true
						total = #menu.slots[entry.mode]
						for slot, macro in ipairs(menu.upgradeplan[entry.mode]) do
							if macro ~= "" then
								count = count + 1
							end
						end
					end
					if upgradetype.allowempty == false then
						for slot, macro in pairs(menu.upgradeplan[upgradetype.type]) do
							if macro == "" then
								missing = true
								break
							end
						end
					end
				end

				if menu.mode == "purchase" then
					active = active and (menu.validLicence == true)
				end

				-- if nothing selected yet, select this one if active
				if (not found) and active then
					found = true
					selected = true
					menu.upgradetypeMode = entry.mode
					menu.selectMapMacroSlot()
				end

				-- if selected, but not active, select next active entry
				if selected and (not active) then
					found = false
					selected = false
				end

				local color = Helper.color.white
				if active then
					if missing then
						color = Helper.color.red
					end
				else
					color = Helper.color.grey
				end

				local row = ftable:addRow(active, { fixed = true, bgColor = Helper.color.transparent })
				if total > 0 then
					local height = math.floor((menu.frameworkData.sidebarWidth - 2 * menu.scaleSize) * count / total)
					row[1]:createIcon("solid", { cellBGColor = active and Helper.defaultSimpleBackgroundColor or Helper.color.darkgrey, color = Helper.color.white, width = menu.scaleSize, height = height, x = menu.scaleSize, y = (menu.frameworkData.sidebarWidth - height - 2 * menu.scaleSize) / 2 })
				else
					row[1]:createText("", { cellBGColor = active and Helper.defaultSimpleBackgroundColor or Helper.color.darkgrey, x = 0, y = 0 })
				end

				if selected then
					menu.selectedRows.left = row.index
				elseif prevSelected then
					menu.selectedRows.left = row.index
				end
				row[2]:createButton({ active = active, height = menu.frameworkData.sidebarWidth, mouseOverText = entry.name, bgColor = selected and Helper.defaultArrowRowBackgroundColor or Helper.defaultButtonBackgroundColor }):setIcon(entry.icon, { color = color })
				row[2].handlers.onClick = function () return menu.buttonLeftBar(entry.mode, row.index, overrideMode, overrideSlot) end
			else
				if entry.mode == menu.upgradetypeMode then
					found = false
				end
			end
		end
	end
	ftable:setTopRow(menu.topRows.left)
	ftable:setSelectedRow(menu.selectedRows.left)
	menu.topRows.left = nil
	menu.selectedRows.left = nil
end

function menu.getPresetLoadouts()
	menu.loadouts = {}
	if not menu.isReadOnly then
		if ((menu.mode == "purchase") and (menu.macro ~= "")) or ((menu.mode == "upgrade") and (menu.object ~= 0)) then
			local n = C.GetNumLoadoutsInfo(menu.object, menu.macro)
			local buf = ffi.new("UILoadoutInfo[?]", n)
			n = C.GetLoadoutsInfo(buf, n, menu.object, menu.macro)
			for i = 0, n - 1 do
				local id = ffi.string(buf[i].id)
				local active = C.CanBuildLoadout(menu.container, menu.object, menu.macro, id)
				table.insert(menu.loadouts, { id = id, name = ffi.string(buf[i].name), icon = ffi.string(buf[i].iconid), deleteable = buf[i].deleteable, active = active, mouseovertext = active and "" or ReadText(1026, 8011) })
			end
		end
		table.sort(menu.loadouts, function (a, b) return a.name < b.name end)
		table.insert(menu.loadouts, 1, { id = "empty", name = ReadText(1001, 7941), icon = "", deleteable = false, preset = 0 })
		table.insert(menu.loadouts, 2, { id = "low", name = ReadText(1001, 7910), icon = "", deleteable = false, preset = 0.1 })
		table.insert(menu.loadouts, 3, { id = "medium", name = ReadText(1001, 7911), icon = "", deleteable = false, preset = 0.5 })
		table.insert(menu.loadouts, 4, { id = "high", name = ReadText(1001, 7912), icon = "", deleteable = false, preset = 1.0 })
	end
end

function menu.getDataAndDisplay(upgradeplan, crew, newedit, firsttime)
	-- get preset loadouts
	menu.getPresetLoadouts()
	menu.refreshTitleBar()

	-- init upgradeplan
	menu.upgradeplan = {}
	menu.upgradewares = {}
	for _, upgradetype in ipairs(Helper.upgradetypes) do
		menu.upgradeplan[upgradetype.type] = {}
		if upgradetype.supertype ~= "group" then
			menu.upgradewares[upgradetype.type] = {}
		end
	end

	-- assemble possible upgrades (wares, macros, stock)
	if (not menu.isReadOnly) and ((menu.mode ~= "modify") or (not menu.modeparam[1])) then
		if menu.containerowner ~= "player" then
			local n = C.GetNumAvailableEquipment(menu.container, "")
			local buf = ffi.new("EquipmentWareInfo[?]", n)
			n = C.GetAvailableEquipment(buf, n, menu.container, "")
			for i = 0, n - 1 do
				local entry = {}
				entry.ware = ffi.string(buf[i].ware)
				entry.macro = ffi.string(buf[i].macro)
				entry.objectamount = 0
				entry.isFromShipyard = true
				
				local type = ffi.string(buf[i].type) --DON'T USE RESERVED WORDS AS VARIABLES Florian, I KNOW WHAT YOU DID HERE
				if (type == "lasertower") or (type == "satellite") or (type == "mine") or (type == "navbeacon") or (type == "resourceprobe") then
					type = "deployable"
				end
				if type == "" then
					DebugError(string.format("Could not find upgrade type for the equipment ware: '%s'. Check the ware tags. [Florian]", entry.ware))
				else
					if menu.upgradewares[type] then
						table.insert(menu.upgradewares[type], entry)
					else
						menu.upgradewares[type] = { entry }
					end
				end
			end
		else
            local numblueprints = C.GetNumBlueprints("", "", "")
            local blueprints = ffi.new("UIBlueprint[?]", numblueprints)
            numblueprints = C.GetBlueprints(blueprints, numblueprints, "", "", "")
            local blueprintlist = {}
            for i = 0, numblueprints-1 do
                blueprintlist[ffi.string(blueprints[i].ware)] = true
            end
		
			local numwares = C.GetNumWares("equipment", false, "", "") --no licenseowner returns ALL wares, as it should!
			local wares = ffi.new("const char*[?]", numwares)
			numwares = C.GetWares(wares, numwares, "equipment", false, "", "") --noblueprint for last param for release
			for i = 0, numwares - 1 do
				local entry = {}
				entry.ware = ffi.string(wares[i])
				entry.macro = GetWareData(entry.ware, "component")
				entry.objectamount = 0
				entry.isFromShipyard = true
				
				local wareTags = GetWareData(entry.ware, "tags")
				local wareType = ""
				local requiresBlueprint = true
				
				for tag,_ in pairs(wareTags) do
					if tag == "lasertower" or tag == "satellite" or tag == "mine" or tag == "navbeacon" or tag == "resourceprobe" then
						wareType = "deployable"
						requiresBlueprint = false
					elseif tag == "thruster" or tag == "countermeasure" or tag == "engine" or tag == "missile" or tag == "shield" or tag == "weapon" or tag == "turret" or tag == "drone" or tag == "software" then
						wareType = tag
						requiresBlueprint = tag ~= "countermeasure" and tag ~= "drone" and tag ~= "software"
					end
				end

				if wareType == "" then
					DebugError(string.format("Could not find upgrade type for the equipment ware: '%s'. Check the ware tags. [Florian]", entry.ware))
				else
					if blueprintlist[entry.ware] or not requiresBlueprint then
						if menu.upgradewares[wareType] then
							table.insert(menu.upgradewares[wareType], entry)
						else
							menu.upgradewares[wareType] = { entry }
						end
					end
				end
			end
		end
	end

	-- assemble available slots/ammo/software
	menu.groups = {}
	menu.slots = {}
	menu.ammo = { missile = {}, drone = {}, deployable = {}, countermeasure = {}, }
	menu.software = {}
	menu.crew = { 
		total = 0,
		capacity = 0,
		unassigned = {},
		roles = {},
		hired = 0,
		hireddetails = {},
		transferdetails = {},
		fired = {},
		ware = "crew",
		availableworkforce = 0,
		maxavailableworkforce = 0,
	}
	if (not menu.isReadOnly) and ((menu.mode ~= "modify") or (not menu.modeparam[1])) then
		local workforceinfo = C.GetWorkForceInfo(menu.container, "")
		menu.crew.availableworkforce = workforceinfo.available
		menu.crew.maxavailableworkforce = workforceinfo.maxavailable
	end

	if menu.object ~= 0 then
		local paintmod = ffi.new("UIPaintMod")
		if C.GetInstalledPaintMod(menu.object, paintmod) then
			menu.installedPaintMod = {}
			menu.installedPaintMod.name = ffi.string(paintmod.Name)
			menu.installedPaintMod.ware = ffi.string(paintmod.Ware)
			menu.installedPaintMod.quality = paintmod.Quality
			menu.installedPaintMod.amount = paintmod.Amount
		end
	elseif menu.macro ~= "" then
		local paintmod = ffi.new("UIPaintMod")
		if C.GetPlayerPaintThemeMod(menu.object, menu.macro, paintmod) then
			menu.installedPaintMod = {}
			menu.installedPaintMod.name = ffi.string(paintmod.Name)
			menu.installedPaintMod.ware = ffi.string(paintmod.Ware)
			menu.installedPaintMod.quality = paintmod.Quality
			menu.installedPaintMod.amount = paintmod.Amount
		end
	end

	if menu.mode == "purchase" then
		if menu.macro ~= "" then
			menu.prepareMacroUpgradeSlots(menu.macro)
			menu.prepareMacroCrewInfo(menu.macro)
		end
	elseif (menu.mode == "upgrade") or (menu.mode == "modify") then
		if menu.object ~= 0 then
			menu.prepareComponentUpgradeSlots(menu.object)
			menu.prepareComponentCrewInfo(menu.object)

			menu.checkCurrentBuildTasks()
		end
	end
	if menu.mode == "modify" then
		if menu.object ~= 0 then
			menu.shieldgroups = {}
			local n = C.GetNumShieldGroups(menu.object)
			local buf = ffi.new("ShieldGroup[?]", n)
			n = C.GetShieldGroups(buf, n, menu.object)
			for i = 0, n - 1 do
				local entry = {}
				entry.context = buf[i].context
				entry.group = ffi.string(buf[i].group)
				entry.component = buf[i].component

				table.insert(menu.shieldgroups, entry)
			end
			for i, entry in ipairs(menu.shieldgroups) do
				if (entry.context == menu.object) and (entry.group == "") then
					menu.shieldgroups.hasMainGroup = true
					-- force maingroup to first index
					table.insert(menu.shieldgroups, 1, entry)
					table.remove(menu.shieldgroups, i + 1)
					break
				end
			end
		end

		if menu.installedPaintMod then
			if menu.modwares["paint"] then
				local found = false
				for i, mod in ipairs(menu.modwares["paint"]) do
					if mod.ware == menu.installedPaintMod.ware then
						found = true
						break
					end
				end
				if not found then
					table.insert(menu.modwares["paint"], menu.installedPaintMod)
				end
			else
				menu.modwares["paint"] = { menu.installedPaintMod }
			end
		end
	end

	menu.groups = {}
	if ((menu.mode == "purchase") and (menu.macro ~= "")) or (((menu.mode == "upgrade") or (menu.mode == "modify")) and (menu.object ~= 0)) then
		local n = C.GetNumUpgradeGroups(menu.object, menu.macro)
		local buf = ffi.new("UpgradeGroup[?]", n)
		n = C.GetUpgradeGroups(buf, n, menu.object, menu.macro)
		for i = 0, n - 1 do
			if (ffi.string(buf[i].path) ~= "..") or (ffi.string(buf[i].group) ~= "") then
				table.insert(menu.groups, { path = ffi.string(buf[i].path), group = ffi.string(buf[i].group) })
				local group = menu.groups[#menu.groups]
				for j, upgradetype in ipairs(Helper.upgradetypes) do
					if upgradetype.supertype == "group" then
						local groupinfo = C.GetUpgradeGroupInfo(menu.object, menu.macro, group.path, group.group, upgradetype.grouptype)
						local currentmacro = ffi.string(groupinfo.currentmacro)
						menu.groups[#menu.groups][upgradetype.grouptype] = { count = groupinfo.count, total = groupinfo.total, slotsize = ffi.string(groupinfo.slotsize), currentcomponent = (groupinfo.currentcomponent ~= 0) and groupinfo.currentcomponent or nil, currentmacro = currentmacro, possiblemacros = {} }
						menu.upgradeplan[upgradetype.type][#menu.groups] = { macro = ffi.string(groupinfo.currentmacro), count = groupinfo.count, path = group.path, group = group.group }
						if currentmacro ~= "" then
							local i = menu.findUpgradeMacro(upgradetype.grouptype, currentmacro)
							if i then
								menu.upgradewares[upgradetype.grouptype][i].objectamount = menu.upgradewares[upgradetype.grouptype][i].objectamount + groupinfo.count
							else
								table.insert(menu.upgradewares[upgradetype.grouptype], { ware = GetMacroData(currentmacro, "ware"), macro = currentmacro, objectamount = groupinfo.count, isFromShipyard = false })
							end
						end
					end
				end
			end
		end
	end

	-- assemble possible upgrades per slot
	for type, slots in pairs(menu.slots) do
		local upgradetype = Helper.findUpgradeType(type)
		if upgradetype.supertype == "macro" then
			slots.count = #slots
			for i, slot in ipairs(slots) do
				if menu.upgradewares[type] then
					for _, upgradeware in ipairs(menu.upgradewares[type]) do
						if C.IsUpgradeMacroCompatible(menu.object, 0, menu.macro, false, type, i, upgradeware.macro) then
							table.insert(slot.possiblemacros, upgradeware.macro)
						elseif slot.currentmacro and (slot.currentmacro == upgradeware.macro) then
							table.insert(slot.possiblemacros, upgradeware.macro)
						end
					end
				end

				local groupinfo = C.GetUpgradeSlotGroup(menu.object, menu.macro, type, i)
				if (ffi.string(groupinfo.path) ~= "..") or (ffi.string(groupinfo.group) ~= "") then
					slot.isgroup = true
					slots.count = slots.count - 1
				end
			end
		elseif upgradetype.supertype == "virtualmacro" then
			for i, slot in ipairs(slots) do
				if menu.upgradewares[type] then
					for _, upgradeware in ipairs(menu.upgradewares[type]) do
						if C.IsVirtualUpgradeMacroCompatible(menu.object, menu.macro, type, i, upgradeware.macro) then
							table.insert(slot.possiblemacros, upgradeware.macro)
						end
					end
				end
			end
		end
	end

	-- assemble possible upgrades per group
	for i, group in ipairs(menu.groups) do
		for j, upgradetype in ipairs(Helper.upgradetypes) do
			if upgradetype.supertype == "group" then
				local wares = menu.upgradewares[upgradetype.grouptype] or {}
				for _, upgradeware in ipairs(wares) do
					if upgradeware.macro ~= "" then
						if C.IsUpgradeGroupMacroCompatible(menu.object, menu.macro, group.path, group.group, upgradetype.grouptype, upgradeware.macro) then
							table.insert(menu.groups[i][upgradetype.grouptype].possiblemacros, upgradeware.macro)
						end
					end
				end
			end
		end
	end

	-- assemble possible ammo
	for _, upgradetype in ipairs(Helper.upgradetypes) do
		if upgradetype.supertype == "ammo" then
			if ((menu.mode == "purchase") and (menu.macro ~= "")) or (((menu.mode == "upgrade") or (menu.mode == "modify")) and (menu.object ~= 0)) then
				if menu.upgradewares[upgradetype.type] then
					for _, upgradeware in ipairs(menu.upgradewares[upgradetype.type]) do
						if not menu.ammo[upgradetype.type][upgradeware.macro] then
							menu.ammo[upgradetype.type][upgradeware.macro] = 0
							menu.upgradeplan[upgradetype.type][upgradeware.macro] = 0
						end
					end
				end
			end
		end
	end

	-- assemble possible software per slot
	for type, softwarelist in pairs(menu.software) do
		for j, software in ipairs(softwarelist) do
			software.defaultsoftware = 0
			software.possiblesoftware = {}
			local n = C.GetNumSoftwarePredecessors(software.maxsoftware)
			local buf = ffi.new("const char*[?]", n)
			n = C.GetSoftwarePredecessors(buf, n, software.maxsoftware)
			for i = 0, n - 1 do
				local ware = ffi.string(buf[i])
				if (not menu.isReadOnly) or (ware == software.currentsoftware) then
					table.insert(software.possiblesoftware, ware)
				end
				if C.IsSoftwareDefault(menu.object, menu.macro, ware) then
					software.defaultsoftware = i + 1
					if software.currentsoftware == "" then
						if upgradeplan or (menu.mode ~= "purchase") or (menu.upgradeplan[type][j] ~= "") then
							menu.upgradeplan[type][j] = ware
						end
					end
				end
			end
			if (not menu.isReadOnly) or (software.maxsoftware == software.currentsoftware) then
				table.insert(software.possiblesoftware, software.maxsoftware)
			end
			if C.IsSoftwareDefault(menu.object, menu.macro, software.maxsoftware) then
				software.defaultsoftware = #software.possiblesoftware
				if software.currentsoftware == "" then
					if upgradeplan or (menu.mode ~= "purchase") or (menu.upgradeplan[type][j] ~= "") then
						menu.upgradeplan[type][j] = software.maxsoftware
					end
				end
			end
		end
	end

	if upgradeplan then
		for type, upgradelist in pairs(menu.upgradeplan) do
			local upgradetype = Helper.findUpgradeType(type)
			for key, upgrade in pairs(upgradelist) do
				if upgradetype.supertype == "group" then
					local found = false
					for key2, upgrade2 in pairs(upgradeplan[type]) do
						if (upgrade2.path == upgrade.path) and (upgrade2.group == upgrade.group) then
							found = true
							menu.upgradeplan[type][key].macro = upgrade2.macro or ""
							menu.upgradeplan[type][key].count = upgrade2.count or 0
							break
						end
					end
					if not found then
						menu.upgradeplan[type][key].macro = ""
						menu.upgradeplan[type][key].count = 0
					end
				else
					local newvalue
					if (upgradetype.supertype == "macro") or (upgradetype.supertype == "virtualmacro") then
						newvalue = upgradeplan[type][key] or ""
					elseif (upgradetype.supertype == "software") then
						if not upgradeplan[type][key] then
							if menu.upgradeplan[type][key] ~= "" then
								if menu.software[type][key].defaultsoftware ~= 0 then
									newvalue = menu.software[type][key].possiblesoftware[menu.software[type][key].defaultsoftware]
								else
									newvalue = ""
								end
							else
								newvalue = ""
							end
						else
							newvalue = upgradeplan[type][key]
						end
					elseif (upgradetype.supertype == "ammo") then
						newvalue = upgradeplan[type][key] or 0
					end
					menu.upgradeplan[type][key] = newvalue
				end
			end
		end
	end
	if crew then
		menu.crew.hired = crew.hired
		menu.crew.fired = crew.fired
		menu.crew.unassigned = crew.unassigned
		for _, entry in ipairs(crew.hireddetails) do
			table.insert(menu.crew.hireddetails, entry)
		end
		for i, entry in ipairs(crew.roles) do
			menu.crew.roles[i].wanted = entry.wanted
			for j, tier in ipairs(entry.tiers) do
				menu.crew.roles[i].tiers[j].wanted = tier.wanted
			end
		end
	end
	
	if menu.holomap and (menu.holomap ~= 0) then
		if ((menu.mode == "purchase") and (menu.macro ~= "")) or (((menu.mode == "upgrade") or (menu.mode == "modify")) and (menu.object ~= 0)) then
			if (not newedit) and upgradeplan then
				Helper.callLoadoutFunction(menu.upgradeplan, nil, function (loadout, _) return C.UpdateObjectConfigurationMap(menu.holomap, menu.object, 0, menu.macro, false, loadout) end)
			else
				menu.currentIdx = menu.currentIdx + 1
				Helper.callLoadoutFunction(menu.upgradeplan, nil, function (loadout, _) return C.ShowObjectConfigurationMap(menu.holomap, menu.object, 0, menu.macro, false, loadout) end)
			end
			if menu.installedPaintMod then
				C.SetMapPaintMod(menu.holomap, menu.installedPaintMod.ware)
			end
			menu.selectMapMacroSlot()
			if menu.mapstate then
				C.SetMapState(menu.holomap, menu.mapstate)
				menu.mapstate = nil
			end
		else
			C.ClearMapBehaviour(menu.holomap)
		end
	end

	menu.displayMenu(firsttime)
end

function menu.checkCurrentBuildTasks()
	menu.tasks = {}
	local n = C.GetNumOrders(menu.object)
	local buf = ffi.new("Order[?]", n)
	n = C.GetOrders(buf, n, menu.object)
	for i = 0, n - 1 do
		if ffi.string(buf[i].orderdef) == "Equip" then
			menu.tasks[tostring(menu.object)] = true
			break
		end
	end
end

function menu.displayAmmoSlot(ftable, type, macro, total, capacity, first)
	if (menu.upgradeplan[type][macro] > 0) or menu.isAmmoCompatible(type, macro) then
		local name, infolibrary = GetMacroData(macro, "name", "infolibrary")
		AddKnownItem(infolibrary, macro)

		local difference = menu.upgradeplan[type][macro] - menu.ammo[type][macro]
		local color = Helper.color.white
		if difference < 0 then
			color = Helper.color.red
		elseif difference > 0 then
			color = Helper.color.green
		end

		local errorcase = total > capacity

		local scale = {
			min            = 0,
			max            = math.max(capacity, menu.upgradeplan[type][macro]),
			maxSelect      = errorcase and menu.upgradeplan[type][macro] or math.min(capacity, menu.upgradeplan[type][macro] + capacity - total),
			start          = menu.upgradeplan[type][macro],
			step           = 1,
			suffix         = "",
			exceedMaxValue = false,
			readOnly       = menu.isReadOnly,
		}
		
		local price
		local j = menu.findUpgradeMacro(type, macro)
		if j then
			local upgradeware = menu.upgradewares[type][j]
			if not menu.isReadOnly then
				price = tonumber(C.GetBuildWarePrice(menu.container, upgradeware.ware))
			end

			if (not errorcase) and (not upgradeware.isFromShipyard) then
				scale.maxSelect = math.min(scale.maxSelect, upgradeware.objectamount)
			end
		end

		if not first then
			local row = ftable:addRow(false, { scaling = true, bgColor = Helper.color.transparent })
			row[1]:setColSpan(11):createText("", { fontsize = 1, minRowHeight = Helper.standardTextHeight / 2 })
		end

		local row = ftable:addRow((type ~= "countermeasure") and { type = type, name = name, macro = macro } or true, { scaling = true, borderBelow = false, bgColor = Helper.color.transparent })
		row[1]:setColSpan(price and 8 or 11):setBackgroundColSpan(11):createBoxText(name)
		if price then
			row[9]:setColSpan(3):createBoxText(ConvertMoneyString(price, false, true, 0, true, false) .. " " .. ReadText(1001, 101), { halign = "right" })
		end

		local row = ftable:addRow(true, { scaling = true })
		row[1]:setColSpan(11):createSliderCell({ width = slidercellWidth, height = Helper.standardTextHeight, valueColor = Helper.color.slidervalue, min = scale.min, max = scale.max, maxSelect = scale.maxSelect, start = scale.start, step = scale.step, suffix = scale.suffix, exceedMaxValue = scale.exceedMaxValue, readOnly = scale.readOnly }):setText(ReadText(1001, 1202), { color = color })
		row[1].handlers.onSliderCellChanged = function (_, ...) return menu.slidercellSelectAmount(type, nil, macro, row.index, ...) end
		row[1].handlers.onRightClick = function (...) return menu.buttonInteract({ type = type, name = name, macro = macro }, ...) end
	end
end

function menu.displayCrewSlot(ftable, idx, data, buttonWidth, price, first)
	if data.canhire then
		local color = Helper.color.white
		local capacity = menu.crew.capacity
		for _, entry in ipairs(menu.crew.roles) do
			if entry.id ~= data.id then
				capacity = capacity - entry.wanted
			end
		end

		local scale
		if menu.mode == "purchase" then
			scale = {
				min            = 0,
				max            = menu.crew.capacity,
				maxSelect      = capacity,
				start          = data.wanted,
				step           = 1,
				suffix         = "",
				exceedMaxValue = false,
				readOnly       = menu.isReadOnly,
			}
		else
			scale = {
				min            = 0,
				max            = menu.crew.capacity,
				maxSelect      = math.min(data.wanted + #menu.crew.unassigned + menu.crew.availableworkforce, capacity),
				start          = data.wanted,
				step           = 1,
				suffix         = "",
				exceedMaxValue = false,
				readOnly       = menu.isReadOnly,
			}
		end

		if not first then
			local row = ftable:addRow(false, { scaling = true, bgColor = Helper.color.transparent })
			row[1]:setColSpan(11):createText("", { fontsize = 1, minRowHeight = Helper.standardTextHeight / 2 })
		end

		local row = ftable:addRow(true, { scaling = true, borderBelow = false, bgColor = Helper.color.transparent })
		row[1]:setColSpan(menu.isReadOnly and 11 or 8):setBackgroundColSpan(11):createBoxText(data.name)
		if not menu.isReadOnly then
			row[9]:setColSpan(3):createBoxText(ConvertMoneyString(price, false, true, 0, true, false) .. " " .. ReadText(1001, 101), { halign = "right" })
		end

		local row = ftable:addRow(true, { scaling = true })
		row[1]:setColSpan(11):createSliderCell({ width = slidercellWidth, height = Helper.standardTextHeight, valueColor = Helper.color.slidervalue, min = scale.min, max = scale.max, maxSelect = scale.maxSelect, start = scale.start, step = scale.step, suffix = scale.suffix, exceedMaxValue = scale.exceedMaxValue, readOnly = scale.readOnly }):setText(ReadText(1001, 1202), { color = color })
		row[1].handlers.onSliderCellChanged = function (_, ...) return menu.slidercellSelectCrewAmount(idx, 1, row.index, ...) end

		if menu.mode ~= "purchase" then
			for j, tier in ipairs(data.tiers) do
				if not tier.hidden then
					local scale = {
						min            = 0,
						max            = data.wanted,
						maxSelect      = tier.wanted,
						start          = tier.wanted,
						step           = 1,
						suffix         = "",
						exceedMaxValue = false,
						readOnly       = menu.isReadOnly,
					}

					local row = ftable:addRow(true, { scaling = true })
					row[1]:setColSpan(11):createSliderCell({ width = slidercellWidth, height = Helper.standardTextHeight, valueColor = Helper.color.slidervalue, min = scale.min, max = scale.max, maxSelect = scale.maxSelect, start = scale.start, step = scale.step, suffix = scale.suffix, exceedMaxValue = scale.exceedMaxValue, readOnly = scale.readOnly }):setText("  " .. tier.name, { color = color })
					row[1].handlers.onSliderCellChanged = function (_, ...) return menu.slidercellSelectCrewAmount(idx, j, row.index, ...) end
				end
			end
		end
	else
		local row = ftable:addRow(false, { scaling = true })
		row[1]:setColSpan(11):createText(data.name .. ": " .. data.total)
	end
end

function menu.displaySoftwareSlot(ftable, type, slot, slotdata)
	local plansoftware = menu.upgradeplan[menu.upgradetypeMode][slot]
	local name = GetWareData(slotdata.possiblesoftware[1], "factoryname")

	local row = ftable:addRow(false, { bgColor = Helper.defaultTitleBackgroundColor })
	if slotdata.defaultsoftware ~= 0 then
		local color = Helper.color.white
		if plansoftware == "" then
			color = Helper.color.red
		end

		row[1]:setColSpan(7):setBackgroundColSpan(11):createText(name, menu.subHeaderTextProperties)
		row[1].properties.color = color
		row[8]:setColSpan(4):createText(ReadText(1001, 8047), menu.subHeaderTextProperties)
		row[8].properties.color = color
		row[8].properties.halign = "right"
	else
		row[1]:setColSpan(11):createText(name, menu.subHeaderTextProperties)
	end

	for i, software in ipairs(slotdata.possiblesoftware) do
		if (menu.searchtext == "") or menu.filterUpgradeByText(software, menu.searchtext) then
			local haslicence, icon, overridecolor, mouseovertext = menu.checkLicence(software, nil, true)

			local price
			if not menu.isReadOnly then
				price = GetContainerWarePrice(ConvertStringTo64Bit(tostring(menu.container)), software, false)
			end

			local installcolor = Helper.color.white
			if software == slotdata.currentsoftware then
				if software ~= plansoftware then
					installcolor = Helper.color.red
				end
			elseif software == plansoftware then
				installcolor = Helper.color.green
			end

			local active = not menu.isReadOnly
			if software == plansoftware then
				if i == slotdata.defaultsoftware then
					active = false
				end
			end

			local name = GetWareData(software, "name")
			AddKnownItem("software", software)
			local row = ftable:addRow({ type = type, name = name, software = software }, { scaling = true, bgColor = Helper.color.transparent })
			row[1]:setColSpan(1):createCheckBox(software == plansoftware, { scaling = false, active = active, width = menu.rowHeight, height = menu.rowHeight })
			row[1].handlers.onClick = function () return menu.checkboxSelectSoftware(type, slot, software, row.index) end
			row[2]:setColSpan(price and 7 or 10):createText(name, { color = installcolor })
			if price then
				row[9]:setColSpan(3):createText(ConvertMoneyString(price, false, true, 0, true, false) .. " " .. ReadText(1001, 101), { halign = "right" })
			end
		end
	end
end

function menu.checkCurrentSlot(slots, slot)
	local upgradetype = Helper.findUpgradeType(menu.upgradetypeMode)
	if upgradetype then
		if menu.upgradetypeMode == "enginegroup" then
			if menu.groups[slot]["engine"].total == 0 then
				for i, upgradegroup in ipairs(menu.groups) do
					if upgradegroup["engine"].total > 0 then
						slot = i
						break
					end
				end
			end
		elseif menu.upgradetypeMode == "turretgroup" then
			if menu.groups[slot]["engine"].total > 0 then
				for i, upgradegroup in ipairs(menu.groups) do
					if upgradegroup["engine"].total == 0 then
						slot = i
						break
					end
				end
			end
		elseif (menu.upgradetypeMode ~= "consumables") and (menu.upgradetypeMode ~= "software") then
			if not upgradetype.mergeslots then
				if (not slots[slot]) or slots[slot].isgroup then
					for i, slot2 in ipairs(slots) do
						if not slot2.isgroup then
							slot = i
							break
						end
					end
				end
			else
				slot = 1
			end
		end
	end

	return slot
end

function menu.displaySlots(frame, firsttime)
	if menu.upgradetypeMode and ((menu.mode ~= "purchase") or menu.validLicence) then
		local upgradetype = Helper.findUpgradeType(menu.upgradetypeMode)
	
		local count, rowcount, slidercount = 1, 0, 0
		menu.groupedupgrades = {}

		local slots = {}
		if (menu.upgradetypeMode ~= "enginegroup") and (menu.upgradetypeMode ~= "turretgroup") and (menu.upgradetypeMode ~= "consumables") and (menu.upgradetypeMode ~= "crew") and (menu.upgradetypeMode ~= "repair") and (menu.upgradetypeMode ~= "software") then
			if (upgradetype.supertype == "macro") or (upgradetype.supertype == "virtualmacro") then
				slots = menu.slots[menu.upgradetypeMode] or {}
			end
		end

		menu.currentSlot = menu.checkCurrentSlot(slots, menu.currentSlot)

		if (menu.upgradetypeMode == "enginegroup") or (menu.upgradetypeMode == "turretgroup") then
			local upgradegroup = menu.groups[menu.currentSlot]
			if upgradegroup then
				for i, upgradetype2 in ipairs(Helper.upgradetypes) do
					local upgradegroupcount = 1
					if upgradetype2.supertype == "group" then
						menu.groupedupgrades[upgradetype2.grouptype] = {}
						for i, macro in ipairs(upgradegroup[upgradetype2.grouptype].possiblemacros) do
							if (menu.searchtext == "") or menu.filterUpgradeByText(macro, menu.searchtext) then
								local group = math.ceil(upgradegroupcount / 3)
								menu.groupedupgrades[upgradetype2.grouptype][group] = menu.groupedupgrades[upgradetype2.grouptype][group] or {}
								table.insert(menu.groupedupgrades[upgradetype2.grouptype][group], { macro = macro, icon = (C.IsIconValid("upgrade_" .. macro) and ("upgrade_" .. macro) or "upgrade_notfound"), name = GetMacroData(macro, "name") })
								upgradegroupcount = upgradegroupcount + 1
							end
						end

						if (not menu.isReadOnly) and upgradetype2.allowempty then
							local group = math.ceil(upgradegroupcount / 3)
							menu.groupedupgrades[upgradetype2.grouptype][group] = menu.groupedupgrades[upgradetype2.grouptype][group] or {}
							table.insert(menu.groupedupgrades[upgradetype2.grouptype][group], { macro = "", icon = "upgrade_empty", name = ReadText(1001, 7906) })
							upgradegroupcount = upgradegroupcount + 1
						end
					end
					if upgradegroupcount > 1 then
						slidercount = slidercount + 1
					end
					rowcount = rowcount + math.ceil((upgradegroupcount - 1) / 3)
				end
			end
		elseif (menu.upgradetypeMode == "repair") then
			for i, component in ipairs(menu.damagedcomponents) do
				local group = math.ceil(count / 3)
				--print("populating grouped upgrades. " .. tostring(GetComponentData(ConvertStringToLuaID(tostring(component)), "macro")) .. " of " .. ffi.string(C.GetComponentName(menu.object)) .. " is " .. tostring(100 - GetComponentData(ConvertStringToLuaID(tostring(component)), "hullpercent")) .. "% damaged.")
				local macro = GetComponentData(ConvertStringToLuaID(tostring(component)), "macro")
				menu.groupedupgrades[group] = menu.groupedupgrades[group] or {}
				if component == menu.object then
					table.insert(menu.groupedupgrades[group], { macro = macro, icon = (C.IsIconValid("ship_" .. macro) and ("ship_" .. macro) or "ship_notfound"), name = GetMacroData(macro, "name"), component = component })
				else
					table.insert(menu.groupedupgrades[group], { macro = macro, icon = (C.IsIconValid("upgrade_" .. macro) and ("upgrade_" .. macro) or "upgrade_notfound"), name = GetMacroData(macro, "name"), component = component })
				end
				count = count + 1
			end
			rowcount = rowcount + math.ceil(count / 3)
		elseif (menu.upgradetypeMode ~= "consumables") and (menu.upgradetypeMode ~= "crew") and (menu.upgradetypeMode ~= "software") then
			if #slots > 0 then
				if (upgradetype.supertype == "macro") or (upgradetype.supertype == "virtualmacro") then
					for i, macro in ipairs(slots[menu.currentSlot].possiblemacros) do
						if (menu.searchtext == "") or menu.filterUpgradeByText(macro, menu.searchtext) then
							local group = math.ceil(count / 3)
							menu.groupedupgrades[group] = menu.groupedupgrades[group] or {}
							table.insert(menu.groupedupgrades[group], { macro = macro, icon = (C.IsIconValid("upgrade_" .. macro) and ("upgrade_" .. macro) or "upgrade_notfound"), name = GetMacroData(macro, "name") })
							count = count + 1
						end
					end

					if (not menu.isReadOnly) and upgradetype.allowempty then
						local group = math.ceil(count / 3)
						menu.groupedupgrades[group] = menu.groupedupgrades[group] or {}
						table.insert(menu.groupedupgrades[group], { macro = "", icon = "upgrade_empty", name = ReadText(1001, 7906) })
						count = count + 1
					end
				end
			end
			rowcount = rowcount + math.ceil(count / 3)
		end

		if next(menu.groupedupgrades) then
			menu.groupedslots = {}
			if (menu.upgradetypeMode == "enginegroup") or (menu.upgradetypeMode == "turretgroup") then
				local groupcount = 1
				for i, upgradegroup in ipairs(menu.groups) do
					if (menu.upgradetypeMode == "enginegroup") == (upgradegroup["engine"].total > 0) then
						local group = math.ceil(groupcount / 9)
						menu.groupedslots[group] = menu.groupedslots[group] or {}
						table.insert(menu.groupedslots[group], {i, upgradegroup, groupcount})
						groupcount = groupcount + 1
					end
				end
			elseif menu.upgradetypeMode == "repair" then
				local slotcount = 1
				menu.repairslots = {}

				for i, component in ipairs(menu.damagedcomponents) do
					local group = math.ceil(slotcount / 3)
					local macro
					local loccomponent
					local slotnum = 0

					local found = false
					if component == menu.object then
						macro = GetComponentData(ConvertStringToLuaID(tostring(component)), "macro")
						loccomponent = component
						slotnum = i
						found = true
					else
						for _, upgradetype in ipairs(Helper.upgradetypes) do
							if upgradetype.supertype == "macro" then
								if menu.slots[upgradetype.type] then
									for j = 1, #menu.slots[upgradetype.type] do
										if menu.slots[upgradetype.type][j].component == component then
											macro = menu.slots[upgradetype.type][j].currentmacro
											loccomponent = menu.slots[upgradetype.type][j].component
											slotnum = j
											found = true
											break
										end
									end
								end
							end
						end
					end

					if found then
						--print("populating repair slots. registering " .. ffi.string(C.GetComponentName(loccomponent)) .. " at slot " .. tostring(slotnum) .. " for repair.")
						menu.repairslots[group] = menu.repairslots[group] or {}
						table.insert(menu.repairslots[group], {slotnum, macro, slotcount, loccomponent})
						slotcount = slotcount + 1
						found = false
					else
						print("ERROR: was unable to make a repair slot for " .. ffi.string(C.GetComponentName(component)) .. ".")
					end
				end
			elseif (menu.upgradetypeMode ~= "consumables") and (menu.upgradetypeMode ~= "software") then
				if not upgradetype.mergeslots then
					local slotcount = 1
					for i, slot in ipairs(slots) do
						if not slot.isgroup then
							local group = math.ceil(slotcount / 9)
							menu.groupedslots[group] = menu.groupedslots[group] or {}
							table.insert(menu.groupedslots[group], {i, slot, slotcount})
							slotcount = slotcount + 1
						end
					end
				end
			end
		end
		
		menu.rowHeight = math.max(23, Helper.scaleY(Helper.standardTextHeight))
		menu.extraFontSize = Helper.scaleFont(Helper.standardFont, Helper.standardFontSize)
		local maxSlotWidth = math.floor((menu.slotData.width - 8 * Helper.borderSize) / 9)

		local hasScrollbar = false
		local headerHeight = menu.titleData.height + #menu.groupedslots * (maxSlotWidth + Helper.borderSize) + menu.rowHeight + 2 * Helper.borderSize
		local boxTextHeight = math.ceil(C.GetTextHeight(" \n ", Helper.standardFont, menu.extraFontSize, 0)) + 2 * Helper.borderSize
		--[[ Keep for simpler debugging
			print((Helper.viewHeight - 2 * menu.slotData.offsetY) .. " vs " .. (headerHeight + rowcount * (3 * (maxSlotWidth + Helper.borderSize) + boxTextHeight) + slidercount * (menu.subHeaderRowHeight + Helper.borderSize)))
			print(headerHeight)
			print(boxTextHeight)
			print(rowcount .. " * " .. 3 * (maxSlotWidth + Helper.borderSize))
			print(slidercount .. " * " .. menu.subHeaderRowHeight + Helper.borderSize) --]]
		if (Helper.viewHeight - 2 * menu.slotData.offsetY) < (headerHeight + rowcount * (3 * (maxSlotWidth + Helper.borderSize) + boxTextHeight) + slidercount * (menu.subHeaderRowHeight + Helper.borderSize)) then
			hasScrollbar = true
		end

		local slotWidth = maxSlotWidth - math.floor((hasScrollbar and Helper.scrollbarWidth or 0) / 9)
		local extraPixels = (menu.slotData.width - 8 * Helper.borderSize) % 9
		local slotWidths = { slotWidth, slotWidth, slotWidth, slotWidth, slotWidth, slotWidth, slotWidth, slotWidth, slotWidth }
		if extraPixels > 0 then
			for i = 1, extraPixels do
				slotWidths[i] = slotWidths[i] + 1
			end
		end
		-- prevent negative column width
		if slotWidths[1] - menu.rowHeight - Helper.borderSize < 1 then
			slotWidths[1] = menu.rowHeight + Helper.borderSize + 1
		end
		local columnWidths = {}
		local maxColumnWidth = 0
		for i = 1, 3 do
			columnWidths[i] = slotWidths[(i - 1) * 3 + 1] + slotWidths[(i - 1) * 3 + 2] + slotWidths[(i - 1) * 3 + 3] + 2 * Helper.borderSize
			maxColumnWidth = math.max(maxColumnWidth, columnWidths[i])
		end
		local slidercellWidth = menu.slotData.width - math.floor(hasScrollbar and Helper.scrollbarWidth or 0)

		local highlightmode = "column"
		if (menu.upgradetypeMode == "consumables") or (menu.upgradetypeMode == "crew") or (menu.upgradetypeMode == "software") then
			highlightmode = "on"
		end
		local ftable = frame:addTable(11, { tabOrder = 1, width = menu.slotData.width, maxVisibleHeight = Helper.viewHeight - 2 * menu.slotData.offsetY, x = menu.slotData.offsetX, y = menu.slotData.offsetY, scaling = false, reserveScrollBar = menu.upgradetypeMode == "consumables", highlightMode = highlightmode, backgroundID = "solid", backgroundColor = Helper.color.transparent60 })
		if menu.setdefaulttable then
			ftable.properties.defaultInteractiveObject = true
			menu.setdefaulttable = nil
		end
		ftable:setColWidth(1, menu.rowHeight)
		ftable:setColWidth(2, slotWidths[1] - menu.rowHeight - Helper.borderSize)
		-- exact col widths are unimportant in these menus, keeping them variable and equal helps with scrollbar support
		if (menu.upgradetypeMode ~= "consumables") and (menu.upgradetypeMode ~= "crew") and (menu.upgradetypeMode ~= "software") then
			for i = 2, 8 do
				ftable:setColWidth(i + 1, slotWidths[i])
			end
		end
		ftable:setColWidth(11, menu.rowHeight)
		ftable:setDefaultColSpan(1, 4)
		ftable:setDefaultColSpan(5, 3)
		ftable:setDefaultColSpan(8, 4)

		local name = menu.getLeftBarEntry(menu.upgradetypeMode).name or ""
		local sizeicon
		if (menu.upgradetypeMode ~= "enginegroup") and (menu.upgradetypeMode ~= "turretgroup") and (menu.upgradetypeMode ~= "consumables") and (menu.upgradetypeMode ~= "crew") and (menu.upgradetypeMode ~= "repair") and (menu.upgradetypeMode ~= "software") then
			if upgradetype.supertype == "macro" and ((menu.object ~= 0) or (menu.macro ~= "")) then
				local slotsize = ffi.string(C.GetSlotSize(menu.object, 0, menu.macro, false, upgradetype.type, menu.currentSlot))
				if slotsize ~= "" then
					name = upgradetype.text[slotsize]
					sizeicon = "be_upgrade_size_" .. slotsize
				end
			elseif upgradetype.supertype == "virtualmacro" then
				if menu.class == "ship_s" then
					name = upgradetype.text["small"]
					sizeicon = "be_upgrade_size_small"
				elseif menu.class == "ship_m" then
					name = upgradetype.text["medium"]
					sizeicon = "be_upgrade_size_medium"
				elseif menu.class == "ship_l" then
					name = upgradetype.text["large"]
					sizeicon = "be_upgrade_size_large"
				elseif menu.class == "ship_xl" then
					name = upgradetype.text["extralarge"]
					sizeicon = "be_upgrade_size_extralarge"
				end
			end
		end

		local color = Helper.color.white
		if upgradetype and (not upgradetype.allowempty) then
			if menu.upgradeplan[upgradetype.type][menu.currentSlot] == "" then
				color = Helper.color.red
			end
		end
		local row = ftable:addRow(false, { fixed = true, bgColor = Helper.defaultTitleBackgroundColor })
		row[1]:setColSpan(11):createText(name, menu.headerTextProperties)
		row[1].properties.color = color

		if next(menu.groupedupgrades) then
			for _, group in ipairs(menu.groupedslots) do
				local row = ftable:addRow(true, { bgColor = Helper.color.transparent })
				for i = 1, 9 do
					if group[i] then
						local col = (i > 1) and (i + 1) or 1
						local colspan = ((i == 1) or (i == 9)) and 2 or 1

						local bgcolor = Helper.defaultTitleBackgroundColor
						if group[i][1] == menu.currentSlot then
							bgcolor = Helper.defaultArrowRowBackgroundColor
						end
						local count, total = 0, 0
						if (menu.upgradetypeMode == "enginegroup") or (menu.upgradetypeMode == "turretgroup") then
							for _, upgradetype2 in ipairs(Helper.upgradetypes) do
								if upgradetype2.supertype == "group" then
									if menu.groups[group[i][1]][upgradetype2.grouptype].total > 0 then
										if upgradetype2.mergeslots then
											count = count + ((menu.upgradeplan[upgradetype2.type][group[i][1]].count > 0) and 1 or 0)
											total = total + 1
										else
											count = count + menu.upgradeplan[upgradetype2.type][group[i][1]].count
											total = total + menu.groups[group[i][1]][upgradetype2.grouptype].total
										end
										if upgradetype2.allowempty == false then
											if menu.upgradeplan[upgradetype2.type][group[i][1]].macro == "" then
												color = Helper.color.red
											end
										end
									end
								end
							end
						else
							if (upgradetype.allowempty == false) then
								if menu.upgradeplan[upgradetype.type][group[i][1]] == "" then
									color = Helper.color.red
								end
							end
						end

						local mouseovertext = ""
						if upgradetype then
							mouseovertext = ReadText(1001, 66) .. " " .. group[i][3]
						else
							mouseovertext = ReadText(1001, 8023) .. " " .. group[i][3]
						end

						row[col]:setColSpan(colspan):createButton({ height = slotWidths[i], bgColor = bgcolor, mouseOverText = mouseovertext }):setText(group[i][3], { halign = "center", fontsize = Helper.scaleFont(Helper.standardFont, Helper.standardFontSize) })
						if total > 0 then
							local width = math.max(1, math.floor(count * (slotWidths[i] - 2 * menu.scaleSize) / total))
							row[col]:setIcon("solid", { color = Helper.color.white, width = width + 2 * Helper.configButtonBorderSize, height = menu.scaleSize + 2 * Helper.configButtonBorderSize, x = menu.scaleSize - Helper.configButtonBorderSize, y = slotWidths[i] - 2 * menu.scaleSize - Helper.configButtonBorderSize })
						end
						row[col].handlers.onClick = function () return menu.buttonSelectSlot(group[i][1], row.index, col) end
					end
				end
			end
		end

		local row = ftable:addRow(true, { fixed = true })
		row[1]:setColSpan(10):createEditBox({  }):setText(menu.searchtext, {  }):setHotkey("INPUT_STATE_DETAILMONITOR_0", { displayIcon = true })
		row[1].handlers.onEditBoxDeactivated = menu.editboxSearchUpdateText
		row[11]:createButton({ height = menu.rowHeight }):setText("X", { halign = "center", font = Helper.standardFontBold })
		row[11].handlers.onClick = function () return menu.buttonClearEditbox(row.index) end

		if next(menu.groupedupgrades) then
			if (menu.upgradetypeMode == "enginegroup") or (menu.upgradetypeMode == "turretgroup") then
				for i, upgradetype2 in ipairs(Helper.upgradetypes) do
					if upgradetype2.supertype == "group" then
						if menu.groups[menu.currentSlot] and (menu.groups[menu.currentSlot][upgradetype2.grouptype].total > 0) then
							local hasmod, modicon = menu.checkMod(upgradetype2.grouptype, menu.groups[menu.currentSlot][upgradetype2.grouptype].currentcomponent)

							local color = Helper.color.white
							if upgradetype2.allowempty == false then
								if menu.upgradeplan[upgradetype2.type][menu.currentSlot].macro == "" then
									color = Helper.color.red
								end
							end
							local plandata = menu.upgradeplan[upgradetype2.type][menu.currentSlot]
							
							local row = ftable:addRow(true, {  })
							local name = upgradetype2.text.default
							local slotsize = menu.groups[menu.currentSlot][upgradetype2.grouptype].slotsize
							if slotsize ~= "" then
								name = upgradetype2.text[slotsize]
								sizeicon = "be_upgrade_size_" .. slotsize
							end

							if plandata.macro ~= "" then
								name = GetMacroData(plandata.macro, "name")
							end

							if not upgradetype2.mergeslots then
								local scale = {
									min       = 0,
									minSelect = (plandata.macro == "") and 0 or 1,
									max       = menu.groups[menu.currentSlot][upgradetype2.grouptype].total,
								}
								scale.maxSelect = (plandata.macro == "") and 0 or scale.max
								scale.start = math.max(scale.minSelect, math.min(scale.maxSelect, plandata.count))

								-- handle already installed equipment
								local haslicence = menu.checkLicence(plandata.macro)
								if (plandata.macro == menu.groups[menu.currentSlot][upgradetype2.grouptype].currentmacro) and (not haslicence) then
									scale.maxSelect = math.min(scale.maxSelect, menu.groups[menu.currentSlot][upgradetype2.grouptype].count)
								end

								local mouseovertext = ""
								if hasmod then
									mouseovertext = "\27R" .. ReadText(1026, 8009) .. "\27X"
								end

								row[1]:setColSpan(11):createSliderCell({ width = slidercellWidth, height = menu.subHeaderRowHeight, valueColor = Helper.color.slidervalue, min = scale.min, minSelect = scale.minSelect, max = scale.max, maxSelect = scale.maxSelect, start = scale.start, readOnly = hasmod or menu.isReadOnly, mouseOverText = mouseovertext }):setText(name, menu.subHeaderSliderCellTextProperties)
								row[1].handlers.onSliderCellChanged = function (_, ...) return menu.slidercellSelectGroupAmount(upgradetype2.type, menu.currentSlot, row.index, false, ...) end
								row[1].properties.text.color = color
							else
								row[1]:setColSpan(11):createText(name, menu.subHeaderTextProperties)
								row[1].properties.color = color
							end

							for _, group in ipairs(menu.groupedupgrades[upgradetype2.grouptype]) do
								local row = ftable:addRow(true, { bgColor = Helper.color.transparent, borderBelow = false })
								local row2 = ftable:addRow(false, { bgColor = Helper.color.transparent })
								for i = 1, 3 do
									if group[i] then
										local column = i * 3 - 2
										if i > 1 then
											column = column + 1
										end

										local haslicence, icon, overridecolor, mouseovertext = menu.checkLicence(group[i].macro)
										local extraText = ""

										-- handle already installed equipment
										if (group[i].macro == menu.groups[menu.currentSlot][upgradetype2.grouptype].currentmacro) and (not haslicence) then
											haslicence = true
											mouseovertext = mouseovertext .. "\n" .. "\27G" .. ReadText(1026, 8004)
										end

										local weaponicon = GetMacroData(group[i].macro, "ammoicon")
										if weaponicon and (weaponicon ~= "") and C.IsIconValid(weaponicon) then
											weaponicon = "\27[" .. weaponicon .. "]"
										else
											weaponicon = ""
										end
										if hasmod then
											mouseovertext = "\27R" .. ReadText(1026, 8009) .. "\27X\n" .. mouseovertext
										end
										
										local hasstock = group[i].macro == ""
										local j = menu.findUpgradeMacro(upgradetype2.grouptype, group[i].macro)
										if j then
											local upgradeware = menu.upgradewares[upgradetype2.grouptype][j]
											local price
											if not menu.isReadOnly then
												price = tonumber(C.GetBuildWarePrice(menu.container, upgradeware.ware))
											end
											extraText = (price and (ConvertMoneyString(price, false, true, 0, true, false) .. " " .. ReadText(1001, 101)) or "")

											hasstock = upgradeware.isFromShipyard or (menu.groups[menu.currentSlot][upgradetype2.grouptype].currentmacro == group[i].macro)
										end
										
										if group[i].macro ~= "" then
											local shortname, makerrace, infolibrary = GetMacroData(group[i].macro, "shortname", "makerrace", "infolibrary")
											for _, racestring in ipairs(makerrace) do
												extraText = racestring .. " - " .. extraText
											end
											extraText = TruncateText(shortname, Helper.standardFont, menu.extraFontSize, columnWidths[i] - 2 * Helper.borderSize) .. "\n" .. extraText

											AddKnownItem(infolibrary, group[i].macro)
										else
											extraText = TruncateText(group[i].name, Helper.standardFont, menu.extraFontSize, columnWidths[i] - 2 * Helper.borderSize) .. "\n" .. extraText
										end

										local installicon, installcolor = (group[i].macro ~= "") and (sizeicon or "") or ""
										if not haslicence then
											installcolor = Helper.color.darkgrey
										elseif (group[i].macro ~= "") then
											if (group[i].macro == menu.groups[menu.currentSlot][upgradetype2.grouptype].currentmacro) and (group[i].macro ~= plandata.macro) then
												installicon = "be_upgrade_uninstalled"
												installcolor = Helper.color.red
											elseif (group[i].macro == plandata.macro) then
												installicon = "be_upgrade_installed"
												installcolor = Helper.color.green
												if hasmod then
													weaponicon = weaponicon .. " " .. modicon
												end
												if firsttime then
													menu.selectedRows.slots = row.index
													menu.selectedCols.slots = column
													firsttime = nil
												end
											end
										end
										local active = ((group[i].macro == plandata.macro) or (not hasmod)) and hasstock
										row[column]:createButton({
											active = active,
											width = columnWidths[i],
											height = maxColumnWidth,
											mouseOverText = mouseovertext,
											bgColor = haslicence and Helper.defaultButtonBackgroundColor or Helper.defaultUnselectableButtonBackgroundColor,
											highlightColor = haslicence and Helper.defaultButtonHighlightColor or Helper.defaultUnselectableButtonHighlightColor,
										}):setIcon(group[i].icon):setIcon2(installicon, { color = installcolor }):setText(icon, { y = maxColumnWidth / 2 - Helper.standardTextHeight / 2 - Helper.configButtonBorderSize, halign = "right", color = overridecolor }):setText2(weaponicon, { y = -maxColumnWidth / 2 + Helper.standardTextHeight / 2 + Helper.configButtonBorderSize })
										if haslicence then
											row[column].handlers.onClick = function () return menu.buttonSelectGroupUpgrade(upgradetype2.type, menu.currentSlot, group[i].macro, row.index, column) end
										end
										if group[i].macro ~= "" then
											row[column].handlers.onRightClick = function (...) return menu.buttonInteract({ type = upgradetype2.type, name = group[i].name, macro = group[i].macro }, ...) end
										end

										row2[column]:createBoxText(extraText, { width = columnWidths[i], fontsize = menu.extraFontSize, color = overridecolor, boxColor = haslicence and Helper.defaultButtonBackgroundColor or Helper.defaultUnselectableButtonBackgroundColor })
									end
								end
							end
						end
					end
				end
			elseif menu.upgradetypeMode == "repair" then
				for k, group in ipairs(menu.groupedupgrades) do
					local row = ftable:addRow(true, { bgColor = Helper.color.transparent, borderBelow = false })
					local row2 = ftable:addRow(false, { bgColor = Helper.color.transparent })
					for i = 1, 3 do
						if group[i] then
							local repairslotdata = menu.repairslots[k][i]
							local objectstring = tostring(menu.object)
							local componentstring = tostring(repairslotdata[4])
							local price
							if not menu.isReadOnly then
								price = tonumber(C.GetRepairPrice(repairslotdata[4], menu.container))
							end
							local extraText = price and (ConvertMoneyString(price, false, true, 0, true, false) .. " " .. ReadText(1001, 101)) or ""

							local shortname, makerrace, mk = GetMacroData(group[i].macro, "shortname", "makerrace", "mk")
							local class = ffi.string(C.GetComponentClass(repairslotdata[4]))
							if (class == "weapon") or (class == "missilelauncher") then
								if mk > 0 then
									extraText = ReadText(20111, 100 * mk + 1) .. " - " .. extraText
								end
							end
							for _, racestring in ipairs(makerrace) do
								extraText = racestring .. " - " .. extraText
							end
							extraText = TruncateText(shortname, Helper.standardFont, menu.extraFontSize, columnWidths[i] - 2 * Helper.borderSize) .. "\n" .. extraText

							local color = Helper.defaultButtonBackgroundColor
							-- TODO: handle button colors for queued items here.

							local installicon, installcolor = sizeicon or ""
							menu.repairplan[objectstring] = menu.repairplan[objectstring] or {}
							if menu.repairplan[objectstring][componentstring] then
								installicon = "be_upgrade_installed"
								installcolor = Helper.color.green
							end

							local column = i * 3 - 2
							if i > 1 then
								column = column + 1
							end
							row[column]:createButton({ width = columnWidths[i], height = maxColumnWidth, bgColor = color }):setIcon(group[i].icon):setIcon2(installicon, { color = installcolor })
							row[column].handlers.onClick = function () return menu.buttonSelectRepair(repairslotdata[4], row.index, column) end

							row2[column]:createBoxText(extraText, { width = columnWidths[i], fontsize = menu.extraFontSize })
						end
					end
				end
			else
				local planmacro = menu.upgradeplan[menu.upgradetypeMode][menu.currentSlot]
				local hasmod, modicon = menu.checkMod(upgradetype.type, slots[menu.currentSlot].component)

				for _, group in ipairs(menu.groupedupgrades) do
					local row = ftable:addRow(true, { bgColor = Helper.color.transparent, borderBelow = false })
					local row2 = ftable:addRow(false, { bgColor = Helper.color.transparent })
					for i = 1, 3 do
						if group[i] then
							local column = i * 3 - 2
							if i > 1 then
								column = column + 1
							end

							local haslicence, icon, overridecolor, mouseovertext = menu.checkLicence(group[i].macro, nil, false)
							local extraText = ""

							-- handle already installed equipment
							if (group[i].macro == slots[menu.currentSlot].currentmacro) and (not haslicence) then
								haslicence = true
								mouseovertext = mouseovertext .. "\n" .. "\27G" .. ReadText(1026, 8004)
							end

							local weaponicon
							if upgradetype.supertype == "macro" then
								weaponicon = GetMacroData(group[i].macro, "ammoicon")
							end
							if weaponicon and (weaponicon ~= "") and C.IsIconValid(weaponicon) then
								weaponicon = "\27[" .. weaponicon .. "]"
							else
								weaponicon = ""
							end
							if hasmod then
								mouseovertext = "\27R" .. ReadText(1026, 8009) .. "\27X\n" .. mouseovertext
							end

							local hasstock = group[i].macro == ""
							local j = menu.findUpgradeMacro(upgradetype.type, group[i].macro)
							if j then
								local upgradeware = menu.upgradewares[upgradetype.type][j]
								local price
								if not menu.isReadOnly then
									price = tonumber(C.GetBuildWarePrice(menu.container, upgradeware.ware))
								end
								extraText = (price and (ConvertMoneyString(price, false, true, 0, true, false) .. " " .. ReadText(1001, 101)) or "")

								hasstock = upgradeware.isFromShipyard or (slots[menu.currentSlot].currentmacro == group[i].macro)
							end

							if group[i].macro ~= "" then
								local shortname, makerrace, mk, infolibrary = GetMacroData(group[i].macro, "shortname", "makerrace", "mk", "infolibrary")
								if upgradetype.type == "weapon" then
									if mk > 0 then
										extraText = ReadText(20111, 100 * mk + 1) .. " - " .. extraText
									end
								end
								for _, racestring in ipairs(makerrace) do
									extraText = racestring .. " - " .. extraText
								end
								extraText = TruncateText(shortname, Helper.standardFont, menu.extraFontSize, columnWidths[i] - 2 * Helper.borderSize) .. "\n" .. extraText

								AddKnownItem(infolibrary, group[i].macro)
							else
								extraText = TruncateText(group[i].name, Helper.standardFont, menu.extraFontSize, columnWidths[i] - 2 * Helper.borderSize) .. "\n" .. extraText
							end

							local installicon, installcolor = (group[i].macro ~= "") and (sizeicon or "") or ""
							if not haslicence then
								installcolor = Helper.color.darkgrey
							else
								if (group[i].macro ~= "") then
									if (group[i].macro == slots[menu.currentSlot].currentmacro) and (group[i].macro ~= planmacro) then
										installicon = "be_upgrade_uninstalled"
										installcolor = Helper.color.red
									elseif (group[i].macro == planmacro) then
										installicon = "be_upgrade_installed"
										installcolor = Helper.color.green
										if hasmod then
											weaponicon = weaponicon .. " " .. modicon
										end
										if firsttime then
											menu.selectedRows.slots = row.index
											menu.selectedCols.slots = column
											firsttime = nil
										end
									end
								end
							end

							local active = ((group[i].macro == planmacro) or (not hasmod)) and hasstock
							row[column]:createButton({
								active = active,
								width = columnWidths[i],
								height = maxColumnWidth,
								mouseOverText = mouseovertext,
								bgColor = haslicence and Helper.defaultButtonBackgroundColor or Helper.defaultUnselectableButtonBackgroundColor,
								highlightColor = haslicence and Helper.defaultButtonHighlightColor or Helper.defaultUnselectableButtonHighlightColor,
							}):setIcon(group[i].icon):setIcon2(installicon, { color = installcolor }):setText(icon, { y = maxColumnWidth / 2 - Helper.standardTextHeight / 2 - Helper.configButtonBorderSize, halign = "right", color = overridecolor }):setText2(weaponicon, { y = -maxColumnWidth / 2 + Helper.standardTextHeight / 2 + Helper.configButtonBorderSize })
							if haslicence then
								row[column].handlers.onClick = function () return menu.buttonSelectUpgradeMacro(menu.upgradetypeMode, menu.currentSlot, group[i].macro, row.index, column) end
							end
							if group[i].macro ~= "" then
								row[column].handlers.onRightClick = function (...) return menu.buttonInteract({ type = menu.upgradetypeMode, name = group[i].name, macro = group[i].macro }, ...) end
							end

							row2[column]:createBoxText(extraText, { width = columnWidths[i], fontsize = menu.extraFontSize, color = overridecolor, boxColor = haslicence and Helper.defaultButtonBackgroundColor or Helper.defaultUnselectableButtonBackgroundColor })
						end
					end
				end
			end
		else
			if menu.upgradetypeMode == "consumables" then
				-- ammo
				local titlefirst = true
				for _, upgradetype in ipairs(Helper.upgradetypes) do
					if upgradetype.supertype == "ammo" then
						if next(menu.ammo[upgradetype.type]) then
							local total, capacity = menu.getAmmoUsage(upgradetype.type)
							local display = false
							for macro, _ in pairs(menu.ammo[upgradetype.type]) do
								if (total > 0) or menu.isAmmoCompatible(upgradetype.type, macro) then
									display = true
									break
								end
							end

							if ((total > 0) or (capacity > 0)) and display then
								if not titlefirst then
									local row = ftable:addRow(false, { scaling = true, bgColor = Helper.color.transparent })
									row[1]:setColSpan(11):createText("", { fontsize = 1, minRowHeight = Helper.standardTextHeight / 2 })
								end
								titlefirst = false

								local name = upgradetype.type
								if upgradetype.type == "drone" then
									name = ReadText(1001, 8)
								elseif upgradetype.type == "missile" then
									name = ReadText(1001, 1304)
								elseif upgradetype.type == "deployable" then
									name = ReadText(1001, 1332)		-- "Deployables"
								elseif upgradetype.type == "countermeasure" then
									name = ReadText(1001, 8063)		-- "Countermeasures"
								end
								
								local row = ftable:addRow(false, { bgColor = Helper.defaultHeaderBackgroundColor })
								row[1]:setColSpan(7):setBackgroundColSpan(10):createText(name, menu.subHeaderTextProperties)
								row[8]:setColSpan(4):createText(total .. "\27X" .. " / " .. capacity, menu.subHeaderTextProperties)
								row[8].properties.halign = "right"
								row[8].properties.color = (total > capacity) and Helper.color.red or Helper.color.white

								local first = true
								local sortedammo = Helper.orderedKeys(menu.ammo[upgradetype.type], menu.sortAmmo)
								for _, macro in ipairs(sortedammo) do
									if (menu.searchtext == "") or menu.filterUpgradeByText(macro, menu.searchtext) then
										menu.displayAmmoSlot(ftable, upgradetype.type, macro, total, capacity, first)
										first = false
									end
								end
							end
						end
					end
				end
			elseif menu.upgradetypeMode == "crew" then
				-- crew
				local minprice, maxprice = GetWareData(menu.crew.ware, "minprice", "maxprice")
				local price = maxprice - menu.crew.availableworkforce / menu.crew.maxavailableworkforce * (maxprice - minprice)

				if (menu.mode ~= "purchase") and (not menu.isReadOnly) then
					local row = ftable:addRow(false, { bgColor = Helper.color.transparent, scaling = true })
					row[1]:setColSpan(8):createText(string.format(ReadText(1001, 8024), ffi.string(C.GetComponentName(menu.container))), { mouseOverText = ReadText(1001, 2808) .. ReadText(1001, 120) .. " " .. ConvertMoneyString(price, false, true, 0, true, false) .. " " .. ReadText(1001, 101) })
					row[9]:setColSpan(3):createText(menu.crew.availableworkforce - menu.crew.hired, { halign = "right" })

					local row = ftable:addRow(false, { bgColor = Helper.color.transparent, scaling = true })
					row[1]:setColSpan(11):createText("")
				end

				local row = ftable:addRow(true, { bgColor = Helper.color.transparent })
				row[1]:setColSpan(7):setBackgroundColSpan(9):createText(ReadText(1001, 80), menu.subHeaderTextProperties)
				-- include the +1 for the captain
				row[8]:setColSpan(3):createText((menu.crew.total + menu.crew.hired + (menu.captainSelected and 1 or 0)) .. " / " .. (menu.crew.capacity + 1), menu.subHeaderTextProperties)
				row[8].properties.halign = "right"
				row[11]:createButton({ active = (not menu.isReadOnly), mouseOverText = ReadText(1026, 8001), height = menu.rowHeight, y = math.max(0, row:getHeight() - menu.rowHeight) }):setIcon("menu_reset")
				row[11].handlers.onClick = menu.buttonResetCrew

				local isbigship = (menu.class == "ship_m") or (menu.class == "ship_l") or (menu.class == "ship_xl")
				local row = ftable:addRow(true, { scaling = true, bgColor = Helper.color.transparent })
				row[1]:setColSpan(1):createCheckBox(menu.captainSelected, { scaling = false, active = not menu.captainSelected, width = menu.rowHeight, height = menu.rowHeight })
				row[1].handlers.onClick = function () return menu.checkboxSelectCaptain(row.index) end
				row[2]:setColSpan(7):createText(isbigship and ReadText(1001, 4848) or ReadText(1001, 4847), { color = menu.captainSelected and Helper.color.white or Helper.color.red })
				row[9]:setColSpan(3):createText(ReadText(1001, 8047), { halign = "right", color = menu.captainSelected and Helper.color.white or Helper.color.red })

				local row = ftable:addRow(false, { bgColor = Helper.color.transparent, scaling = true })
				row[1]:setColSpan(11):createText("")

				if menu.mode ~= "purchase" and (not menu.isReadOnly) then
					local color = Helper.color.white
					if #menu.crew.unassigned > 0 then
						color = Helper.color.red
					end

					local row = ftable:addRow(true, { scaling = true, bgColor = Helper.color.transparent })
					row[1]:setColSpan(7):createText(ReadText(1001, 8025))
					row[8]:setColSpan(3):createText(#menu.crew.unassigned, { halign = "right", color = color })
					row[11]:createButton({ active = (not menu.isReadOnly), mouseOverText = ReadText(1026, 8002) }):setIcon("menu_dismiss")
					row[11].handlers.onClick = menu.buttonFireCrew
				end

				local first = true
				for i, entry in ipairs(menu.crew.roles) do
					menu.displayCrewSlot(ftable, i, entry, buttonWidth, price, first)
					first = false
				end
			elseif menu.upgradetypeMode == "software" then
				-- software
				local first = true
				if menu.software[menu.upgradetypeMode] then
					for slot, slotdata in ipairs(menu.software[menu.upgradetypeMode]) do
						if #slotdata.possiblesoftware > 0 then
							if first then
								first = false
							else
								local row = ftable:addRow(false, { bgColor = Helper.color.transparent })
								row[1]:setColSpan(11):createText(" ")
							end
							menu.displaySoftwareSlot(ftable, menu.upgradetypeMode, slot, slotdata)
						end
					end
				end
			end
		end

		ftable:setTopRow(menu.topRows.slots)
		ftable:setSelectedRow(menu.selectedRows.slots)
		ftable:setSelectedCol(menu.selectedCols.slots or 0)
	end

	menu.topRows.slots = nil
	menu.selectedRows.slots = nil
	menu.selectedCols.slots = nil
end

function menu.displayModifySlots(frame)
	if menu.upgradetypeMode then
		local slotWidth = math.floor((menu.slotData.width - 8 * Helper.borderSize) / 9)

		local ftable = frame:addTable(6, { tabOrder = 1, width = menu.slotData.width, height = 0, x = menu.slotData.offsetX, y = menu.slotData.offsetY, scaling = false, reserveScrollBar = false, backgroundID = "solid", backgroundColor = Helper.color.transparent60 })
		if menu.setdefaulttable then
			ftable.properties.defaultInteractiveObject = true
			menu.setdefaulttable = nil
		end
		ftable:setColWidth(1, Helper.standardTextHeight)
		ftable:setColWidth(2, slotWidth - Helper.standardTextHeight - Helper.borderSize)
		ftable:setColWidth(3, slotWidth)
		ftable:setColWidth(4, slotWidth)
		ftable:setColWidth(6, menu.slotData.width / 3)
		ftable:setDefaultColSpan(2, 4)
		ftable:setDefaultBackgroundColSpan(1, 6)

		local row = ftable:addRow(false, { fixed = true, bgColor = Helper.defaultTitleBackgroundColor })
		row[1]:setColSpan(6):createText(ReadText(1001, 8031), menu.headerTextProperties)

		local row = ftable:addRow(true, { fixed = true, bgColor = Helper.color.transparent })
		for i, entry in ipairs(Helper.modQualities) do
			local col = i
			if i > 1 then
				col = col + 1
			end

			local bgColor = Helper.defaultButtonBackgroundColor
			if entry.category == menu.modCategory then
				bgColor = Helper.defaultArrowRowBackgroundColor
			end

			row[col]:setColSpan((i == 1) and 2 or 1):createButton({ height = slotWidth, mouseOverText = entry.name, bgColor = bgColor }):setIcon(entry.icon)
			row[col].handlers.onClick = function () return menu.buttonModCategory(entry.category, row.index, col) end
		end
		row[5]:setColSpan(2)

		if menu.object ~= 0 then
			local entry = menu.getLeftBarEntry(menu.upgradetypeMode)
			if entry.upgrademode == "ship" then
				menu.displayModSlot(ftable, entry.upgrademode, entry.modclass, 1)
			elseif entry.upgrademode == "shield" then
				for i, shieldgroupdata in ipairs(menu.shieldgroups) do
					menu.displayModSlot(ftable, entry.upgrademode, entry.modclass, i, shieldgroupdata)
				end
			else
				local upgradetype = Helper.findUpgradeType(entry.upgrademode)

				if upgradetype.mergeslots then
					local slotdata = menu.slots[entry.upgrademode][1]
					if slotdata.currentmacro ~= "" then
						menu.displayModSlot(ftable, entry.upgrademode, entry.modclass, 1, slotdata)
					end
				else
					for slot, slotdata in ipairs(menu.slots[entry.upgrademode]) do
						if slotdata.currentmacro ~= "" then
							menu.displayModSlot(ftable, entry.upgrademode, entry.modclass, slot, slotdata)
						end
					end
				end
			end
		end

		ftable:setTopRow(menu.topRows.slots)
		ftable:setSelectedRow(menu.selectedRows.slots)
		ftable:setSelectedCol(menu.selectedCols.slots or 0)
	end
	menu.topRows.slots = nil
	menu.selectedRows.slots = nil
	menu.selectedCols.slots = nil
end

function menu.displayModifyPaintSlots(frame)
	if menu.upgradetypeMode then	
		local entry = menu.getLeftBarEntry(menu.upgradetypeMode)
		-- available mods
		local categoryQuality = menu.getModQuality(menu.modCategory)
		if not categoryQuality then
			DebugError(string.format("Could not resolve mod category '%s' to quality level. Check Helper.modQualities [Florian]", menu.modCategory))
			return
		end

		local defaultpaintmod
		local buf = ffi.new("UIPaintMod")
		if (menu.object ~= 0) or (menu.macro ~= "") then
			if C.GetPlayerPaintThemeMod(menu.object, menu.macro, buf) then
				defaultpaintmod = {}
				defaultpaintmod.name = ffi.string(buf.Name)
				defaultpaintmod.ware = ffi.string(buf.Ware)
				defaultpaintmod.quality = buf.Quality
				defaultpaintmod.isdefault = true
			end
		end
		
		if not menu.selectedPaintMod then
			if menu.modeparam[1] and (#menu.selectableships > 1) then
				menu.selectedPaintMod = defaultpaintmod
			else
				menu.selectedPaintMod = menu.installedPaintMod
			end
		end

		local count = 0
		menu.groups = {}

		if categoryQuality == 1 then
			if defaultpaintmod then
				count = count + 1
				local group = math.ceil(count / 3)
				if menu.groups[group] then
					table.insert(menu.groups[group], defaultpaintmod)
				else
					menu.groups[group] = { defaultpaintmod }
				end
			end
		end

		if menu.modwares[entry.modclass] then
			for _, entry in ipairs(menu.modwares[entry.modclass]) do
				if (entry.quality == categoryQuality) and ((defaultpaintmod == nil) or (entry.ware ~= defaultpaintmod.ware)) then
					count = count + 1
					local group = math.ceil(count / 3)
					if menu.groups[group] then
						table.insert(menu.groups[group], entry)
					else
						menu.groups[group] = { entry }
					end
				end
			end
		end

		local buttontable = frame:addTable(2, { tabOrder = 9, width = menu.slotData.width, height = 0, x = menu.slotData.offsetX, y = 0, backgroundID = "solid", backgroundColor = Helper.color.transparent60 })
		buttontable:setColWidthPercent(2, 40)

		local row = buttontable:addRow(false, { fixed = true, bgColor = Helper.defaultTitleBackgroundColor })
		row[1]:setColSpan(2):createText(ReadText(1001, 8514), menu.headerTextProperties)

		local active = menu.selectedPaintMod ~= nil
		local missingcount
		if menu.selectedPaintMod then
			if menu.modeparam[1] then
				missingcount = 0
				for _, ship in pairs(menu.modeparam[2]) do
					local paintmod = ffi.new("UIPaintMod")
					if C.GetInstalledPaintMod(ship, paintmod) then
						if menu.selectedPaintMod.ware ~= ffi.string(paintmod.Ware) then
							missingcount = missingcount + 1
						end
					else
						missingcount = missingcount + 1
					end
				end
				if (missingcount == 0) or ((not menu.selectedPaintMod.isdefault) and (missingcount > menu.selectedPaintMod.amount)) then
					active = false
				end
			else
				active = menu.selectedPaintMod.ware ~= menu.installedPaintMod.ware
			end
		end

		local row = buttontable:addRow(false, { fixed = true, bgColor = Helper.color.transparent })
		row[1]:setColSpan(2):createText(menu.selectedPaintMod and (menu.selectedPaintMod.isdefault and ReadText(1001, 8516) or (((missingcount and (missingcount > 0)) and (missingcount .. ReadText(1001, 42) .. " ") or "") .. menu.selectedPaintMod.name)) or "", { color =  menu.selectedPaintMod and Helper.modQualities[menu.selectedPaintMod.quality].color or Helper.color.white })

		local row = buttontable:addRow(true, { fixed = true, bgColor = Helper.color.transparent })
		row[2]:createButton({ active = active }):setText(ReadText(1001, 4803), { halign = "center" })
		row[2].handlers.onClick = menu.buttonInstallPaintMod

		buttontable.properties.y = Helper.viewHeight - buttontable:getFullHeight() - Helper.frameBorder
		
		menu.extraFontSize = Helper.scaleFont(Helper.standardFont, Helper.standardFontSize)
		local maxSlotWidth = math.floor((menu.slotData.width - 8 * Helper.borderSize) / 9)
		local boxTextHeight = math.ceil(C.GetTextHeight(" ", Helper.standardFont, menu.extraFontSize, 0)) + 2 * Helper.borderSize
		local headerHeight = menu.titleData.height + (maxSlotWidth + Helper.borderSize) + 2 * Helper.borderSize
		--[[ Keep for simpler debugging
			print((buttontable.properties.y - menu.slotData.offsetY) .. " vs " .. (headerHeight + #menu.groups * (3 * (maxSlotWidth + Helper.borderSize) + boxTextHeight)))
			print(headerHeight)
			print(boxTextHeight)
			print(#menu.groups .. " * " .. 3 * (maxSlotWidth + Helper.borderSize) + boxTextHeight) --]]
		if (buttontable.properties.y - menu.slotData.offsetY) < (headerHeight + #menu.groups * (3 * (maxSlotWidth + Helper.borderSize) + boxTextHeight)) then
			hasScrollbar = true
		end

		local slotWidth = maxSlotWidth - math.floor((hasScrollbar and Helper.scrollbarWidth or 0) / 9)
		local extraPixels = (menu.slotData.width - 8 * Helper.borderSize) % 9
		local slotWidths = { slotWidth, slotWidth, slotWidth, slotWidth, slotWidth, slotWidth, slotWidth, slotWidth, slotWidth }
		if extraPixels > 0 then
			for i = 1, extraPixels do
				slotWidths[i] = slotWidths[i] + 1
			end
		end
		columnWidths = {}
		maxColumnWidth = 0
		for i = 1, 3 do
			columnWidths[i] = slotWidths[(i - 1) * 3 + 1] + slotWidths[(i - 1) * 3 + 2] + slotWidths[(i - 1) * 3 + 3] + 2 * Helper.borderSize
			maxColumnWidth = math.max(maxColumnWidth, columnWidths[i])
		end

		local ftable = frame:addTable(9, { tabOrder = 1, width = menu.slotData.width, maxVisibleHeight = buttontable.properties.y - menu.slotData.offsetY, x = menu.slotData.offsetX, y = menu.slotData.offsetY, scaling = false, reserveScrollBar = false, backgroundID = "solid", backgroundColor = Helper.color.transparent60, highlightMode = "column" })
		if menu.setdefaulttable then
			ftable.properties.defaultInteractiveObject = true
			menu.setdefaulttable = nil
		end
		for i = 1, 8 do
			ftable:setColWidth(i, slotWidths[i])
		end

		local row = ftable:addRow(false, { fixed = true, bgColor = Helper.defaultTitleBackgroundColor })
		row[1]:setColSpan(9):createText(ReadText(1001, 8031), menu.headerTextProperties)

		local row = ftable:addRow(true, { fixed = true, bgColor = Helper.color.transparent })
		for i, entry in ipairs(Helper.modQualities) do
			local col = i

			local bgColor = Helper.defaultButtonBackgroundColor
			if entry.category == menu.modCategory then
				bgColor = Helper.defaultArrowRowBackgroundColor
			end

			row[col]:createButton({ height = slotWidth, mouseOverText = entry.name, bgColor = bgColor }):setIcon(entry.icon)
			row[col].handlers.onClick = function () return menu.buttonModCategory(entry.category, row.index, col) end
		end

		if menu.object ~= 0 then
			if next(menu.groups) then
				for _, group in ipairs(menu.groups) do
					local row = ftable:addRow(true, { bgColor = Helper.color.transparent, borderBelow = false })
					local row2 = ftable:addRow(false, { bgColor = Helper.color.transparent })
					for i, entry in ipairs(group) do
						local col = i * 3 - 2

						local active, overridecolor = true, nil
						if menu.modeparam[1] then
							local missingcount = 0
							for _, ship in pairs(menu.modeparam[2]) do
								local paintmod = ffi.new("UIPaintMod")
								if C.GetInstalledPaintMod(ship, paintmod) then
									if entry.ware ~= ffi.string(paintmod.Ware) then
										missingcount = missingcount + 1
									end
								else
									missingcount = missingcount + 1
								end
							end
							if (not entry.isdefault) and (missingcount > entry.amount) then
								active = false
								overridecolor = Helper.color.red
							end
						end

						local installicon, installcolor = "", Helper.color.white
						if entry.ware == menu.selectedPaintMod.ware then
							installicon = "be_upgrade_installed"
							installcolor = Helper.color.green
						elseif entry.ware == menu.installedPaintMod.ware then
							installicon = "be_upgrade_uninstalled"
							installcolor = Helper.color.red
						end

						row[col]:setColSpan(3):createButton({
							width = columnWidths[i],
							height = maxColumnWidth,
							bgColor = active and Helper.defaultButtonBackgroundColor or Helper.defaultUnselectableButtonBackgroundColor,
							highlightColor = active and Helper.defaultButtonHighlightColor or Helper.defaultUnselectableButtonHighlightColor,
						}):setIcon(entry.ware):setIcon2(installicon, { color = installcolor }):setText((entry.amount and (entry.amount > 0)) and (ReadText(1001, 42) .. " " .. entry.amount) or "", { x = Helper.scaleX(Helper.configButtonBorderSize), y = - maxColumnWidth / 2 + Helper.standardTextHeight / 2 + Helper.configButtonBorderSize, halign = "right", fontsize = Helper.scaleFont(Helper.standardFont, Helper.headerRow1FontSize), color = overridecolor })
						if active then
							row[col].handlers.onClick = function () return menu.buttonSelectPaintMod(entry, row.index, col) end
						end

						local extraText = TruncateText(entry.isdefault and ReadText(1001, 8516) or entry.name, Helper.standardFont, menu.extraFontSize, columnWidths[i] - 2 * Helper.borderSize)
						row2[col]:setColSpan(3):createBoxText(extraText, { width = columnWidths[i], fontsize = menu.extraFontSize, color = overridecolor, boxColor = active and Helper.defaultButtonBackgroundColor or Helper.defaultUnselectableButtonBackgroundColor })
					end
				end
			else
				local row = ftable:addRow(true, { bgColor = Helper.color.transparent, scaling = true })
				row[1]:setColSpan(9):createText("  " .. Helper.modQualities[categoryQuality].paintnonetext, { color = Helper.modQualities[categoryQuality].color })
			end
		end

		ftable:setTopRow(menu.topRows.slots)
		ftable:setSelectedRow(menu.selectedRows.slots)
		ftable:setSelectedCol(menu.selectedCols.slots or 0)

		ftable.properties.nextTable = buttontable.index
		buttontable.properties.prevTable = ftable.index
	end
	menu.topRows.slots = nil
	menu.selectedRows.slots = nil
	menu.selectedCols.slots = nil

end

function menu.displayModSlot(ftable, type, modclass, slot, slotdata)
	local upgradetype
	if type ~= "ship" then
		upgradetype = Helper.findUpgradeType(type)
	end
	
	local isexpanded = menu.isModSlotExpanded(type, slot)
	if (type == "ship") or (upgradetype and upgradetype.mergeslots) then
		isexpanded = true
	end

	local installedmod = {}
	local hasinstalledmod = false
	if (type == "weapon") or (type == "turret") then
		hasinstalledmod, installedmod = Helper.getInstalledModInfo(type, slotdata.component)
	elseif type == "engine" then
		hasinstalledmod, installedmod = Helper.getInstalledModInfo(type, menu.object)
	elseif type == "shield" then
		hasinstalledmod, installedmod = Helper.getInstalledModInfo(type, menu.object, slotdata.context, slotdata.group)
	elseif type == "ship" then
		hasinstalledmod, installedmod = Helper.getInstalledModInfo(type, menu.object)
	end

	local row = ftable:addRow(true, { scaling = true })
	if (type ~= "ship") and ((not upgradetype) or (not upgradetype.mergeslots)) then
		row[1]:setBackgroundColSpan(1):createButton({ active = true }):setText(isexpanded and "-" or "+", { halign = "center" })
		row[1].handlers.onClick = function() return menu.expandModSlot(type, slot, row.index) end
		row[2]:setBackgroundColSpan(5)
	end
	if type == "ship" then
		row[2]:setColSpan(5):createText(ffi.string(C.GetComponentName(menu.object)) .. " (" .. ReadText(1001, 8008) .. ReadText(1001, 120) .. " " .. GetMacroData(GetComponentData(ConvertStringTo64Bit(tostring(menu.object)), "macro"), "name") .. ")" .. (hasinstalledmod and ("  \27[" .. Helper.modQualities[installedmod.Quality].icon2 .. "]") or ""))
	elseif type == "shield" then
		if (slotdata.context == menu.object) and (slotdata.group == "") then
			row[2]:setColSpan(5):createText(ffi.string(C.GetComponentName(slotdata.context)) .. " (" .. ReadText(1001, 8008) .. ReadText(1001, 120) .. " " .. GetMacroData(GetComponentData(ConvertStringTo64Bit(tostring(slotdata.context)), "macro"), "name") .. ")" .. (hasinstalledmod and ("  \27[" .. Helper.modQualities[installedmod.Quality].icon2 .. "]") or ""))
		else
			row[2]:createText(GetMacroData(GetComponentData(ConvertStringTo64Bit(tostring(slotdata.component)), "macro"), "name") .. (hasinstalledmod and ("  \27[" .. Helper.modQualities[installedmod.Quality].icon2 .. "]") or ""))
			row[6]:createText("[" .. ReadText(1001, 66) .. " " .. (slot - (menu.shieldgroups.hasMainGroup and 1 or 0)) .. "]", { halign = "right" })
		end
	else
		row[2]:createText(GetMacroData(slotdata.currentmacro, "name") .. (hasinstalledmod and ("  \27[" .. Helper.modQualities[installedmod.Quality].icon2 .. "]") or ""))
		if (not uprgadetype) or (not upgradetype.mergeslots) then
			row[6]:createText("[" .. ReadText(1001, 66) .. " " .. slot .. "]", { halign = "right" })
		end
	end

	if isexpanded then
		if hasinstalledmod then
			-- name
			local color = Helper.modQualities[installedmod.Quality].color
			local row = ftable:addRow(true, { bgColor = Helper.color.transparent, scaling = true })
			row[2]:createText(installedmod.Name, { color = color })
			row[6]:createText(ReadText(1001, 8033), { halign = "right", color = color })
			-- Effects
			local row = ftable:addRow(true, { bgColor = Helper.color.transparent, scaling = true })
			row[2]:setColSpan(5):createText("   " .. ReadText(1001, 8034) .. ReadText(1001, 120))
			-- default property
			for i, property in ipairs(Helper.modProperties[modclass]) do
				if property.key == installedmod.PropertyType then
					if installedmod[property.key] ~= property.basevalue then
						local effectcolor
						if installedmod[property.key] > property.basevalue then
							effectcolor = property.pos_effect and Helper.color.green or Helper.color.red
						else
							effectcolor = property.pos_effect and Helper.color.red or Helper.color.green
						end
						local row = ftable:addRow(true, { bgColor = Helper.color.transparent, scaling = true })
						row[2]:createText("      " .. property.text, { font = Helper.standardFontBold })
						row[6]:createText(property.eval(installedmod[property.key]), { font = Helper.standardFontBold, halign = "right", color = effectcolor })
					end
					break
				end
			end
			-- other properties
			for i, property in ipairs(Helper.modProperties[modclass]) do
				if property.key ~= installedmod.PropertyType then
					if installedmod[property.key] ~= property.basevalue then
						local effectcolor
						if installedmod[property.key] > property.basevalue then
							effectcolor = property.pos_effect and Helper.color.green or Helper.color.red
						else
							effectcolor = property.pos_effect and Helper.color.red or Helper.color.green
						end
						local row = ftable:addRow(true, { bgColor = Helper.color.transparent, scaling = true })
						row[2]:createText("      " .. property.text)
						row[6]:createText(property.eval(installedmod[property.key]), { halign = "right", color = effectcolor })
					end
				end
			end
			-- dismantle
			local row = ftable:addRow(true, { bgColor = Helper.color.transparent, scaling = true })
			row[1]:setColSpan(4)
			row[5]:setColSpan(2):createButton({  }):setText(ReadText(1001, 6601), { halign = "center" })
			if (type == "ship") or (type == "engine") then
				row[5].handlers.onClick = function () return menu.buttonDismantleMod(type, menu.object) end
			elseif type == "shield" then
				row[5].handlers.onClick = function () return menu.buttonDismantleMod(type, menu.object, slotdata.context, slotdata.group) end
			else
				row[5].handlers.onClick = function () return menu.buttonDismantleMod(type, slotdata.component) end
			end
		else
			local row = ftable:addRow(true, { bgColor = Helper.color.transparent, scaling = true })
			row[2]:setColSpan(5):createText(ReadText(1001, 8032))
		end

		-- available mods
		local categoryQuality = menu.getModQuality(menu.modCategory)
		if not categoryQuality then
			DebugError(string.format("Could not resolve mod category '%s' to quality level. Check Helper.modQualities [Florian]", menu.modCategory))
			return
		end
		local found = false
		if menu.modwares[modclass] then
			for _, entry in ipairs(menu.modwares[modclass]) do
				if entry.quality == categoryQuality then
					if (modclass ~= "weapon") or C.CheckWeaponModCompatibility(slotdata.component, entry.ware) then
						found = true
						menu.displayModBlueprint(ftable, type, slot, slotdata, modclass, entry, hasinstalledmod)
					end
				end
			end
		end
		if not found then
			local row = ftable:addRow(true, { bgColor = Helper.color.transparent, scaling = true })
			row[2]:setColSpan(5):createText(" " .. Helper.modQualities[categoryQuality].nonetext, { color = Helper.modQualities[categoryQuality].color })
		end
	end
end

function menu.displayModBlueprint(ftable, type, slot, slotdata, modclass, moddata, hasinstalledmod)
	local isexpanded = menu.isModSlotExpanded(type, slot .. moddata.ware)
	-- mod name
	local row = ftable:addRow(true, { bgColor = Helper.color.transparent, scaling = true })
	row[1]:setBackgroundColSpan(1):createButton({ active = true }):setText(isexpanded and "-" or "+", { halign = "center" })
	row[1].handlers.onClick = function() return menu.expandModSlot(type, slot .. moddata.ware, row.index) end
	row[2]:setBackgroundColSpan(5):setColSpan(5):createText(" " .. GetWareData(moddata.ware, "name"), { color = Helper.modQualities[moddata.quality].color })

	if isexpanded then
		-- Resources
		for _, resource in ipairs(moddata.resources) do
			local row = ftable:addRow(true, { bgColor = Helper.color.transparent, scaling = true })
			local color = (resource.data.amount < resource.data.needed) and Helper.color.grey or Helper.color.white
			-- name
			row[2]:setColSpan(4):createText("      " .. resource.data.name, { color = color })
			-- amount
			row[6]:createText(resource.data.amount .. " / " .. resource.data.needed, { halign = "right", color = color })
		end
		-- Effects
		local row = ftable:addRow(true, { bgColor = Helper.color.transparent, scaling = true })
		row[2]:setColSpan(5):createText("   " .. ReadText(1001, 8034) .. ReadText(1001, 120))
		-- Property
		local moddef = C.GetEquipmentModInfo(moddata.ware)
		local propertytype = ffi.string(moddef.PropertyType)
		for i, property in ipairs(Helper.modProperties[modclass]) do
			if property.key == propertytype then
				local minvalue = moddef["MinValue" .. property.type]
				local mineffectcolor = Helper.color.white
				if minvalue > property.basevalue then
					mineffectcolor = property.pos_effect and Helper.color.green or Helper.color.red
				elseif minvalue < property.basevalue then
					mineffectcolor = property.pos_effect and Helper.color.red or Helper.color.green
				end

				local maxvalue = moddef["MaxValue" .. property.type]
				local maxeffectcolor = Helper.color.white
				if maxvalue > property.basevalue then
					maxeffectcolor = property.pos_effect and Helper.color.green or Helper.color.red
				elseif maxvalue < property.basevalue then
					maxeffectcolor = property.pos_effect and Helper.color.red or Helper.color.green
				end

				local row = ftable:addRow(true, { bgColor = Helper.color.transparent, scaling = true })
				row[2]:createText("      " .. property.text, { font = Helper.standardFontBold })
				if property.pos_effect and (minvalue < maxvalue) or (minvalue > maxvalue) then
					row[6]:createText(property.eval2(minvalue, mineffectcolor, maxvalue, maxeffectcolor), { font = Helper.standardFontBold, halign = "right" })
				else
					row[6]:createText(property.eval2(maxvalue, maxeffectcolor, minvalue, mineffectcolor), { font = Helper.standardFontBold, halign = "right" })
				end
				break
			end
		end
		-- Bonus properties
		if moddef.BonusMax > 0 then
			local mouseovertext = ReadText(1026, 8005) .. ReadText(1001, 120)
			for n = 1, moddef.BonusMax do
				-- n < n_max:
				-- p_n = p^n * (1-p)
				-- n == n_max:
				-- p_n_max = p^n_max
				local probability = ((moddef.BonusChance ^ n) * ((n ~= moddef.BonusMax) and (1 - moddef.BonusChance) or 1))
				mouseovertext = mouseovertext .. "\n" .. string.format("%+d %s%s %4.1f%%", n, ReadText(1001, 6602), ReadText(1001, 120), probability * 100)
			end

			local row = ftable:addRow(true, { bgColor = Helper.color.transparent, scaling = true })
			row[2]:createText("      " .. ((moddef.BonusMax == 1) and ReadText(1001, 8039) or string.format(ReadText(1001, 8040), moddef.BonusMax)), { mouseOverText = mouseovertext })
			row[6]:createText("???", { halign = "right" })
		end
		-- Install
		local row = ftable:addRow(true, { bgColor = Helper.color.transparent, scaling = true })
		row[1]:setColSpan(4)
		local playermoney = GetPlayerMoney()
		local mouseovertext = ""
		if hasinstalledmod then
			if mouseovertext ~= "" then
				mouseovertext = mouseovertext .. "\n"
			end
			mouseovertext = mouseovertext .. ReadText(1026, 8006)
		end
		if moddata.craftableamount == 0 then
			if mouseovertext ~= "" then
				mouseovertext = mouseovertext .. "\n"
			end
			mouseovertext = mouseovertext .. ReadText(1026, 8007)
		end
		if playermoney < Helper.modQualities[moddata.quality].price then
			if mouseovertext ~= "" then
				mouseovertext = mouseovertext .. "\n"
			end
			mouseovertext = mouseovertext .. ReadText(1026, 8008)
		end
		if mouseovertext ~= "" then
			mouseovertext = "\27R" .. mouseovertext
		end
		row[5]:setColSpan(2):createButton({ active = (not hasinstalledmod) and (moddata.craftableamount > 0) and (playermoney >= Helper.modQualities[moddata.quality].price), mouseOverText = mouseovertext }):setText(string.format(ReadText(1001, 8043) .. " " .. ReadText(1001, 101), ConvertMoneyString(Helper.modQualities[moddata.quality].price, false, true, 0, true, false)), { halign = "center" })
		if (type == "ship") or (type == "engine") then
			row[5].handlers.onClick = function () return menu.buttonInstallMod(type, menu.object, moddata.ware, Helper.modQualities[moddata.quality].price) end
		elseif type == "shield" then
			row[5].handlers.onClick = function () return menu.buttonInstallMod(type, menu.object, moddata.ware, Helper.modQualities[moddata.quality].price, slotdata.context, slotdata.group) end
		else
			row[5].handlers.onClick = function () return menu.buttonInstallMod(type, slotdata.component, moddata.ware, Helper.modQualities[moddata.quality].price) end
		end
	end
end

function menu.displayEmptySlots(frame)
	local ftable = frame:addTable(1, { tabOrder = 1, width = menu.slotData.width, height = 0, x = menu.slotData.offsetX, y = menu.slotData.offsetY, scaling = true, reserveScrollBar = false, backgroundID = "solid", backgroundColor = Helper.color.transparent60 })
	if menu.setdefaulttable then
		ftable.properties.defaultInteractiveObject = true
		menu.setdefaulttable = nil
	end

	local row = ftable:addRow(false, { bgColor = Helper.defaultTitleBackgroundColor })
	row[1]:createText(ReadText(1001, 7935), menu.headerTextProperties)

	row = ftable:addRow(false, { scaling = true, bgColor = Helper.color.transparent })
	row[1]:createText(ReadText(1001, 8013))

	menu.topRows.slots = nil
	menu.selectedRows.slots = nil
	menu.selectedCols.slots = nil
end

function menu.checkLicence(macro, rawicon, issoftware, rawmouseovertext)
	local haslicence = true
	local icon
	local overridecolor = Helper.color.white
	local mouseovertext = ""
	if not menu.isReadOnly then
		if macro ~= "" then
			local ware
			if issoftware then
				ware = macro
			else
				ware = GetMacroData(macro, "ware")
			end
			local tradelicence = GetWareData(ware, "tradelicence")
			if tradelicence ~= "" then
				haslicence = HasLicence("player", tradelicence, menu.containerowner)
				local licenceinfo = ffi.new("LicenceInfo")
				if C.GetLicenceInfo(licenceinfo, menu.containerowner, tradelicence) then
					icon = ffi.string(licenceinfo.icon)
					if icon ~= "" then
						if not rawicon then
							icon = "\27[" .. icon .. "]"
						end
						mouseovertext = (rawmouseovertext and "" or (haslicence and "" or "\27R")) .. string.format(ReadText(1026, 8003), ffi.string(licenceinfo.name))
					end
				end
			end
			
			if menu.containerowner == "player" then
				haslicence = true
			end
			
			if not haslicence then
				overridecolor = Helper.color.red
			end
		end
	end

	return haslicence, icon, overridecolor, mouseovertext
end

function menu.checkMod(type, component)
	local hasmod, modicon = false, ""
	if component then
		if (type == "weapon") or (type == "turret") then
			local buf = ffi.new("UIWeaponMod")
			hasmod = C.GetInstalledWeaponMod(component, buf)
			if hasmod then
				modicon = "\27[" .. Helper.modQualities[buf.Quality].icon2 .. "]"
			end
		elseif type == "engine" then
			local buf = ffi.new("UIEngineMod")
			hasmod = C.GetInstalledEngineMod(menu.object, buf)
			if hasmod then
				modicon = "\27[" .. Helper.modQualities[buf.Quality].icon2 .. "]"
			end
		elseif type == "shield" then
			local shieldgroup = ffi.new("ShieldGroup")
			local found = C.GetShieldGroup(shieldgroup, menu.object, component);
			if found then
				local buf = ffi.new("UIShieldMod")
				hasmod = C.GetInstalledShieldMod(menu.object, shieldgroup.context, shieldgroup.group, buf)
				if hasmod then
					modicon = "\27[" .. Helper.modQualities[buf.Quality].icon2 .. "]"
				end
			end
		end
	end

	return hasmod, modicon
end

function menu.checkEquipment(removedEquipment, currentEquipment, newEquipment, repairedEquipment)
	-- Equipment
	for i, upgradetype in ipairs(Helper.upgradetypes) do
		local slots = menu.upgradeplan[upgradetype.type]
		local first = true
		for slot, macro in pairs(slots) do
			if first or (not upgradetype.mergeslots) then
				first = false
				if (upgradetype.supertype == "group") and (not upgradetype.pseudogroup) then
					local data = macro
					local oldslotdata = menu.groups[slot][upgradetype.grouptype]

					if data.macro ~= "" then
						local i = menu.findUpgradeMacro(upgradetype.grouptype, data.macro)
						if not i then
							break
						end
						local upgradeware = menu.upgradewares[upgradetype.grouptype][i]

						if oldslotdata.currentmacro ~= "" then
							local j = menu.findUpgradeMacro(upgradetype.grouptype, oldslotdata.currentmacro)
							if not j then
								break
							end
							local oldupgradeware = menu.upgradewares[upgradetype.grouptype][j]

							if data.macro == oldslotdata.currentmacro then
								if upgradetype.mergeslots then
									menu.insertWare(currentEquipment, upgradetype.grouptype, upgradeware.ware, (upgradetype.mergeslots and #slots or data.count))
								else
									if oldslotdata.count < data.count then
										menu.insertWare(currentEquipment, upgradetype.grouptype, upgradeware.ware, oldslotdata.count)
										menu.insertWare(newEquipment, upgradetype.grouptype, upgradeware.ware, data.count - oldslotdata.count, "normal")
									elseif oldslotdata.count > data.count then
										menu.insertWare(currentEquipment, upgradetype.grouptype, upgradeware.ware, data.count)
										menu.insertWare(removedEquipment, upgradetype.grouptype, upgradeware.ware, oldslotdata.count - data.count, "normal")
									else
										menu.insertWare(currentEquipment, upgradetype.grouptype, upgradeware.ware, (upgradetype.mergeslots and #slots or data.count))
									end
								end
							else
								menu.insertWare(removedEquipment, upgradetype.grouptype, oldupgradeware.ware, (upgradetype.mergeslots and #slots or oldslotdata.count), "normal")
								menu.insertWare(newEquipment, upgradetype.grouptype, upgradeware.ware, (upgradetype.mergeslots and #slots or data.count), "normal")
							end
						else
							menu.insertWare(newEquipment, upgradetype.grouptype, upgradeware.ware, (upgradetype.mergeslots and #slots or data.count), "normal")
						end
					elseif oldslotdata.currentmacro ~= "" then
						local j = menu.findUpgradeMacro(upgradetype.grouptype, oldslotdata.currentmacro)
						if not j then
							break
						end
						local oldupgradeware = menu.upgradewares[upgradetype.grouptype][j]

						menu.insertWare(removedEquipment, upgradetype.grouptype, oldupgradeware.ware, (upgradetype.mergeslots and #slots or oldslotdata.count), "normal")
					end
				elseif (upgradetype.supertype == "macro") or (upgradetype.supertype == "virtualmacro") then
					local oldslotdata = menu.slots[upgradetype.type][slot]
					if macro ~= "" then
						local i = menu.findUpgradeMacro(upgradetype.type, macro)
						if not i then
							break
						end
						local upgradeware = menu.upgradewares[upgradetype.type][i]

						if oldslotdata.currentmacro ~= "" then
							local j = menu.findUpgradeMacro(upgradetype.type, oldslotdata.currentmacro)
							if not j then
								break
							end
							local oldupgradeware = menu.upgradewares[upgradetype.type][j]

							if macro == oldslotdata.currentmacro then
								menu.insertWare(currentEquipment, upgradetype.type, upgradeware.ware, (upgradetype.mergeslots and #slots or 1))
							else
								menu.insertWare(removedEquipment, upgradetype.type, oldupgradeware.ware, (upgradetype.mergeslots and #slots or 1), "normal")
								menu.insertWare(newEquipment, upgradetype.type, upgradeware.ware, (upgradetype.mergeslots and #slots or 1), "normal")
							end
						else
							menu.insertWare(newEquipment, upgradetype.type, upgradeware.ware, (upgradetype.mergeslots and #slots or 1), "normal")
						end
					elseif oldslotdata.currentmacro ~= "" then
						local j = menu.findUpgradeMacro(upgradetype.type, oldslotdata.currentmacro)
						if not j then
							break
						end
						local oldupgradeware = menu.upgradewares[upgradetype.type][j]

						menu.insertWare(removedEquipment, upgradetype.type, oldupgradeware.ware, (upgradetype.mergeslots and #slots or 1), "normal")
					end
				elseif upgradetype.supertype == "ammo" then
					if menu.ammo[upgradetype.type][slot] then
						local current = menu.ammo[upgradetype.type][slot]
						local new = macro
						local macro = slot
						if current ~= new then
							local j = menu.findUpgradeMacro(upgradetype.type, macro)
							if not j then
								break
							end
							local upgradeware = menu.upgradewares[upgradetype.type][j]

							if current < new then
								if current > 0 then
									menu.insertWare(currentEquipment, "consumables", upgradeware.ware, current)
								end
								menu.insertWare(newEquipment, "consumables", upgradeware.ware, new - current, "normal")
							elseif current > new then
								if new > 0 then
									menu.insertWare(currentEquipment, "consumables", upgradeware.ware, new)
								end
								menu.insertWare(removedEquipment, "consumables", upgradeware.ware, current - new, "normal")
							elseif current > 0 then
								menu.insertWare(currentEquipment, "consumables", upgradeware.ware, current)
							end
						end
					end
				elseif upgradetype.supertype == "software" then
					local newware = macro
					local oldware = menu.software[upgradetype.type][slot].currentsoftware
					if oldware ~= newware then
						if oldware ~= "" then
							menu.insertWare(removedEquipment, upgradetype.supertype, oldware, 1, "software")
						end
						if newware ~= "" then
							menu.insertWare(newEquipment, upgradetype.supertype, newware, 1, "software")
						end
					elseif newware ~= "" then
						menu.insertWare(currentEquipment, upgradetype.supertype, newware, 1)
					end
				end
			end
		end
	end

	-- Crew
	if menu.crew.hired > 0 then
		local color = Helper.color.green

		menu.insertWare(newEquipment, "crew", menu.crew.ware, menu.crew.hired, "crew")
		if menu.crew.total > 0 then
			menu.insertWare(currentEquipment, "crew", menu.crew.ware, menu.crew.total)
		end
	elseif #menu.crew.fired > 0 then
		menu.insertWare(removedEquipment, "crew", menu.crew.ware, #menu.crew.fired, "crew")
		if (menu.crew.total - #menu.crew.fired) > 0 then
			menu.insertWare(currentEquipment, "crew", menu.crew.ware, menu.crew.total - #menu.crew.fired)
		end
	elseif menu.crew.total > 0 then
		menu.insertWare(currentEquipment, "crew", menu.crew.ware, menu.crew.total)
	end

	-- Repair
	local objectidstring = tostring(menu.object)
	if menu.repairplan and menu.repairplan[objectidstring] then
		for componentidstring in pairs(menu.repairplan[objectidstring]) do
			if componentidstring ~= "processed" then
				menu.insertComponent(repairedEquipment, componentidstring, "normal")
			end
		end
	end

	local totalprice = 0
	if menu.object == 0 then
		if not menu.isReadOnly then
			totalprice = totalprice + tonumber(C.GetBuildWarePrice(menu.container, GetMacroData(menu.macro, "ware") or ""))
		end
	end
	for _, data in pairs(removedEquipment) do
		for _, entry in ipairs(data) do
			totalprice = totalprice - entry.amount * entry.price
		end
	end
	for _, data in pairs(newEquipment) do
		for _, entry in ipairs(data) do
			totalprice = totalprice + entry.amount * entry.price
		end
	end
	for _, entry in ipairs(repairedEquipment) do
		totalprice = totalprice + entry.price
	end
	return RoundTotalTradePrice(totalprice)
end

function menu.displayPlan(frame)
	-- errors & warnings
	menu.criticalerrors = {}
	menu.errors = {}
	menu.warnings = {}

	if (menu.mode == "purchase") and (not menu.validLicence) and (menu.macro ~= "") then
		local haslicence, icon, overridecolor, mouseovertext = menu.checkLicence(menu.macro, true, nil, true)
		menu.errors[2] = mouseovertext
	end

	if ((menu.mode == "upgrade") or (menu.mode == "modify")) and (menu.object ~= 0) then
		if menu.tasks[tostring(menu.object)] then
			menu.errors[3] = ReadText(1001, 8521)
		end
	end

	for _, upgradetype in ipairs(Helper.upgradetypes) do
		local slots = menu.upgradeplan[upgradetype.type]
		for slot, macro in pairs(slots) do
			-- current allowempty warning
			if upgradetype.allowempty == false then
				if macro == "" then
					menu.errors[1] = ReadText(1001, 8020)
					break
				end
			end
		end
		-- missing software
		if upgradetype.supertype == "software" then
			if menu.software[upgradetype.type] then
				for slot, slotdata in ipairs(menu.software[upgradetype.type]) do
					if #slotdata.possiblesoftware > 0 then
						if (slotdata.defaultsoftware ~= 0) and (menu.upgradeplan[upgradetype.type][slot] == "") then
							menu.errors[1] = ReadText(1001, 8020)
							break
						end
					end
				end
			end
		end
		-- missing captain
		if not menu.captainSelected then
			menu.errors[1] = ReadText(1001, 8020)
		end
		-- current ammo warning
		if upgradetype.supertype == "ammo" then
			local total, capacity = menu.getAmmoUsage(upgradetype.type)
			if total > capacity then
				menu.warnings[2] = ReadText(1001, 8021)
			end
		end
	end
	-- current crew warning
	if #menu.crew.unassigned > 0 then
		menu.warnings[3] = ReadText(1001, 8022)
	end
	-- resource warning
	if menu.mode == "purchase" then
		local considerCurrent = false
		if (menu.macro ~= "") and (menu.editingshoppinglist == nil) then
			considerCurrent = true
		end
		local numorders = #menu.shoppinglist + (considerCurrent and 1 or 0)
		local buildorders = ffi.new("UIBuildOrderList[?]", numorders)
		for i, entry in ipairs(menu.shoppinglist) do
			buildorders[i - 1].shipid = 0
			buildorders[i - 1].macroname = Helper.ffiNewString(entry.macro)
			buildorders[i - 1].loadout = Helper.callLoadoutFunction(entry.upgradeplan, nil, function (loadout, _) return loadout end, false)
			buildorders[i - 1].amount = entry.amount
		end
		if considerCurrent then
			local index = #menu.shoppinglist
			buildorders[index].shipid = 0
			buildorders[index].macroname = Helper.ffiNewString(menu.macro)
			buildorders[index].loadout = Helper.callLoadoutFunction(menu.upgradeplan, nil, function (loadout, _) return loadout end, false)
			buildorders[index].amount = 1
		end

		menu.missingResources = {}
		local n = C.GetNumMissingBuildResources(menu.container, buildorders, numorders)
		local buf = ffi.new("UIWareInfo[?]", n)
		n = C.GetMissingBuildResources(buf, n)
		for i = 0, n - 1 do
			table.insert(menu.missingResources, { ware = ffi.string(buf[i].ware), amount = buf[i].amount })
		end
		if (not menu.isReadOnly) and (#menu.missingResources > 0) then
			menu.warnings[4] = ReadText(1001, 8018)
		end
	elseif menu.mode == "upgrade" then
		local considerCurrent = false
		if (menu.object ~= 0) and (menu.editingshoppinglist == nil) then
			considerCurrent = true
		end
		local numorders = #menu.shoppinglist + (considerCurrent and 1 or 0)
		local buildorders = ffi.new("UIBuildOrderList[?]", numorders)
		for i, entry in ipairs(menu.shoppinglist) do
			buildorders[i - 1].shipid = entry.object
			buildorders[i - 1].macroname = ""
			buildorders[i - 1].loadout = Helper.callLoadoutFunction(entry.upgradeplan, nil, function (loadout, _) return loadout end, false)
			buildorders[i - 1].amount = entry.amount
		end
		if considerCurrent then
			local index = #menu.shoppinglist
			buildorders[index].shipid = menu.object
			buildorders[index].macroname = ""
			buildorders[index].loadout = Helper.callLoadoutFunction(menu.upgradeplan, nil, function (loadout, _) return loadout end, false)
			buildorders[index].amount = 1
		end

		menu.missingResources = {}
		if menu.container then
			local n = C.GetNumMissingLoadoutResources(menu.container, buildorders, numorders)
			local buf = ffi.new("UIWareInfo[?]", n)
			n = C.GetMissingLoadoutResources(buf, n)
			for i = 0, n - 1 do
				table.insert(menu.missingResources, { ware = ffi.string(buf[i].ware), amount = buf[i].amount })
			end
		end
		if (not menu.isReadOnly) and (#menu.missingResources > 0) then
			menu.warnings[4] = ReadText(1001, 8018)
		end
	end

	if not menu.isReadOnly then
		local constructions = {}
		-- current builds
		local n = C.GetNumBuildTasks(menu.container, true, true)
		menu.buildInProgress = n
		-- other builds
		n = C.GetNumBuildTasks(menu.container, false, true)
		local buf = ffi.new("BuildTaskInfo[?]", n)
		n = C.GetBuildTasks(buf, n, menu.container, false, true)
		for i = 0, n - 1 do
			table.insert(constructions, { id = buf[i].id, component = buf[i].component, macro = ffi.string(buf[i].macro), factionid = ffi.string(buf[i].factionid), buildercomponent = buf[i].buildercomponent, price = buf[i].price, inprogress = false })
		end
		menu.queuePosition = #constructions + 1
		for i, construction in ipairs(constructions) do
			if (not construction.inprogress) and (construction.factionid ~= "player") then
				menu.queuePosition = i
				break
			end
		end
	end

	menu.shoppinglisttotal = 0
	menu.timetotal = 0
	for i, entry in ipairs(menu.shoppinglist) do
		entry.color = menu.warnings[4] and Helper.color.orange or Helper.color.white
		if i ~= menu.editingshoppinglist then
			menu.timetotal = menu.timetotal + math.ceil(entry.amount / C.GetNumSuitableBuildProcessors(menu.container, entry.object, entry.macro)) * entry.duration
			menu.shoppinglisttotal = menu.shoppinglisttotal + entry.amount * entry.price
			-- ammo error
			if entry.warnings[2] then
				entry.color = Helper.color.red
				menu.criticalerrors[3] = ReadText(1001, 8016)
			end
			-- crew error
			if #entry.crew.unassigned > 0 then
				entry.color = Helper.color.red
				menu.criticalerrors[4] = ReadText(1001, 8017)
			end
		end
		for _, upgradetype in ipairs(Helper.upgradetypes) do
			local slots = entry.upgradeplan[upgradetype.type]
			for slot, macro in pairs(slots) do
				-- allowempty error
				if upgradetype.allowempty == false then
					if macro == "" then
						if i ~= menu.editingshoppinglist then
							entry.color = Helper.color.red
							menu.criticalerrors[2] = ReadText(1001, 8015)
						end
						break
					end
				end
			end
		end
	end
	-- money error
	local playerMoney = GetPlayerMoney()
	if menu.shoppinglisttotal > playerMoney then
		menu.criticalerrors[1] = ReadText(1001, 8014)
	end

	Helper.ffiClearNewHelper()
	-- edit warning
	if (menu.macro ~= "") or (menu.object ~= 0) then
		menu.warnings[5] = ReadText(1001, 8019)
	end

	-- BUTTONS
	local buttontable = frame:addTable(2, { tabOrder = 6, width = menu.planData.width, height = Helper.scaleY(Helper.standardButtonHeight), x = menu.planData.offsetX, y = 0, reserveScrollBar = false, backgroundID = "solid", backgroundColor = Helper.color.transparent60 })
	local row
	if menu.isReadOnly then
		row = buttontable:addRow(true, { fixed = true, bgColor = Helper.color.transparent })
		row[2]:createButton({ }):setText(ReadText(1001, 8035), { halign = "center" })
		row[2].handlers.onClick = function () return menu.closeMenu("back") end
	else
		row = buttontable:addRow(true, { fixed = true, bgColor = Helper.defaultTitleBackgroundColor })
		local button = row[1]:createButton({ active = (#menu.shoppinglist > (menu.editingshoppinglist and 1 or 0)) and (next(menu.criticalerrors) == nil) }):setText(ReadText(1001, 8011), { halign = "center" })
		if (menu.object == 0) and (menu.macro == "") then
			button:setHotkey("INPUT_STATE_DETAILMONITOR_X", { displayIcon = true })
		end
		row[1].handlers.onClick = menu.buttonConfirm
		row[2]:createButton({  }):setText(ReadText(1001, 8010), { halign = "center" })
		row[2].handlers.onClick = function () return menu.closeMenu("back") end
	end
	buttontable.properties.y = Helper.viewHeight - buttontable:getFullHeight() - menu.planData.offsetY

	-- STATUS
	local statustable, resourcetable
	if not menu.isReadOnly then
		statustable = frame:addTable(2, { tabOrder = 7, width = menu.planData.width, x = menu.planData.offsetX, y = 0, reserveScrollBar = false, highlightMode = "off", skipTabChange = true, backgroundID = "solid", backgroundColor = Helper.color.transparent60 })

		local row = statustable:addRow(false, { fixed = true, bgColor = Helper.defaultTitleBackgroundColor })
		row[1]:setColSpan(2):createText(ReadText(1001, 8012), menu.headerTextProperties)

		local infoCount = 0
		local visibleHeight

		local row = statustable:addRow(false, { fixed = true, bgColor = Helper.color.transparent })
		row[1]:createText(ReadText(1001, 8522))
		row[2]:createText(menu.buildInProgress, { halign = "right" })
		infoCount = infoCount + 1

		local row = statustable:addRow(false, { fixed = true, bgColor = Helper.color.transparent })
		row[1]:createText(ReadText(1001, 8523))
		row[2]:createText(menu.queuePosition - 1, { halign = "right" })
		infoCount = infoCount + 1

		local row = statustable:addRow(false, { fixed = true, bgColor = Helper.color.transparent })
		row[1]:createText(ReadText(1001, 8509))
		row[2]:createText("#" .. menu.queuePosition .. " - " .. (menu.warnings[4] and "--:--" or ConvertTimeString(menu.timetotal, (menu.timetotal > 3600) and "%h:%M:%S" or "%M:%S")), { halign = "right" })
		infoCount = infoCount + 1

		local row = statustable:addRow(false, { fixed = true, bgColor = Helper.color.transparent })
		row[1]:createText(ReadText(1001, 2927))
		row[2]:createText(ConvertMoneyString(menu.shoppinglisttotal, false, true, 0, true, false) .. " " .. ReadText(1001, 101), { halign = "right" })
		infoCount = infoCount + 1

		local row = statustable:addRow(false, { fixed = true, bgColor = Helper.color.transparent })
		row[1]:createText(ReadText(1001, 2003))
		row[2]:createText(function () return ConvertMoneyString(GetPlayerMoney(), false, true, 0, true, false) .. " " .. ReadText(1001, 101) end, { halign = "right" })
		infoCount = infoCount + 1

		local row = statustable:addRow(false, { fixed = true, bgColor = Helper.color.transparent })
		row[1]:createText(ReadText(1001, 2004))
		row[2]:createText(function () return ConvertMoneyString(GetPlayerMoney() - menu.shoppinglisttotal, false, true, 0, true, false) .. " " .. ReadText(1001, 101) end, { halign = "right", color = function () return GetPlayerMoney() - menu.shoppinglisttotal < 0 and Helper.color.red or Helper.color.white end })
		infoCount = infoCount + 1

		for _, errorentry in Helper.orderedPairs(menu.criticalerrors) do
			row = statustable:addRow(true, { bgColor = Helper.color.transparent })
			row[1]:setColSpan(2):createText(errorentry, { color = Helper.color.red, wordwrap = true })
			infoCount = infoCount + 1
			if infoCount == config.maxStatusRowCount then
				visibleHeight = statustable:getFullHeight()
			end
		end
		for _, errorentry in Helper.orderedPairs(menu.errors) do
			row = statustable:addRow(true, { bgColor = Helper.color.transparent })
			row[1]:setColSpan(2):createText(errorentry, { color = Helper.color.red, wordwrap = true })
			infoCount = infoCount + 1
			if infoCount == config.maxStatusRowCount then
				visibleHeight = statustable:getFullHeight()
			end
		end
		for _, warningentry in Helper.orderedPairs(menu.warnings) do
			row = statustable:addRow(true, { bgColor = Helper.color.transparent })
			row[1]:setColSpan(2):createText(warningentry, { color = Helper.color.orange, wordwrap = true })
			infoCount = infoCount + 1
			if infoCount == config.maxStatusRowCount then
				visibleHeight = statustable:getFullHeight()
			end
		end
		if (not next(menu.criticalerrors)) and (not next(menu.errors)) and (not next(menu.warnings)) then
			row = statustable:addRow(true, { bgColor = Helper.color.transparent })
			row[1]:setColSpan(2):createText(ReadText(1001, 7923), { color = Helper.color.green })
			infoCount = infoCount + 1
			if infoCount == config.maxStatusRowCount then
				visibleHeight = statustable:getFullHeight()
			end
		end

		if visibleHeight then
			statustable.properties.maxVisibleHeight = visibleHeight
		else
			statustable.properties.maxVisibleHeight = statustable:getFullHeight()
		end
		statustable.properties.y = buttontable.properties.y - statustable:getVisibleHeight() - 2 * Helper.borderSize

		if #menu.missingResources > 0 then
			resourcetable = frame:addTable(2, { tabOrder = 8, width = menu.planData.width, x = menu.planData.offsetX, y = 0, reserveScrollBar = true, highlightMode = "off", skipTabChange = true, backgroundID = "solid", backgroundColor = Helper.color.transparent60 })

			local row = resourcetable:addRow(false, { fixed = true, bgColor = Helper.defaultTitleBackgroundColor })
			row[1]:setColSpan(2):createText(ReadText(1001, 8046), menu.headerTextProperties)

			local visibleHeight
			for i, entry in ipairs(menu.missingResources) do
				local row = resourcetable:addRow(true, { bgColor = Helper.color.transparent })
				row[1]:createText(GetWareData(entry.ware, "name"))
				row[2]:createText(ConvertIntegerString(entry.amount, true, 0, true), { halign = "right", color = Helper.color.orange })
				if i == 5 then
					visibleHeight = resourcetable:getFullHeight()
				end
			end

			if visibleHeight then
				resourcetable.properties.maxVisibleHeight = visibleHeight
			else
				resourcetable.properties.maxVisibleHeight = resourcetable:getFullHeight()
			end
			resourcetable.properties.y = statustable.properties.y - resourcetable:getVisibleHeight() - 2 * Helper.borderSize
		end
	end

	-- SHOPPINGLIST
	local maxVisibleHeight = buttontable.properties.y - menu.planData.offsetY
	if not menu.isReadOnly then
		if #menu.missingResources > 0 then
			maxVisibleHeight = resourcetable.properties.y - menu.planData.offsetY
		else
			maxVisibleHeight = statustable.properties.y - menu.planData.offsetY
		end
	end
	local ftable = frame:addTable(7, { tabOrder = 3, width = menu.planData.width, maxVisibleHeight = maxVisibleHeight, x = menu.planData.offsetX, y = menu.planData.offsetY, reserveScrollBar = true, backgroundID = "solid", backgroundColor = Helper.color.transparent60 })
	ftable:setColWidth(1, Helper.scaleY(Helper.standardTextHeight), false)
	ftable:setColWidth(2, Helper.scaleY(Helper.standardTextHeight), false)
	ftable:setColWidth(5, 0.2 * menu.planData.width, false)
	ftable:setColWidth(6, Helper.scaleY(Helper.standardTextHeight), false)
	ftable:setColWidth(7, Helper.scaleY(Helper.standardTextHeight), false)

	-- currently editing
	local row = ftable:addRow(false, { bgColor = Helper.defaultTitleBackgroundColor })
	row[1]:setColSpan(7):createText(menu.isReadOnly and ReadText(1001, 8045) or ReadText(1001, 8005), menu.headerTextProperties)

	if (menu.object ~= 0) or (menu.macro ~= "") then
		local removedEquipment = {}
		local currentEquipment = {}
		local newEquipment = {}
		local repairedEquipment = {}
		menu.total = menu.checkEquipment(removedEquipment, currentEquipment, newEquipment, repairedEquipment)

		local buildorder = ffi.new("UIBuildOrderList")
		buildorder.shipid = menu.object
		buildorder.macroname = Helper.ffiNewString(menu.macro)
		buildorder.loadout = Helper.callLoadoutFunction(menu.upgradeplan, nil, function (loadout, _) return loadout end)
		buildorder.amount = 1
		if not menu.isReadOnly then
			menu.duration = C.GetBuildDuration(menu.container, buildorder)
		end

		local colspan = 3
		if menu.isReadOnly then
			colspan = 6
		end

		local row = ftable:addRow(true, { bgColor = Helper.color.transparent })
		local name = ""
		if menu.object ~= 0 then
			name = ffi.string(C.GetComponentName(menu.object))
		else
			name = GetMacroData(menu.macro, "name")
		end
		row[1]:setColSpan(colspan + 1):createText(name, { color = menu.warnings[4] and Helper.color.orange or Helper.color.white })
		if not menu.isReadOnly then
			row[5]:setColSpan(3):createText(ConvertMoneyString(menu.total, false, true, 0, true, false) .. " " .. ReadText(1001, 101), { halign = "right" })

			local row = ftable:addRow(false, { bgColor = Helper.color.transparent })
			row[2]:setColSpan(3):createText(ReadText(1001, 8508))
			row[5]:setColSpan(3):createText((menu.warnings[4] and "--:--" or ConvertTimeString(menu.duration, (menu.duration > 3600) and "%h:%M:%S" or "%M:%S")), { halign = "right" })
		end

		if next(removedEquipment) or next(currentEquipment) or next(newEquipment) or (#repairedEquipment > 0) or (menu.object == 0) then
			-- chassis
			if menu.object == 0 then
				local ware = GetMacroData(menu.macro, "ware")
				local isextended = menu.isUpgradeExpanded(menu.currentIdx, ware, "chassis")
				local name, resources = GetWareData(ware, "name", "resources")
				row = ftable:addRow(true, { bgColor = Helper.color.transparent })
				if (resources ~= nil) and (#resources > 0) then
					row[1]:createButton({ height = Helper.standardTextHeight }):setText(isextended and "-" or "+", { halign = "center" })
					row[1].handlers.onClick = function () return menu.expandUpgrade(menu.currentIdx, ware, "chassis", row.index) end
				end
				row[2]:setColSpan(colspan):createText(ReadText(1001, 8008), { color = Helper.color.green })
				if not menu.isReadOnly then
					row[5]:setColSpan(3):createText(ConvertMoneyString(tonumber(C.GetBuildWarePrice(menu.container, ware or "")), false, true, 0, true, false) .. " " .. ReadText(1001, 101), { halign = "right", color = Helper.color.green })
				end
				if resources and (#resources > 0) and isextended then
					menu.displayUpgradeResources(ftable, resources, 1)
				end
			end
			for _, entry in ipairs(config.leftBar) do
				if removedEquipment[entry.mode] or currentEquipment[entry.mode] or newEquipment[entry.mode] then
					row = ftable:addRow(true, { bgColor = Helper.color.transparent })
					row[2]:setColSpan(6):createText(entry.name,{ font = Helper.standardFontBold, titleColor = Helper.defaultSimpleBackgroundColor })
				end
				-- removed
				if removedEquipment[entry.mode] then
					for _, entry in ipairs(removedEquipment[entry.mode]) do
						local isextended = menu.isUpgradeExpanded(menu.currentIdx, entry.ware, "removed")
						local name, resources = GetWareData(entry.ware, "name", "resources")
						local mouseOverText = name .. "\n" .. string.format(ReadText(1026, 8010), ConvertMoneyString(-entry.price, false, true, 0, true, false) .. " " .. ReadText(1001, 101))
						row = ftable:addRow(true, { bgColor = Helper.color.transparent })
						if (resources ~= nil) and (#resources > 0) then
							row[1]:createButton({ height = Helper.standardTextHeight }):setText(isextended and "-" or "+", { halign = "center" })
							row[1].handlers.onClick = function () return menu.expandUpgrade(menu.currentIdx, entry.ware, "removed", row.index) end
						end
						row[2]:setColSpan(colspan):createText(entry.amount .. ReadText(1001, 42) .. " " .. name, { color = Helper.color.red, mouseOverText = mouseOverText })
						if entry.price then
							row[5]:setColSpan(3):createText(ConvertMoneyString(-entry.amount * entry.price, false, true, 0, true, false) .. " " .. ReadText(1001, 101), { halign = "right", color = Helper.color.red, mouseOverText = mouseOverText })
						end
						if resources and (#resources > 0) and isextended then
							menu.displayUpgradeResources(ftable, resources, entry.amount)
						end
					end
				end
				-- current
				if currentEquipment[entry.mode] then
					for _, entry in ipairs(currentEquipment[entry.mode]) do
						local isextended = menu.isUpgradeExpanded(menu.currentIdx, entry.ware, "current")
						local name, resources = GetWareData(entry.ware, "name", "resources")
						row = ftable:addRow(true, { bgColor = Helper.color.transparent })
						if (resources ~= nil) and (#resources > 0) then
							row[1]:createButton({ height = Helper.standardTextHeight }):setText(isextended and "-" or "+", { halign = "center" })
							row[1].handlers.onClick = function () return menu.expandUpgrade(menu.currentIdx, entry.ware, "current", row.index) end
						end
						row[2]:setColSpan(colspan):createText(entry.amount .. ReadText(1001, 42) .. " " .. GetWareData(entry.ware, "name"), { mouseOverText = name })
						if resources and (#resources > 0) and isextended then
							menu.displayUpgradeResources(ftable, resources, entry.amount)
						end
					end
				end
				-- new
				if newEquipment[entry.mode] then
					for _, entry in ipairs(newEquipment[entry.mode]) do
						local isextended = menu.isUpgradeExpanded(menu.currentIdx, entry.ware, "new")
						local name, resources = GetWareData(entry.ware, "name", "resources")
						local mouseOverText = name .. "\n" .. string.format(ReadText(1026, 8010), ConvertMoneyString(entry.price, false, true, 0, true, false) .. " " .. ReadText(1001, 101))
						row = ftable:addRow(true, { bgColor = Helper.color.transparent })
						if (resources ~= nil) and (#resources > 0) then
							row[1]:createButton({ height = Helper.standardTextHeight }):setText(isextended and "-" or "+", { halign = "center" })
							row[1].handlers.onClick = function () return menu.expandUpgrade(menu.currentIdx, entry.ware, "new", row.index) end
						end
						row[2]:setColSpan(colspan):createText(entry.amount .. ReadText(1001, 42) .. " " .. name, { color = Helper.color.green, mouseOverText = mouseOverText })
						if entry.price then
							row[5]:setColSpan(3):createText(ConvertMoneyString(entry.amount * entry.price, false, true, 0, true, false) .. " " .. ReadText(1001, 101), { halign = "right", color = Helper.color.green, mouseOverText = mouseOverText })
						end
						if resources and (#resources > 0) and isextended then
							menu.displayUpgradeResources(ftable, resources, entry.amount)
						end
					end
				end
			end
			-- repaired
			if #repairedEquipment > 0 then
				row = ftable:addRow(true, { bgColor = Helper.color.transparent })
				row[2]:setColSpan(6):createText(ReadText(1001, 3000),{ font = Helper.standardFontBold, titleColor = Helper.defaultSimpleBackgroundColor })
			end
			for _, entry in ipairs(repairedEquipment) do
				row = ftable:addRow(true, { bgColor = Helper.color.transparent })
				local repaircomponent = ConvertStringTo64Bit(entry.component)
				local name = ffi.string(C.GetComponentName(repaircomponent))
				row[2]:setColSpan(colspan):createText(ReadText(1001, 4217) .. ReadText(1001, 120) .. " " .. name .. " (" .. (100 - GetComponentData(repaircomponent, "hullpercent")) .. "% " .. ReadText(1001, 1) .. ")", { color = Helper.color.green, mouseOverText = name })
				row[5]:setColSpan(3):createText(ConvertMoneyString(tonumber(C.GetRepairPrice(repaircomponent, menu.container)), false, true, 0, true, false) .. " " .. ReadText(1001, 101), { halign = "right", color = Helper.color.green })
			end
		else
			row = ftable:addRow(true, { bgColor = Helper.color.transparent })
			row[2]:setColSpan(6):createText("--- " .. ReadText(1001, 7936) .. " ---", { halign = "center" } )
		end

		if not menu.isReadOnly then
			local row = ftable:addRow(true, { bgColor = Helper.color.transparent })
			local hasupgrades = (next(removedEquipment) ~= nil) or (next(newEquipment) ~= nil)
			local hasrepairs = (#repairedEquipment > 0)
			local mouseovertext = ""
			for i, error in Helper.orderedPairs(menu.errors) do
				if (i >= 1) and (i <= 3) then
					if mouseovertext ~= "" then
						mouseovertext = mouseovertext .. "\n"
					else
						mouseovertext = "\27R"
					end
					mouseovertext = mouseovertext .. error
				end
			end
			local button = row[4]:setColSpan(4):createButton({ active = (not menu.errors[1]) and (not menu.errors[2]) and (not menu.errors[3]) and (hasupgrades or hasrepairs), mouseOverText = mouseovertext }):setText(ReadText(1001, 8006), { halign = "center" })
			if (menu.object ~= 0) or (menu.macro ~= "") then
				button:setHotkey("INPUT_STATE_DETAILMONITOR_X", { displayIcon = true })
			end
			row[4].handlers.onClick = menu.editingshoppinglist and function () menu.buttonConfirmPurchaseEdit(hasupgrades, hasrepairs) end or function () menu.buttonAddPurchase(hasupgrades, hasrepairs) end
		end
	else
		-- nothing selected
		local row = ftable:addRow(true, { bgColor = Helper.color.transparent })
		row[2]:setColSpan(6):createText(ReadText(1001, 8007))

		local row = ftable:addRow(true, { bgColor = Helper.color.transparent })
		row[4]:setColSpan(4):createButton({ active = false }):setText(ReadText(1001, 8006), { halign = "center" })
	end

	if not menu.isReadOnly then
		-- shoppinglist
		local row = ftable:addRow(false, { bgColor = Helper.defaultTitleBackgroundColor })
		row[1]:setColSpan(7):createText(ReadText(1001, 8009), menu.headerTextProperties)

		if next(menu.shoppinglist) then
			for i, entry in ipairs(menu.shoppinglist) do
				if i ~= menu.editingshoppinglist then
					local row = ftable:addRow(true, { bgColor = Helper.color.transparent })
					if menu.mode == "purchase" then
						-- set amount
						row[1]:createButton({ active = entry.amount > 1 }):setText("-", { halign = "center" })
						row[1].handlers.onClick = function () return menu.buttonChangePurchaseAmount(i, -1) end
						local active = playerMoney - menu.shoppinglisttotal - entry.price > 0
						row[2]:createButton({ active = active, mouseOverText = active and "" or ReadText(1026, 8008) }):setText("+", { halign = "center" })
						row[2].handlers.onClick = function () return menu.buttonChangePurchaseAmount(i, 1) end
					end
					-- name
					local name = ""
					if entry.object ~= 0 then
						name = ffi.string(C.GetComponentName(entry.object))
					else
						name = GetMacroData(entry.macro, "name")
					end
					row[3]:setColSpan(2):createText(entry.amount .. ReadText(1001, 42) .. " " .. name, { color = entry.color, mouseOverText = menu.getLoadoutSummary(entry.upgradeplan, entry.crew, menu.repairplan and menu.repairplan[tostring(entry.object)]) })
					-- price
					row[5]:createText(ConvertMoneyString(entry.amount * (entry.price or 0), false, true, 0, true, false) .. " " .. ReadText(1001, 101), { halign = "right" })
					-- edit & remove
					row[6]:createButton({  }):setIcon("menu_edit")
					row[6].handlers.onClick = function () return menu.buttonEditPurchase(i) end
					row[7]:createButton({  }):setText("x", { halign = "center" })
					row[7].handlers.onClick = function () return menu.buttonRemovePurchase(i) end
					
					local row = ftable:addRow(false, { bgColor = Helper.color.transparent })
					row[3]:setColSpan(2):createText("   " .. ReadText(1001, 8508))
					local duration = math.ceil(entry.amount / C.GetNumSuitableBuildProcessors(menu.container, entry.object, entry.macro)) * entry.duration
					row[5]:setColSpan(3):createText((menu.warnings[4] and "- -:- -" or ConvertTimeString(duration, (duration > 3600) and "%h:%M:%S" or "%M:%S")), { halign = "right" })
				end
			end
		else
			local row = ftable:addRow(true, { bgColor = Helper.color.transparent })
			row[2]:setColSpan(6):createText("--- " .. ReadText(1001, 34) .. " ---")
		end
	end

	menu.topRows.plan = nil
	menu.selectedRows.plan = nil
	menu.selectedCols.plan = nil
end

function menu.displayUpgradeResources(ftable, resources, upgradeamount)
	local colspan = 3
	if menu.isReadOnly then
		colspan = 6
	end

	for _, resource in ipairs(resources) do
		local ismissing = false
		for _, entry in ipairs(menu.missingResources) do
			if entry.ware == resource.ware then
				ismissing = true
				break
			end
		end
		local row = ftable:addRow(true, { bgColor = Helper.color.transparent })
		local color = Helper.color.white
		if ismissing then
			color = Helper.color.red
		end
		row[2]:setColSpan(colspan):createText("   " .. upgradeamount * resource.amount .. ReadText(1001, 42) .. " " .. GetWareData(resource.ware, "name"), { color = color, mouseOverText = mouseOverText })
	end
end

function menu.displayModifyPlan(frame)
	-- errors & warnings
	menu.criticalerrors = {}
	menu.errors = {}
	if menu.upgradetypeMode == "paintmods" then
		menu.warnings = {
			[1] = ReadText(1001, 8518)
		}
	else
		menu.warnings = {
			[1] = ReadText(1001, 8036)
		}
	end

	menu.shoppinglisttotal = 0

	-- BUTTONS
	local buttontable = frame:addTable(2, { tabOrder = 6, width = menu.planData.width, height = Helper.scaleY(Helper.standardButtonHeight), x = menu.planData.offsetX, y = Helper.viewHeight - Helper.scaleY(Helper.standardButtonHeight) - menu.planData.offsetY, reserveScrollBar = false, backgroundID = "solid", backgroundColor = Helper.color.transparent60 })
	local row = buttontable:addRow(true, { fixed = true, bgColor = Helper.color.transparent })
	row[2]:createButton({ }):setText(ReadText(1001, 8035), { halign = "center" })
	row[2].handlers.onClick = function () return menu.closeMenu("back") end

	-- STATUS
	local statustable = frame:addTable(2, { tabOrder = 7, width = menu.planData.width, x = menu.planData.offsetX, y = 0, reserveScrollBar = false, highlightMode = "off", skipTabChange = true, backgroundID = "solid", backgroundColor = Helper.color.transparent60 })

	local row = statustable:addRow(false, { fixed = true, bgColor = Helper.defaultTitleBackgroundColor })
	row[1]:setColSpan(2):createText(ReadText(1001, 8012), menu.headerTextProperties)

	local infoCount = 0
	local visibleHeight

	local row = statustable:addRow(false, { fixed = true, bgColor = Helper.color.transparent })
	row[1]:createText(ReadText(1001, 2003))
	row[2]:createText(function () return ConvertMoneyString(GetPlayerMoney(), false, true, 0, true, false) .. " " .. ReadText(1001, 101) end, { halign = "right" })
	infoCount = infoCount + 1

	for _, errorentry in Helper.orderedPairs(menu.criticalerrors) do
		row = statustable:addRow(true, { bgColor = Helper.color.transparent })
		row[1]:setColSpan(2):createText(errorentry, { color = Helper.color.red, wordwrap = true })
		infoCount = infoCount + 1
		if infoCount == 4 then
			visibleHeight = statustable:getFullHeight()
		end
	end
	for _, errorentry in Helper.orderedPairs(menu.errors) do
		row = statustable:addRow(true, { bgColor = Helper.color.transparent })
		row[1]:setColSpan(2):createText(errorentry, { color = Helper.color.red, wordwrap = true })
		infoCount = infoCount + 1
		if infoCount == 4 then
			visibleHeight = statustable:getFullHeight()
		end
	end
	for _, warningentry in Helper.orderedPairs(menu.warnings) do
		row = statustable:addRow(true, { bgColor = Helper.color.transparent })
		row[1]:setColSpan(2):createText(warningentry, { color = Helper.color.orange, wordwrap = true })
		infoCount = infoCount + 1
		if infoCount == 4 then
			visibleHeight = statustable:getFullHeight()
		end
	end
	if (not next(menu.criticalerrors)) and (not next(menu.errors)) and (not next(menu.warnings)) then
		row = statustable:addRow(true, { bgColor = Helper.color.transparent })
		row[1]:setColSpan(2):createText(ReadText(1001, 7923), { color = Helper.color.green })
		infoCount = infoCount + 1
		if infoCount == 4 then
			visibleHeight = statustable:getFullHeight()
		end
	end

	if visibleHeight then
		statustable.properties.maxVisibleHeight = visibleHeight
	else
		statustable.properties.maxVisibleHeight = statustable:getFullHeight()
	end
	statustable.properties.y = buttontable.properties.y - statustable:getVisibleHeight() - 2 * Helper.borderSize

	-- PRICE LIST
	local ftable = frame:addTable(2, { tabOrder = 0, width = menu.planData.width, maxVisibleHeight = statustable.properties.y - menu.planData.offsetY, x = menu.planData.offsetX, y = menu.planData.offsetY, reserveScrollBar = true, backgroundID = "solid", backgroundColor = Helper.color.transparent60 })
	ftable:setColWidth(2, 0.3 * menu.planData.width)

	local row = ftable:addRow(false, { bgColor = Helper.defaultTitleBackgroundColor })
	row[1]:setColSpan(2):createText(ReadText(1001, 8037), menu.headerTextProperties)
	
	if menu.upgradetypeMode == "paintmods" then
		local row = ftable:addRow(false, { bgColor = Helper.color.transparent })
		row[1]:setColSpan(2):createText(ReadText(1001, 8517))
	else
		for i = #Helper.modQualities, 1, -1 do
			local entry = Helper.modQualities[i]
			local row = ftable:addRow(false, { bgColor = Helper.color.transparent })
			row[1]:createText(entry.name)
			row[2]:createText(ConvertMoneyString(entry.price, false, true, 0, true, false) .. " " .. ReadText(1001, 101), { halign = "right" })
		end
	end

	-- SELECTEDSHIPS
	if menu.modeparam[1] then
		local row = ftable:addRow(false, { bgColor = Helper.defaultTitleBackgroundColor })
		row[1]:setColSpan(2):createText(ReadText(1001, 8519), menu.headerTextProperties)
		for _, ship in pairs(menu.modeparam[2]) do
			local row = ftable:addRow(false, { bgColor = Helper.color.transparent })
			row[1]:createText(ffi.string(C.GetComponentName(ship)) .. " (" .. ffi.string(C.GetObjectIDCode(ship)) .. ")", { color = Helper.color.green })
			local paintmod = ffi.new("UIPaintMod")
			if C.GetInstalledPaintMod(ship, paintmod) then
				row[2]:createText(ffi.string(paintmod.Name), { color = Helper.modQualities[paintmod.Quality].color, halign = "right" })
			end
		end
	end

	menu.topRows.plan = nil
	menu.selectedRows.plan = nil
	menu.selectedCols.plan = nil
end

function menu.displayStats(frame)
	local loadoutstats, currentloadoutstats
	if menu.mode == "purchase" then
		local ffiloadoutstats = Helper.callLoadoutFunction(menu.upgradeplan, nil, function (loadout, _) return C.GetLoadoutStatistics(0, menu.macro, loadout) end)
		loadoutstats = Helper.convertLoadoutStats(ffiloadoutstats)

		currentloadoutstats = Helper.convertLoadoutStats(ffi.new("UILoadoutStatistics", 0))
	elseif menu.mode == "upgrade" then
		local ffiloadoutstats = Helper.callLoadoutFunction(menu.upgradeplan, nil, function (loadout, _) return C.GetLoadoutStatistics(menu.object, "", loadout) end)
		loadoutstats = Helper.convertLoadoutStats(ffiloadoutstats)

		local fficurrentloadoutstats = C.GetCurrentLoadoutStatistics(menu.object)
		currentloadoutstats = Helper.convertLoadoutStats(fficurrentloadoutstats)
	elseif menu.mode == "modify" then
		local fficurrentloadoutstats = C.GetCurrentLoadoutStatistics(menu.object)
		loadoutstats = Helper.convertLoadoutStats(fficurrentloadoutstats)

		currentloadoutstats = menu.initialLoadoutStatistics
	end

	local ffimaxloadoutstats = C.GetMaxLoadoutStatistics(menu.object, menu.macro)
	local maxloadoutstats = Helper.convertLoadoutStats(ffimaxloadoutstats)

	local ftable = frame:addTable(6, { tabOrder = 0, width = menu.statsData.width, x = menu.statsData.offsetX, y = 0, reserveScrollBar = false, backgroundID = "solid", backgroundColor = Helper.color.transparent60 })
	ftable:setColWidthPercent(2, 10)
	ftable:setColWidthPercent(5, 10)

	local numRows = math.ceil(#config.stats / 2)
	for i = 1, numRows do
		local row = ftable:addRow(false, { bgColor = Helper.color.transparent })
		local entry = config.stats[i]
		if entry.id ~= "" then
			row[1]:createText(entry.name .. ((entry.unit ~= "") and (" (" .. entry.unit .. ")") or ""))
			local color = Helper.color.white
			if loadoutstats[entry.id] > currentloadoutstats[entry.id] then
				color = Helper.color.green
			elseif loadoutstats[entry.id] < currentloadoutstats[entry.id] then
				color = Helper.color.red
			end
			local value
			if entry.type == "UINT" then
				value = ConvertIntegerString(Helper.round(loadoutstats[entry.id], 0), true, 0, true, false)
			elseif entry.type == "float" then
				local int, frac = math.modf(Helper.round(loadoutstats[entry.id], entry.accuracy))
				value = ConvertIntegerString(int, true, 0, true, false)
				if entry.accuracy > 0 then
					frac = Helper.round(math.abs(frac or 0) * (10 ^ entry.accuracy))
					value = value .. ReadText(1001, 105) .. string.format("%0".. (entry.accuracy) .."d", frac)
				end
			end
			row[2]:createText(value, { halign = "right", color = color })
			row[3]:createStatusBar({ current = loadoutstats[entry.id], start = currentloadoutstats[entry.id], max = maxloadoutstats[entry.id], cellBGColor = Helper.color.transparent60 })
		end
		if i + numRows <= #config.stats then
			local entry2 = config.stats[i + numRows]
			if entry2.id ~= "" then
				row[4]:createText(entry2.name .. ((entry2.unit ~= "") and (" (" .. entry2.unit .. ")") or ""))
				local color = Helper.color.white
				if loadoutstats[entry2.id] > currentloadoutstats[entry2.id] then
					color = Helper.color.green
				elseif loadoutstats[entry2.id] < currentloadoutstats[entry2.id] then
					color = Helper.color.red
				end
				local value
				if entry2.type == "UINT" then
					value = ConvertIntegerString(Helper.round(loadoutstats[entry2.id], 0), true, 0, true, false)
				elseif entry2.type == "float" then
					local int, frac = math.modf(Helper.round(loadoutstats[entry2.id], entry2.accuracy))
					value = ConvertIntegerString(int, true, 0, true, false)
					if entry2.accuracy > 0 then
						frac = Helper.round(math.abs(frac or 0) * (10 ^ entry2.accuracy))
						value = value .. ReadText(1001, 105) .. string.format("%0".. (entry2.accuracy) .."d", frac)
					end
				end
				row[5]:createText(value, { halign = "right", color = color })
				row[6]:createStatusBar({ current = loadoutstats[entry2.id], start = currentloadoutstats[entry2.id], max = maxloadoutstats[entry2.id], cellBGColor = Helper.color.transparent60 })
			end
		end
	end

	ftable.properties.y = Helper.viewHeight - ftable:getVisibleHeight() - menu.statsData.offsetY
	menu.statsTableOffsetY = ftable.properties.y
end

function menu.evaluateShipOptions()
	local classOptions = {}
	for _, class in ipairs(config.classorder) do
		if menu.mode == "purchase" then
			if menu.availableshipmacrosbyclass[class] then
				table.insert(classOptions, { id = class, text = ReadText(1001, 8026) .. " " .. menu.getClassText(class), icon = "", displayremoveoption = false })
			end
		elseif (menu.mode == "upgrade") or (menu.mode == "modify") then
			if menu.selectableshipsbyclass[class] then
				table.insert(classOptions, { id = class, text = ReadText(1001, 8026) .. " " .. menu.getClassText(class), icon = "", displayremoveoption = false })
			end
		end
	end

	local shipOptions = {}
	local curShipOption
	if menu.mode == "purchase" then
		if menu.class ~= "" then
			for _, macro in ipairs(menu.availableshipmacrosbyclass[menu.class]) do
				local haslicence, icon, overridecolor, mouseovertext = menu.checkLicence(macro, true)
				local name, infolibrary, shiptypename = GetMacroData(macro, "name", "infolibrary", "shiptypename")

				table.insert(shipOptions, { id = macro, text = name .. " - " .. shiptypename, icon = icon or "", displayremoveoption = false, overridecolor = overridecolor, mouseovertext = mouseovertext })
				AddKnownItem(infolibrary, macro)
			end
		end
		curShipOption = menu.macro
	elseif (menu.class ~= "") and ((menu.mode == "upgrade") or (menu.mode == "modify")) then
		for _, ship in ipairs(menu.selectableshipsbyclass[menu.class]) do
			table.insert(shipOptions, { id = tostring(ship), text = ffi.string(C.GetComponentName(ship)) .. " (" .. ffi.string(C.GetObjectIDCode(ship)) .. ")", icon = "", displayremoveoption = false })
		end
		curShipOption = tostring(menu.object)
	end
	table.sort(shipOptions, function (a, b) return a.text < b.text end)

	local loadoutOptions = {}
	if next(menu.loadouts) then
		for _, loadout in ipairs(menu.loadouts) do
			table.insert(loadoutOptions, { id = loadout.id, text = loadout.name, icon = "", displayremoveoption = loadout.deleteable, active = loadout.active, mouseovertext = loadout.mouseovertext })
		end
	end

	return classOptions, shipOptions, curShipOption, loadoutOptions
end

function menu.createTitleBar(frame)
	local classOptions, shipOptions, curShipOption, loadoutOptions = menu.evaluateShipOptions()

	if menu.mode == "modify" then
		local ftable = frame:addTable(5, { tabOrder = 4, height = 0, x = menu.titleData.offsetX, y = menu.titleData.offsetY, scaling = false, reserveScrollBar = false })
		ftable:setColWidth(1, menu.titleData.dropdownWidth)
		ftable:setColWidth(2, menu.titleData.dropdownWidth)
		ftable:setColWidth(3, menu.titleData.height)
		ftable:setColWidth(4, menu.titleData.height)
		ftable:setColWidth(5, menu.titleData.height)

		local row = ftable:addRow(true, { fixed = true })
		-- class
		row[1]:createDropDown(classOptions, { startOption = menu.class, active = (not menu.isReadOnly) and (#classOptions > 0), optionWidth = menu.titleData.dropdownWidth }):setTextProperties(config.dropDownTextProperties)
		row[1].handlers.onDropDownConfirmed = menu.dropdownShipClass
		-- ships
		local dropDownIconProperties = {
			width = menu.titleData.height / 2,
			height = menu.titleData.height / 2,
			x = menu.titleData.dropdownWidth - 1.5 * menu.titleData.height,
			y = 0,
		}

		row[2]:createDropDown(shipOptions, { startOption = curShipOption, active = (not menu.isReadOnly) and (menu.class ~= ""), optionWidth = menu.titleData.dropdownWidth, optionHeight = (menu.statsTableOffsetY or Helper.viewHeight) - menu.titleData.offsetY - Helper.frameBorder }):setTextProperties(config.dropDownTextProperties):setIconProperties(dropDownIconProperties)
		row[2].handlers.onDropDownConfirmed = menu.dropdownShip
		-- reset camera
		row[3]:createButton({ active = true, height = menu.titleData.height, mouseOverText = L["Reset view"] }):setIcon("menu_reset_view"):setHotkey("INPUT_STATE_DETAILMONITOR_RESET_VIEW", { displayIcon = false })
		row[3].handlers.onClick = function () return C.ResetMapPlayerRotation(menu.holomap) end
		-- undo
		menu.canundo = false
		row[4]:createButton({ active = menu.canundo, height = menu.titleData.height, mouseOverText = ReadText(1026, 7903) .. " (" .. GetLocalizedKeyName("action", 278) .. ")" }):setIcon("menu_undo")
		row[4].handlers.onClick = function () return menu.undoHelper(true) end
		-- redo
		menu.canredo = false
		row[5]:createButton({ active = menu.canredo, height = menu.titleData.height, mouseOverText = ReadText(1026, 7904) .. " (" .. GetLocalizedKeyName("action", 279) .. ")" }):setIcon("menu_redo")
		row[5].handlers.onClick = function () return menu.undoHelper(false) end
	else
		local ftable = frame:addTable(7, { tabOrder = 4, height = 0, x = menu.titleData.offsetX, y = menu.titleData.offsetY, scaling = false, reserveScrollBar = false })
		ftable:setColWidth(1, config.dropdownRatios.class * menu.titleData.dropdownWidth)
		ftable:setColWidth(2, config.dropdownRatios.ship * menu.titleData.dropdownWidth)
		ftable:setColWidth(3, menu.titleData.dropdownWidth)
		ftable:setColWidth(4, menu.titleData.height)
		ftable:setColWidth(5, menu.titleData.height)
		ftable:setColWidth(6, menu.titleData.height)
		ftable:setColWidth(7, menu.titleData.height)

		local row = ftable:addRow(true, { fixed = true })
		-- class
		row[1]:createDropDown(classOptions, { startOption = menu.class, active = (not menu.isReadOnly) and (#classOptions > 0) }):setTextProperties(config.dropDownTextProperties)
		row[1].handlers.onDropDownConfirmed = menu.dropdownShipClass
		-- ships
		local dropDownIconProperties = {
			width = menu.titleData.height / 2,
			height = menu.titleData.height / 2,
			x = config.dropdownRatios.ship * menu.titleData.dropdownWidth - 1.5 * menu.titleData.height,
			y = 0,
		}
		
		local dropdown = row[2]:createDropDown(shipOptions, { startOption = curShipOption, active = (not menu.isReadOnly) and (menu.class ~= ""), optionHeight = (menu.statsTableOffsetY or Helper.viewHeight) - menu.titleData.offsetY - Helper.frameBorder }):setTextProperties(config.dropDownTextProperties):setIconProperties(dropDownIconProperties)
		row[2].handlers.onDropDownConfirmed = menu.dropdownShip
		local active = true
		if (menu.mode == "purchase") and (menu.macro ~= "") and (not menu.validLicence) then
			active = false
			local haslicence, icon, overridecolor, mouseovertext = menu.checkLicence(menu.macro, true)
			dropdown.properties.text.color = overridecolor
		end
		-- loadout
		row[3]:createDropDown(loadoutOptions, { textOverride = ReadText(1001, 7905), active = (not menu.isReadOnly) and active and ((menu.object ~= 0) or (menu.macro ~= "")) and (next(menu.loadouts) ~= nil), optionWidth = menu.titleData.dropdownWidth + menu.titleData.height + Helper.borderSize, optionHeight = (menu.statsTableOffsetY or Helper.viewHeight) - menu.titleData.offsetY - Helper.frameBorder }):setTextProperties(config.dropDownTextProperties)
		row[3].handlers.onDropDownConfirmed = menu.dropdownLoadout
		row[3].handlers.onDropDownRemoved = menu.dropdownLoadoutRemoved
		-- save
		row[4]:createButton({ active = (not menu.isReadOnly) and active and (menu.object ~= 0), height = menu.titleData.height, mouseOverText = ReadText(1026, 7905) }):setIcon("menu_save")
		row[4].handlers.onClick = menu.buttonTitleSave
		-- reset camera
		row[5]:createButton({ active = true, height = menu.titleData.height, mouseOverText = L["Reset view"] }):setIcon("menu_reset_view"):setHotkey("INPUT_STATE_DETAILMONITOR_RESET_VIEW", { displayIcon = false })
		row[5].handlers.onClick = function () return C.ResetMapPlayerRotation(menu.holomap) end
		-- undo
		menu.canundo = false
		row[6]:createButton({ active = menu.canundo, height = menu.titleData.height, mouseOverText = ReadText(1026, 7903) .. " (" .. GetLocalizedKeyName("action", 278) .. ")" }):setIcon("menu_undo")
		row[6].handlers.onClick = function () return menu.undoHelper(true) end
		-- redo
		menu.canredo = false
		row[7]:createButton({ active = menu.canredo, height = menu.titleData.height, mouseOverText = ReadText(1026, 7904) .. " (" .. GetLocalizedKeyName("action", 279) .. ")" }):setIcon("menu_redo")
		row[7].handlers.onClick = function () return menu.undoHelper(false) end
	end

	menu.topRows.ship = nil
	menu.selectedRows.ship = nil
	menu.selectedCols.ship = nil
end

function menu.refreshTitleBar()
	local text = {
		alignment = "center",
		fontname = Helper.standardFont,
		fontsize = Helper.scaleFont(Helper.standardFont, Helper.standardFontSize),
		color = Helper.color.white,
		x = 0,
		y = 0,
		override = ""
	}

	local classOptions, shipOptions, curShipOption, loadoutOptions = menu.evaluateShipOptions()

	if (menu.mode == "upgrade") and (menu.object ~= 0) then
		menu.damagedcomponents = menu.determineNeededRepairs(menu.object)
	else
		menu.damagedcomponents = {}
	end

	-- class
	Helper.removeDropDownScripts(menu, menu.titlebartable, 1, 1)
	SetCellContent(menu.titlebartable, Helper.createDropDown(classOptions, menu.class, text, nil, true, (not menu.isReadOnly) and (#classOptions > 0), 0, 0, 0, 0, nil, nil, ""), 1, 1)
	Helper.setDropDownScript(menu, nil, menu.titlebartable, 1, 1, nil, menu.dropdownShipClass)
	-- ship
	Helper.removeDropDownScripts(menu, menu.titlebartable, 1, 2)
	local color = Helper.color.white
	local active = true
	if (menu.mode == "purchase") and (menu.macro ~= "") and (not menu.validLicence) then
		active = false
		local haslicence, icon, overridecolor, mouseovertext = menu.checkLicence(menu.macro, true)
		text.color = overridecolor
		color = overridecolor
	end
	local dropDownIconProperties = {
		width = menu.titleData.height / 2,
		height = menu.titleData.height / 2,
		color = color,
		x = config.dropdownRatios.ship * menu.titleData.dropdownWidth - 1.5 * menu.titleData.height,
		y = 0,
	}
	SetCellContent(menu.titlebartable, Helper.createDropDown(shipOptions, curShipOption, text, dropDownIconProperties, true, (not menu.isReadOnly) and (menu.class ~= ""), 0, 0, 0, 0, nil, nil, "", nil, nil, (menu.statsTableOffsetY or Helper.viewHeight) - menu.titleData.offsetY - Helper.frameBorder), 1, 2)
	Helper.setDropDownScript(menu, nil, menu.titlebartable, 1, 2, nil, menu.dropdownShip)

	if menu.mode ~= "modify" then
		-- loadout
		text.override = ReadText(1001, 7905)
		Helper.removeDropDownScripts(menu, menu.titlebartable, 1, 3)
		SetCellContent(menu.titlebartable, Helper.createDropDown(loadoutOptions, "", text, nil, true, (not menu.isReadOnly) and active and ((menu.object ~= 0) or (menu.macro ~= "")) and (next(menu.loadouts) ~= nil), 0, 0, 0, 0, nil, nil, "", menu.titleData.dropdownWidth + menu.titleData.height + Helper.borderSize, nil, (menu.statsTableOffsetY or Helper.viewHeight) - menu.titleData.offsetY - Helper.frameBorder), 1, 3)
		Helper.setDropDownScript(menu, nil, menu.titlebartable, 1, 3, nil, menu.dropdownLoadout, menu.dropdownLoadoutRemoved)
		-- save
		Helper.removeButtonScripts(menu, menu.titlebartable, 1, 4)
		SetCellContent(menu.titlebartable, Helper.createButton(nil, Helper.createButtonIcon("menu_save", nil, 255, 255, 255, 100), true, (not menu.isReadOnly) and active and ((menu.object ~= 0) or (menu.macro ~= "")), 0, 0, 0, menu.titleData.height, nil, nil, nil, ReadText(1026, 7905)), 1, 4)
		Helper.setButtonScript(menu, nil, menu.titlebartable, 1, 4, menu.buttonTitleSave)
	end
end

function menu.displayMenu(firsttime)
	-- Remove possible button scripts from previous view
	Helper.removeAllWidgetScripts(menu, config.infoLayer)
	Helper.currentTableRow = {}
	Helper.closeDropDownOptions(menu.titlebartable, 1, 1)
	Helper.closeDropDownOptions(menu.titlebartable, 1, 2)
	if menu.mode ~= "modify" then
		Helper.closeDropDownOptions(menu.titlebartable, 1, 3)
	end

	menu.infoFrame = Helper.createFrameHandle(menu, {
		layer = config.infoLayer,
		standardButtons = {},
		width = Helper.viewWidth,
		height = Helper.viewHeight,
		x = 0,
		y = 0,
	})

	menu.displayLeftBar(menu.infoFrame)
	
	if ((menu.mode == "purchase") and (menu.macro ~= "")) or ((menu.mode == "upgrade") and (menu.object ~= 0)) then
		menu.displaySlots(menu.infoFrame, firsttime)
	elseif menu.mode == "modify" then
		if menu.upgradetypeMode == "paintmods" then
			menu.displayModifyPaintSlots(menu.infoFrame)
		else
			menu.displayModifySlots(menu.infoFrame)
		end
	else
		menu.displayEmptySlots(menu.infoFrame)
	end
	
	if (menu.mode == "purchase") or (menu.mode == "upgrade") then
		menu.displayPlan(menu.infoFrame)
	elseif menu.mode == "modify" then
		menu.displayModifyPlan(menu.infoFrame)
	end

	menu.statsTableOffsetY = nil
	if ((menu.mode == "purchase") and (menu.macro ~= "")) or ((menu.mode == "upgrade") or (menu.mode == "modify")) and (menu.object ~= 0) then
		menu.displayStats(menu.infoFrame)
	end
	menu.refreshTitleBar()

	menu.infoFrame:display()
end

function menu.displayContextMenu()
	-- Remove possible button scripts from previous view
	Helper.removeAllWidgetScripts(menu, config.contextLayer)
	PlaySound("ui_positive_click")

	local width = 0
	local setup = Helper.createTableSetup(menu)

	if menu.contextMode == 1 then
		width = 300

		local upgradetype = Helper.findUpgradeType(menu.contextData.upgradetype)
		local upgradetype2 = Helper.findUpgradeTypeByGroupType(upgradetype.type)
		local slotdata
		if (menu.upgradetypeMode == "enginegroup") or (menu.upgradetypeMode == "turretgroup") then
			slotdata = menu.groups[menu.currentSlot][upgradetype2.grouptype]
		else
			slotdata = menu.slots[upgradetype.type][menu.contextData.slot]
		end
		local plandata
		if (menu.upgradetypeMode == "enginegroup") or (menu.upgradetypeMode == "turretgroup") then
			plandata = menu.upgradeplan[upgradetype2.type][menu.currentSlot].macro
		else
			plandata = menu.upgradeplan[upgradetype.type][menu.contextData.slot]
		end
		local prefix = ""
		if upgradetype.mergeslots then
			prefix = #menu.slots[upgradetype.type] .. ReadText(1001, 42) .. " "
		end

		local hasmod, modicon = false, ""
		if (menu.upgradetypeMode == "enginegroup") or (menu.upgradetypeMode == "turretgroup") then
			hasmod, modicon = menu.checkMod(upgradetype2.grouptype, slotdata.currentcomponent)
		else
			hasmod, modicon = menu.checkMod(upgradetype.type, slotdata.component)
		end

		if (menu.upgradetypeMode == "enginegroup") or (menu.upgradetypeMode == "turretgroup") then
			local name = upgradetype2.text.default
			if plandata == "" then
				if slotdata.slotsize ~= "" then
					name = upgradetype2.text[slotdata.slotsize]
				end
			else
				name = GetMacroData(plandata, "name")
			end
			if not upgradetype2.mergeslots then
				local minselect = (plandata == "") and 0 or 1
				local maxselect = (plandata == "") and 0 or slotdata.total

				local scale = {
					min       = 0,
					minselect = minselect,
					max       = slotdata.total,
					maxselect = maxselect,
					start     = math.max(minselect, math.min(maxselect, menu.upgradeplan[upgradetype2.type][menu.currentSlot].count)),
					step      = 1,
					suffix    = "",
					exceedmax = false,
					readonly = hasmod or menu.isReadOnly,
				}

				-- handle already installed equipment
				local haslicence = menu.checkLicence(plandata)
				if (plandata == menu.groups[menu.currentSlot][upgradetype2.grouptype].currentmacro) and (not haslicence) then
					scale.maxselect = math.min(scale.maxselect, menu.groups[menu.currentSlot][upgradetype2.grouptype].count)
				end

				local mouseovertext = ""
				if hasmod then
					mouseovertext = "\27R" .. ReadText(1026, 8009) .. "\27X"
				end

				setup:addSimpleRow({
					Helper.createSliderCell(Helper.createTextInfo(name, "left", Helper.standardFont, Helper.standardFontSize, 255, 255, 255, 100), false, 0, 0, 0, Helper.standardTextHeight, nil, Helper.color.slidervalue, scale, mouseovertext)
				}, nil, {1})
			else
				setup:addSimpleRow({
					Helper.createFontString(name, false, "left", 255, 255, 255, 100)
				}, nil, {1})
			end
		end

		for k, macro in ipairs(slotdata.possiblemacros) do
			local name = prefix .. GetMacroData(macro, "name")

			local haslicence, icon, overridecolor, mouseovertext = menu.checkLicence(macro, true)
			local icondesc
			if icon and (icon ~= "") then
				icondesc = Helper.createButtonIcon(icon, "", overridecolor.r, overridecolor.g, overridecolor.b, overridecolor.a, Helper.standardTextHeight, Helper.standardTextHeight, width - Helper.standardTextHeight, 0)
			end

			-- handle already installed equipment
			if (macro == slotdata.currentmacro) and (not haslicence) then
				haslicence = true
				mouseovertext = mouseovertext .. "\n" .. "\27G" .. ReadText(1026, 8004)
			end

			if hasmod then
				mouseovertext = "\27R" .. ReadText(1026, 8009) .. "\27X\n" .. mouseovertext
			end

			mouseovertext = name .. "\n" .. mouseovertext

			local bgcolor = Helper.defaultButtonBackgroundColor
			if not haslicence then
				bgcolor = Helper.color.darkgrey
			end
				
			local color = Helper.color.white
			if (macro == slotdata.currentmacro) and (macro ~= plandata) then
				color = Helper.color.red
			elseif (macro == plandata) then
				color = Helper.color.green
				if hasmod then
					name = name .. " " .. modicon
				end
			end

			setup:addSimpleRow({
				Helper.createButton(Helper.createTextInfo(name, "left", Helper.standardFont, Helper.scaleFont(Helper.standardFont, Helper.standardFontSize), color.r, color.g, color.b, color.a, Helper.standardTextOffsetx), icondesc, false, (macro == plandata) or (not hasmod), 0, 0, width, Helper.scaleY(Helper.standardTextHeight), bgcolor, nil, nil, mouseovertext)
			}, nil, {1})
		end

		if upgradetype.allowempty then
			local name = ReadText(1001, 7906)

			local mouseovertext = ""
			if hasmod then
				mouseovertext = "\27R" .. ReadText(1026, 8009) .. "\27X"
			end

			local color = Helper.color.white
			if ("" == slotdata.currentmacro) and ("" ~= plandata) then
				color = Helper.color.red
			elseif ("" == plandata) then
				color = Helper.color.green
			end

			setup:addSimpleRow({
				Helper.createButton(Helper.createTextInfo(name, "left", Helper.standardFont, Helper.scaleFont(Helper.standardFont, Helper.standardFontSize), color.r, color.g, color.b, color.a, Helper.standardTextOffsetx), nil, true, not hasmod, 0, 0, width, Helper.scaleY(Helper.standardTextHeight), nil, nil, nil, mouseovertext)
			}, nil, {1})
		end
	end

	local contextdesc = setup:createCustomWidthTable({width}, false, true, true, 4, 0, menu.contextData.offsetX, menu.contextData.offsetY, 0, true, menu.topRows.context, menu.selectedRows.context, nil, nil, "column")
	menu.topRows.context = nil
	menu.selectedRows.context = nil
	menu.selectedCols.context = nil

	Helper.displayFrame(menu, {contextdesc}, false, "", "", { close = true }, nil, config.contextLayer)
end

function menu.setUpContextMenuScripts(uitable)
	local nooflines = 1

	if menu.contextMode == 1 then
		local upgradetype = Helper.findUpgradeType(menu.contextData.upgradetype)
		local upgradetype2 = Helper.findUpgradeTypeByGroupType(upgradetype.type)
		local slotdata
		if (menu.upgradetypeMode == "enginegroup") or (menu.upgradetypeMode == "turretgroup") then
			slotdata = menu.groups[menu.currentSlot][upgradetype2.grouptype]
		else
			slotdata = menu.slots[upgradetype.type][menu.contextData.slot]
		end

		if (menu.upgradetypeMode == "enginegroup") or (menu.upgradetypeMode == "turretgroup") then
			if not upgradetype.mergeslots then
				local line = nooflines
				Helper.setSliderCellScript(menu, nil, uitable, nooflines, 1, function (_, ...) return menu.slidercellSelectGroupAmount(upgradetype2.type, menu.currentSlot, nil, line, ...) end)
			end
			nooflines = nooflines + 1
		end

		for k, macro in ipairs(slotdata.possiblemacros) do
			local line = nooflines
			local haslicence = menu.checkLicence(macro)
			-- handle already installed equipment
			if (macro == slotdata.currentmacro) and (not haslicence) then
				haslicence = true
			end
			if haslicence then
				if (menu.upgradetypeMode == "enginegroup") or (menu.upgradetypeMode == "turretgroup") then
					Helper.setButtonScript(menu, nil, uitable, nooflines, 1, function () return menu.buttonSelectGroupUpgrade(upgradetype2.type, menu.currentSlot, macro, nil, nil, line) end)
				else
					Helper.setButtonScript(menu, nil, uitable, nooflines, 1, function () return menu.buttonSelectUpgradeMacro(upgradetype.type, menu.contextData.slot, macro, nil, nil, line) end)
				end
			end
			nooflines = nooflines + 1
		end

		if upgradetype.allowempty then
			local line = nooflines
			if (menu.upgradetypeMode == "enginegroup") or (menu.upgradetypeMode == "turretgroup") then
				Helper.setButtonScript(menu, nil, uitable, nooflines, 1, function () return menu.buttonSelectGroupUpgrade(upgradetype2.type, menu.currentSlot, "", nil, nil, line) end)
			else
				Helper.setButtonScript(menu, nil, uitable, nooflines, 1, function () return menu.buttonSelectUpgradeMacro(upgradetype.type, menu.contextData.slot, "", nil, nil, line) end)
			end
			nooflines = nooflines + 1
		end
	end
end

function menu.displayContextFrame(mode, width, x, y)
	PlaySound("ui_positive_click")
	menu.contextMode = { mode = mode, width = width, x = x, y = y }
	if mode == "saveLoadout" then
		menu.createLoadoutSaveContext()
	elseif mode == "equipment" then
		menu.createEquipmentContext()
	end
end

function menu.createEquipmentContext()
	Helper.removeAllWidgetScripts(menu, config.contextLayer)

	menu.contextFrame = Helper.createFrameHandle(menu, {
		layer = config.contextLayer,
		standardButtons = { close = true },
		backgroundID = "solid",
		backgroundColor = Helper.color.semitransparent,
		width = menu.contextMode.width,
		x = menu.contextMode.x,
		y = menu.contextMode.y,
		autoFrameHeight = true,
	})

	local ftable = menu.contextFrame:addTable(1, { tabOrder = 5, reserveScrollBar = false })

	local row = ftable:addRow(false, { fixed = true, bgColor = Helper.color.transparent })
	row[1]:createText(menu.selectedUpgrade.name, menu.subHeaderTextProperties)

	local row = ftable:addRow(true, { fixed = true, bgColor = Helper.color.transparent })
	row[1]:createButton({ active = true, bgColor = Helper.color.transparent }):setText(ReadText(1001, 2400), { color = Helper.color.white })
	row[1].handlers.onClick = function () return menu.buttonContextEncyclopedia(menu.selectedUpgrade) end

	menu.contextFrame:display()
end

function menu.checkLoadoutNameID()
	local canoverwrite = false
	local cansaveasnew = false
	if menu.loadout then
		local found = false
		for _, loadout in ipairs(menu.loadouts) do
			if loadout.id == menu.loadout then
				menu.loadoutName = loadout.name
				break
			end
		end
		if not found then
			menu.loadout = nil
		end
	end
	if (not menu.loadout) and menu.loadoutName and (menu.loadoutName ~= "") then
		cansaveasnew = true
		for _, loadout in ipairs(menu.loadouts) do
			if loadout.name == menu.loadoutName then
				canoverwrite = true
				cansaveasnew = false
				menu.loadout = loadout.id
				break
			end
		end
	end

	return canoverwrite, cansaveasnew
end

function menu.createLoadoutSaveContext()
	Helper.removeAllWidgetScripts(menu, config.contextLayer)

	menu.contextFrame = Helper.createFrameHandle(menu, {
		layer = config.contextLayer,
		standardButtons = { close = true },
		width = menu.contextMode.width,
		x = menu.contextMode.x,
		y = menu.contextMode.y,
		autoFrameHeight = true,
	})

	local ftable = menu.contextFrame:addTable(2, { tabOrder = 5, scaling = false, borderEnabled = false, reserveScrollBar = false })
	ftable:setDefaultCellProperties("button", { height = Helper.standardTextHeight })
	ftable:setDefaultComplexCellProperties("button", "text", { fontsize = Helper.standardFontSize, halign = "center" })

	local canoverwrite, cansaveasnew = menu.checkLoadoutNameID()

	local row = ftable:addRow(true, { fixed = true })
	row[1]:setColSpan(2):createEditBox({ height = menu.titleData.height }):setText(menu.loadoutName or "", { halign = "center", font = Helper.headerRow1Font, fontsize = Helper.scaleFont(Helper.headerRow1Font, Helper.headerRow1FontSize) })
	row[1].handlers.onTextChanged = menu.editboxLoadoutNameUpdateText

	row = ftable:addRow(true, { scaling = true, fixed = true })
	row[1]:createButton({ active = menu.checkLoadoutOverwriteActive, mouseOverText = ReadText(1026, 7908) }):setText(ReadText(1001, 7908), {  })
	row[1].handlers.onClick = function () return menu.buttonSave(true) end
	row[2]:createButton({ active = menu.checkLoadoutSaveNewActive, mouseOverText = ReadText(1026, 7909) }):setText(ReadText(1001, 7909), {  })
	row[2].handlers.onClick = function () return menu.buttonSave(false) end

	menu.contextFrame:display()
end

function menu.checkLoadoutOverwriteActive()
	local canoverwrite, cansaveasnew = menu.checkLoadoutNameID()
	return canoverwrite
end

function menu.checkLoadoutSaveNewActive()
	local canoverwrite, cansaveasnew = menu.checkLoadoutNameID()
	return cansaveasnew
end

function menu.viewCreated(layer, ...)
	if layer == config.mainLayer then
		menu.titlebartable, menu.map = ...

		if menu.activatemap == nil then
			menu.activatemap = true
		end
	elseif layer == config.infoLayer then
		if menu.upgradetypeMode == "paintmods" then
			menu.leftbartable, menu.buttontable, menu.slottable, menu.plantable = ...
		else
			menu.leftbartable, menu.slottable, menu.plantable = ...
		end

		menu.updateMoney = GetCurRealTime()
	elseif layer == config.contextLayer then
		menu.contexttable = ...

		menu.setUpContextMenuScripts(menu.contexttable)
	end

	-- clear descriptors again
	Helper.releaseDescriptors()
end

menu.updateInterval = 0.01

function menu.onUpdate()
	local curtime = GetCurRealTime()

	if menu.activatemap then
		-- pass relative screenspace of the holomap rendertarget to the holomap (value range = -1 .. 1)
		local renderX0, renderX1, renderY0, renderY1 = Helper.getRelativeRenderTargetSize(menu, config.mainLayer, menu.map)
		local rendertargetTexture = GetRenderTargetTexture(menu.map)
		if rendertargetTexture then
			menu.holomap = C.AddHoloMap(rendertargetTexture, renderX0, renderX1, renderY0, renderY1, menu.mapData.width / menu.mapData.height, 1)
			if ((menu.mode == "purchase") and (menu.macro ~= "")) or (((menu.mode == "upgrade") or (menu.mode == "modify")) and (menu.object ~= 0)) then
				if menu.holomap and (menu.holomap ~= 0) then
					menu.currentIdx = menu.currentIdx + 1
					Helper.callLoadoutFunction(menu.upgradeplan, nil, function (loadout, _) return C.ShowObjectConfigurationMap(menu.holomap, menu.object, 0, menu.macro, false, loadout) end)
					if menu.installedPaintMod then
						C.SetMapPaintMod(menu.holomap, menu.installedPaintMod.ware)
					end
					menu.selectMapMacroSlot()
					
					if menu.mapstate then
						C.SetMapState(menu.holomap, menu.mapstate)
						menu.mapstate = nil
					end
				end
			end

			menu.activatemap = false
		end
	end
	
	if menu.map and menu.holomap ~= 0 then
		local x, y = GetRenderTargetMousePosition(menu.map)
		C.SetMapRelativeMousePosition(menu.holomap, (x and y) ~= nil, x or 0, y or 0)
	end

	if menu.noupdate then
		return
	end

	menu.mainFrame:update()
	menu.infoFrame:update()
	if menu.contextFrame then
		menu.contextFrame:update()
	end

	if menu.updateMoney and (menu.updateMoney + 1 < curtime) then
		menu.updateMoney = curtime
		local currentplayermoney = GetPlayerMoney()
		if menu.shoppinglisttotal > currentplayermoney then
			if not menu.criticalerrors[1] then
				menu.displayMenu()
			end
		else
			if menu.criticalerrors[1] then
				menu.displayMenu()
			end
		end
	end
end

function menu.onRowChanged(row, rowdata, uitable)
end

function menu.onSelectElement()
end

-- rendertarget selections
function menu.onRenderTargetSelect()
	local offset = table.pack(GetLocalMousePosition())
	-- Check if the mouse button was down less than 0.2 seconds and the mouse was moved more than a distance of 2px
	if menu.leftdown and (menu.leftdown.time + 0.2 > GetCurRealTime()) and (not Helper.comparePositions(menu.leftdown.position, offset, 2)) then
		menu.closeContextMenu()
		if ((menu.mode == "purchase") and (menu.macro ~= "")) or ((menu.mode == "upgrade") and (menu.object ~= 0)) then
			local pickedslot = ffi.new("UILoadoutSlot")
			if C.GetPickedMapMacroSlot(menu.holomap, menu.object, 0, menu.macro, false, pickedslot) then
				local groupinfo = C.GetUpgradeSlotGroup(menu.object, menu.macro, pickedslot.upgradetype, pickedslot.slot)

				if (ffi.string(groupinfo.path) ~= "..") or (ffi.string(groupinfo.group) ~= "") then
					menu.upgradetypeMode, menu.currentSlot = menu.findGroupIndex(ffi.string(groupinfo.path), ffi.string(groupinfo.group))
				else
					menu.upgradetypeMode = ffi.string(pickedslot.upgradetype)
					menu.currentSlot = tonumber(pickedslot.slot)
				end
				menu.selectMapMacroSlot()
				menu.displayMenu(true)
			end
		elseif ((menu.mode == "modify") and (menu.object ~= 0)) then
			local pickedslot = ffi.new("UILoadoutSlot")
			if C.GetPickedMapMacroSlot(menu.holomap, menu.object, 0, menu.macro, false, pickedslot) then
				local mode = menu.getModUpgradeMode(ffi.string(pickedslot.upgradetype))
				if mode then
					menu.upgradetypeMode = mode
					menu.currentSlot = tonumber(pickedslot.slot)
					menu.selectMapMacroSlot()
					menu.displayMenu()
				end
			end
		end
	end

	menu.leftdown = nil
end

function menu.selectMapMacroSlot()
	if menu.holomap and (menu.holomap ~= 0) then
		if (menu.upgradetypeMode == "enginegroup") or (menu.upgradetypeMode == "turretgroup") then
			local group = menu.groups[menu.currentSlot]
			if group then
				C.SetSelectedMapGroup(menu.holomap, menu.object, menu.macro, group.path, group.group)
			end
		elseif menu.upgradetypeMode == "repair" then
			if menu.repairslots then
				local group = math.ceil(menu.currentSlot / 3)
				local loccol = menu.currentSlot - ((group - 1) * 3)

				if menu.repairslots[group] and menu.repairslots[group][loccol] then
					local component = menu.repairslots[group][loccol][4]
					local macro = menu.repairslots[group][loccol][2]
					local slot = menu.repairslots[group][loccol][1]

					for i, upgradetype in ipairs(Helper.upgradetypes) do
						if menu.slots[upgradetype.type] and menu.slots[upgradetype.type][slot] and menu.slots[upgradetype.type][slot].component == component then
							C.SetSelectedMapMacroSlot(menu.holomap, menu.object, 0, macro, false, upgradetype.type, slot)
							break
						end
					end

				else
					C.ClearSelectedMapMacroSlots(menu.holomap)
				end
			else
				C.ClearSelectedMapMacroSlots(menu.holomap)
			end
		elseif menu.mode == "modify" then
			local entry = menu.getLeftBarEntry(menu.upgradetypeMode)
			if (entry.upgrademode == "ship") or (entry.upgrademode == "paint") then
				C.ClearSelectedMapMacroSlots(menu.holomap)
			else
				local upgradetype = Helper.findUpgradeType(entry.upgrademode)
				if upgradetype.supertype == "macro" then
					if upgradetype.mergeslots then
						C.SetSelectedMapMacroSlot(menu.holomap, menu.object, 0, menu.macro, false, entry.upgrademode, 0)
					else
						C.SetSelectedMapMacroSlot(menu.holomap, menu.object, 0, menu.macro, false, entry.upgrademode, menu.currentSlot)
					end
				else
					C.ClearSelectedMapMacroSlots(menu.holomap)
				end
			end
		elseif menu.upgradetypeMode and (menu.upgradetypeMode ~= "consumables") and (menu.upgradetypeMode ~= "crew") then
			local upgradetype = Helper.findUpgradeType(menu.upgradetypeMode)
			if upgradetype.supertype == "macro" then
				if upgradetype.mergeslots then
					C.SetSelectedMapMacroSlot(menu.holomap, menu.object, 0, menu.macro, false, menu.upgradetypeMode, 0)
				else
					C.SetSelectedMapMacroSlot(menu.holomap, menu.object, 0, menu.macro, false, menu.upgradetypeMode, menu.currentSlot)
				end
			else
				C.ClearSelectedMapMacroSlots(menu.holomap)
			end
		else
			C.ClearSelectedMapMacroSlots(menu.holomap)
		end
	end
end

-- rendertarget mouse input helper
function menu.onRenderTargetMouseDown()
	C.StartPanMap(menu.holomap)
	menu.leftdown = { time = GetCurRealTime(), position = table.pack(GetLocalMousePosition()) }
end

function menu.onRenderTargetMouseUp()
	C.StopPanMap(menu.holomap)
end

function menu.onRenderTargetRightMouseDown()
	C.StartRotateMap(menu.holomap)
	menu.rightdown = { time = GetCurRealTime(), position = table.pack(GetLocalMousePosition()) }
end

function menu.onRenderTargetRightMouseUp()
	C.StopRotateMap(menu.holomap)
	if not menu.isReadOnly then
		local offset = table.pack(GetLocalMousePosition())
		-- Check if the mouse button was down less than 0.2 seconds and the mouse was moved more than a distance of 2px
		if (menu.rightdown.time + 0.2 > GetCurRealTime()) and (not Helper.comparePositions(menu.rightdown.position, offset, 2)) then
			menu.closeContextMenu()
			if ((menu.mode == "purchase") and (menu.macro ~= "")) or (menu.mode == "upgrade") or (menu.mode == "modify") then
				local pickedslot = ffi.new("UILoadoutSlot")
				if C.GetPickedMapMacroSlot(menu.holomap, menu.object, 0, menu.macro, false, pickedslot) then
					local groupinfo = C.GetUpgradeSlotGroup(menu.object, menu.macro, pickedslot.upgradetype, pickedslot.slot)
					if (ffi.string(groupinfo.path) ~= "..") or (ffi.string(groupinfo.group) ~= "") then
						menu.upgradetypeMode, menu.currentSlot = menu.findGroupIndex(ffi.string(groupinfo.path), ffi.string(groupinfo.group))
					else
						menu.upgradetypeMode = ffi.string(pickedslot.upgradetype)
						menu.currentSlot = tonumber(pickedslot.slot)
					end
					menu.selectMapMacroSlot()
					menu.displayMenu()

					menu.contextMode = 1
					menu.contextData = { offsetX = offset[1] + Helper.viewWidth / 2, offsetY = Helper.viewHeight / 2 - offset[2], upgradetype = ffi.string(pickedslot.upgradetype), slot = tonumber(pickedslot.slot) }
					menu.displayContextMenu()
				end
			end
		end
	end
	menu.rightdown = nil
end

function menu.onRenderTargetScrollDown()
	C.ZoomMap(menu.holomap, 1)
end

function menu.onRenderTargetScrollUp()
	C.ZoomMap(menu.holomap, -1)
end

function menu.onTableRightMouseClick(uitable, row, posx, posy)
	local rowdata = menu.rowDataMap[uitable] and menu.rowDataMap[uitable][row]
	if uitable == menu.slottable then
		if type(rowdata) == "table" then
			menu.selectedUpgrade = rowdata
			local x, y = GetLocalMousePosition()
			if x == nil then
				-- gamepad case
				x = posx
				y = -posy
			end
			menu.displayContextFrame("equipment", Helper.scaleX(200), x + Helper.viewWidth / 2, Helper.viewHeight / 2 - y)
		end
	end
end

function menu.onSaveState()
	local state = {}

	if menu.holomap ~= 0 then
		local mapstate = ffi.new("HoloMapState")
		C.GetMapState(menu.holomap, mapstate)
		state.map = { offset = { x = mapstate.offset.x, y = mapstate.offset.y, z = mapstate.offset.z, yaw = mapstate.offset.yaw, pitch = mapstate.offset.pitch, roll = mapstate.offset.roll,}, cameradistance = mapstate.cameradistance }
	end

	for _, key in ipairs(config.stateKeys) do
		state[key[1]] = menu[key[1]]
	end
	return state
end

function menu.onRestoreState(state)
	if state.map then
		local offset = ffi.new("UIPosRot", {
			x = state.map.offset.x, 
			y = state.map.offset.y, 
			z = state.map.offset.z, 
			yaw = state.map.offset.yaw, 
			pitch = state.map.offset.pitch, 
			roll = state.map.offset.roll
		})
		menu.mapstate = ffi.new("HoloMapState", {
			offset = offset, 
			cameradistance = state.map.cameradistance
		})
	end

	local upgradeplan, crew
	for _, key in ipairs(config.stateKeys) do
		if key[1] == "upgradeplan" then
			upgradeplan = state[key[1]]
		elseif key[1] == "crew" then
			crew = state[key[1]]
		else
			if key[2] == "UniverseID" then
				menu[key[1]] = ffi.new("UniverseID", ConvertIDTo64Bit(state[key[1]]))
			elseif key[2] == "bool" then
				menu[key[1]] = state[key[1]] ~= 0
			else
				menu[key[1]] = state[key[1]]
			end
		end
	end

	return upgradeplan, crew
end

function menu.closeMenu(dueToClose)
	Helper.closeMenu(menu, dueToClose)
	menu.cleanup()
end

function menu.onCloseElement(dueToClose)
	if menu.contextMode then
		menu.closeContextMenu()
		return
	end

	if menu.upgradetypeMode and (dueToClose == "back") and ((menu.mode ~= "modify") or (not menu.modeparam[1])) then
		menu.deactivateUpgradeMode()
		return
	end

	menu.closeMenu(dueToClose)
end

function menu.closeContextMenu()
	Helper.clearFrame(menu, config.contextLayer)
	menu.contextFrame = nil
	menu.contextMode = nil
end

function menu.getClassText(class)
	if class == "ship_xl" then
		return ReadText(1001, 48)
	elseif class == "ship_l" then
		return ReadText(1001, 49)
	elseif class == "ship_m" then
		return ReadText(1001, 50)
	elseif class == "ship_s" then
		return ReadText(1001, 51)
	elseif class == "ship_xs" then
		return ReadText(1001, 52)
	end

	return ""
end

function menu.getAmmoUsage(type)
	local capacity = 0
	if type == "missile" then
		if menu.mode == "purchase" then
			if menu.macro ~= "" then
				capacity = C.GetDefaultMissileStorageCapacity(menu.macro)
			end
		elseif menu.mode == "upgrade" then
			if menu.object ~= 0 then
				capacity = GetComponentData(ConvertStringTo64Bit(tostring(menu.object)), "missilecapacity")
			end
		end
		for slot, macro in pairs(menu.upgradeplan.weapon) do
			local currentmacro = menu.slots.weapon[slot].currentmacro
			if currentmacro ~= macro then
				if currentmacro ~= "" then
					capacity = capacity - C.GetMacroMissileCapacity(currentmacro)
				end
				if macro ~= "" then
					capacity = capacity + C.GetMacroMissileCapacity(macro)
				end
			end
		end
		for slot, macro in pairs(menu.upgradeplan.turret) do
			local currentmacro = menu.slots.turret[slot].currentmacro
			if currentmacro ~= macro then
				if currentmacro ~= "" then
					capacity = capacity - C.GetMacroMissileCapacity(currentmacro)
				end
				if macro ~= "" then
					capacity = capacity + C.GetMacroMissileCapacity(macro)
				end
			end
		end
		for slot, groupdata in pairs(menu.upgradeplan.turretgroup) do
			local currentmacro = menu.groups[slot].turret.currentmacro
			if currentmacro ~= groupdata.macro then
				if currentmacro ~= "" then
					capacity = capacity - groupdata.count * C.GetMacroMissileCapacity(currentmacro)
				end
				if groupdata.macro ~= "" then
					capacity = capacity + groupdata.count * C.GetMacroMissileCapacity(groupdata.macro)
				end
			end
		end
	elseif type == "drone" then
		if menu.mode == "purchase" then
			if menu.macro ~= "" then
				capacity = GetMacroUnitStorageCapacity(menu.macro)
			end
		elseif menu.mode == "upgrade" then
			if menu.object ~= 0 then
				capacity = GetUnitStorageData(ConvertStringTo64Bit(tostring(menu.object))).capacity
			end
		end
	elseif type == "deployable" then
		if menu.mode == "purchase" then
			if menu.macro ~= "" then
				capacity = C.GetMacroDeployableCapacity(menu.macro)
			end
		elseif menu.mode == "upgrade" then
			if menu.object ~= 0 then
				capacity = C.GetDefensibleDeployableCapacity(ConvertStringTo64Bit(tostring(menu.object)))
			end
		end
	elseif type == "countermeasure" then
		if menu.mode == "purchase" then
			if menu.macro ~= "" then
				capacity = C.GetDefaultCountermeasureStorageCapacity(menu.macro)
			end
		elseif menu.mode == "upgrade" then
			if menu.object ~= 0 then
				capacity = GetComponentData(ConvertStringTo64Bit(tostring(menu.object)), "countermeasurecapacity")
			end
		end
	end

	local total = 0
	for macro, amount in pairs(menu.upgradeplan[type]) do
		local volume = 1
		if type == "missile" then
			local ware = menu.upgradewares[type][menu.findUpgradeMacro(type, macro)]
			if ware.ware then
				volume = GetWareData(ware.ware, "volume")
			end
		end
		total = total + amount * volume
	end

	return total, capacity
end

function menu.isAmmoCompatible(type, ammomacro)
	if ammomacro ~= "" then
		if type == "missile" then
			for slot, macro in pairs(menu.upgradeplan.weapon) do
				if macro ~= "" then
					if C.IsAmmoMacroCompatible(macro, ammomacro) then
						return true
					end
				end
			end
			for slot, macro in pairs(menu.upgradeplan.turret) do
				if macro ~= "" then
					if C.IsAmmoMacroCompatible(macro, ammomacro) then
						return true
					end
				end
			end
			for slot, groupdata in pairs(menu.upgradeplan.turretgroup) do
				if groupdata.macro ~= "" then
					if C.IsAmmoMacroCompatible(groupdata.macro, ammomacro) then
						return true
					end
				end
			end
		elseif type == "drone" then
			return C.IsUnitMacroCompatible(menu.object, menu.macro, ammomacro)
		elseif type == "deployable" then
			return C.IsDeployableMacroCompatible(menu.object, menu.macro, ammomacro)
		elseif type == "countermeasure" then
			if menu.macro ~= "" then
				return (C.GetDefaultCountermeasureStorageCapacity(menu.macro) > 0)
			elseif menu.object ~= 0 then
				return (GetComponentData(ConvertStringTo64Bit(tostring(menu.object)), "countermeasurecapacity") > 0)
			end
		end
	end

	return false
end

function menu.filterUpgradeByText(upgrade, text)
	text = utf8.lower(text)

	if string.find(utf8.lower(upgrade), text, 1, true) then
		return true
	end

	return false
end

function menu.findGroupIndex(path, group)
	for i, groupinfo in ipairs(menu.groups) do
		if (groupinfo.path == path) and (groupinfo.group == group) then
			local mode = "turretgroup"
			if groupinfo["engine"].total > 0 then
				mode = "enginegroup"
			end
			return mode, i
		end
	end
end

function menu.getLeftBarEntry(mode)
	local leftBar = config.leftBar
	if menu.mode == "modify" then
		leftBar = config.leftBarMods
	end

	for i, entry in ipairs(leftBar) do
		if entry.mode == mode then
			return entry
		end
	end

	return {}
end

function menu.prepareComponentUpgradeSlots(object)
	-- for all members of set upgradetypes,
	for i, upgradetype in ipairs(Helper.upgradetypes) do
		-- with supertype "macro" (there should be 4)
		if upgradetype.supertype == "macro" then
			-- initialize an entry in table menu.slots with key upgradetype.type
			menu.slots[upgradetype.type] = {}
			-- and for all slots in the object,
			for j = 1, tonumber(C.GetNumUpgradeSlots(object, "", upgradetype.type)) do
				local groupinfo = C.GetUpgradeSlotGroup(object, "", upgradetype.type, j)
				if upgradetype.pseudogroup or ((ffi.string(groupinfo.path) == "..") and (ffi.string(groupinfo.group) == "")) then
					menu.slots[upgradetype.type][j] = { currentmacro = ffi.string(C.GetUpgradeSlotCurrentMacro(object, 0, upgradetype.type, j)), possiblemacros = {}, component = nil }
					menu.upgradeplan[upgradetype.type][j] = menu.slots[upgradetype.type][j].currentmacro
				else
					menu.slots[upgradetype.type][j] = { currentmacro = "", possiblemacros = {}, component = nil }
					menu.upgradeplan[upgradetype.type][j] = ""
				end

				local currentcomponent = C.GetUpgradeSlotCurrentComponent(object, upgradetype.type, j)
				if currentcomponent ~= 0 then
					menu.slots[upgradetype.type][j].component = currentcomponent
				end
			end
		elseif upgradetype.supertype == "ammo" then
			menu.ammo[upgradetype.type] = {}

			local ammo = {}
			if upgradetype.type == "missile" then
				local n = C.GetNumAllMissiles(object)
				local buf = ffi.new("AmmoData[?]", n)
				n = C.GetAllMissiles(buf, n, object)
				for j = 0, n - 1 do
					local entry = {}
					entry.macro = ffi.string(buf[j].macro)
					entry.amount = buf[j].amount
					table.insert(ammo, entry)
				end
			elseif upgradetype.type == "drone" then
				local n = C.GetNumAllUnits(object, false)
				local buf = ffi.new("UnitData[?]", n)
				n = C.GetAllUnits(buf, n, object, false)
				for j = 0, n - 1 do
					local entry = {}
					entry.macro = ffi.string(buf[j].macro)
					entry.category = ffi.string(buf[j].category)
					entry.amount = buf[j].amount
					table.insert(ammo, entry)
				end
			elseif upgradetype.type == "deployable" then
				local numlasertowers = C.GetNumAllLaserTowers(object)
				local lasertowers = ffi.new("AmmoData[?]", numlasertowers)
				numlasertowers = C.GetAllLaserTowers(lasertowers, numlasertowers, object)
				for j = 0, numlasertowers - 1 do
					local entry = {}
					entry.macro = ffi.string(lasertowers[j].macro)
					entry.amount = lasertowers[j].amount
					table.insert(ammo, entry)
				end

				local numsatellites = C.GetNumAllSatellites(object)
				local satellites = ffi.new("AmmoData[?]", numsatellites)
				numsatellites = C.GetAllSatellites(satellites, numsatellites, object)
				for j = 0, numsatellites - 1 do
					local entry = {}
					entry.macro = ffi.string(satellites[j].macro)
					entry.amount = satellites[j].amount
					table.insert(ammo, entry)
				end

				local nummines = C.GetNumAllMines(object)
				local mines = ffi.new("AmmoData[?]", nummines)
				nummines = C.GetAllMines(mines, nummines, object)
				for j = 0, nummines - 1 do
					local entry = {}
					entry.macro = ffi.string(mines[j].macro)
					entry.amount = mines[j].amount
					table.insert(ammo, entry)
				end

				local numnavbeacons = C.GetNumAllNavBeacons(object)
				local navbeacons = ffi.new("AmmoData[?]", numnavbeacons)
				numnavbeacons = C.GetAllNavBeacons(navbeacons, numnavbeacons, object)
				for j = 0, numnavbeacons - 1 do
					local entry = {}
					entry.macro = ffi.string(navbeacons[j].macro)
					entry.amount = navbeacons[j].amount
					table.insert(ammo, entry)
				end

				local numresourceprobes = C.GetNumAllResourceProbes(object)
				local resourceprobes = ffi.new("AmmoData[?]", numresourceprobes)
				numresourceprobes = C.GetAllResourceProbes(resourceprobes, numresourceprobes, object)
				for j = 0, numresourceprobes - 1 do
					local entry = {}
					entry.macro = ffi.string(resourceprobes[j].macro)
					entry.amount = resourceprobes[j].amount
					table.insert(ammo, entry)
				end
			elseif upgradetype.type == "countermeasure" then
				local n = C.GetNumAllCountermeasures(object)
				local buf = ffi.new("AmmoData[?]", n)
				n = C.GetAllCountermeasures(buf, n, object)
				local totalnumcountermeasures = 0
				for j = 0, n - 1 do
					local entry = {}
					entry.macro = ffi.string(buf[j].macro)
					entry.amount = buf[j].amount
					table.insert(ammo, entry)
				end
			end

			for _, item in ipairs(ammo) do
				local isexcluded = false
				for _, exclusion in ipairs(upgradetype.exclude) do
					if item.category == exclusion then
						isexcluded = true
						break
					end
				end
				if not isexcluded then
					menu.ammo[upgradetype.type][item.macro] = item.amount
				end
				menu.upgradeplan[upgradetype.type][item.macro] = item.amount
			end
		elseif upgradetype.supertype == "software" then
			menu.software[upgradetype.type] = {}
			local n = C.GetNumSoftwareSlots(object, "")
			local buf = ffi.new("SoftwareSlot[?]", n)
			n = C.GetSoftwareSlots(buf, n, object, "")
			for j = 0, n - 1 do
				local entry = {}
				entry.maxsoftware = ffi.string(buf[j].max)
				entry.currentsoftware = ffi.string(buf[j].current)
				table.insert(menu.upgradeplan[upgradetype.type], entry.currentsoftware)
				table.insert(menu.software[upgradetype.type], entry)
			end
		elseif upgradetype.supertype == "virtualmacro" then
			menu.slots[upgradetype.type] = {}
			for j = 1, tonumber(C.GetNumVirtualUpgradeSlots(object, "", upgradetype.type)) do
				-- convert index from lua to C-style
				menu.slots[upgradetype.type][j] = { currentmacro = ffi.string(C.GetVirtualUpgradeSlotCurrentMacro(object, upgradetype.type, j)), possiblemacros = {} }
				menu.upgradeplan[upgradetype.type][j] = menu.slots[upgradetype.type][j].currentmacro
			end
		end
	end

	-- add installed components in menu.upgradewares
	for type, slots in pairs(menu.slots) do
		for _, slot in ipairs(slots) do
			if slot.currentmacro and (slot.currentmacro ~= "") then
				local i = menu.findUpgradeMacro(type, slot.currentmacro)
				if i then
					menu.upgradewares[type][i].objectamount = menu.upgradewares[type][i].objectamount + 1
				else
					table.insert(menu.upgradewares[type], { ware = GetMacroData(slot.currentmacro, "ware"), macro = slot.currentmacro, objectamount = 1, isFromShipyard = false })
				end
			end
		end
	end

	-- add installed ammo in menu.upgradewares
	for type, ammo in pairs(menu.ammo) do
		for macro, amount in pairs(ammo) do
			local i = menu.findUpgradeMacro(type, macro)
			if i then
				menu.upgradewares[type][i].objectamount = menu.upgradewares[type][i].objectamount + amount
			else
				table.insert(menu.upgradewares[type], { ware = GetMacroData(macro, "ware"), macro = macro, objectamount = amount, isFromShipyard = false })
			end
		end
	end
end

function menu.prepareMacroUpgradeSlots(macro)
	for i, upgradetype in ipairs(Helper.upgradetypes) do
		if upgradetype.supertype == "macro" then
			menu.slots[upgradetype.type] = {}
			for j = 1, tonumber(C.GetNumUpgradeSlots(0, macro, upgradetype.type)) do
				-- convert index from lua to C-style
				menu.slots[upgradetype.type][j] = { currentmacro = "", possiblemacros = {} }
				menu.upgradeplan[upgradetype.type][j] = ""
			end
		elseif upgradetype.supertype == "ammo" then
			menu.ammo[upgradetype.type] = {}
		elseif upgradetype.supertype == "software" then
			menu.software[upgradetype.type] = {}
			local n = C.GetNumSoftwareSlots(0, macro)
			local buf = ffi.new("SoftwareSlot[?]", n)
			n = C.GetSoftwareSlots(buf, n, 0, macro)
			for j = 0, n - 1 do
				local entry = {}
				entry.maxsoftware = ffi.string(buf[j].max)
				entry.currentsoftware = ffi.string(buf[j].current)
				table.insert(menu.upgradeplan[upgradetype.type], entry.currentsoftware)
				table.insert(menu.software[upgradetype.type], entry)
			end
		elseif upgradetype.supertype == "virtualmacro" then
			menu.slots[upgradetype.type] = {}
			for j = 1, tonumber(C.GetNumVirtualUpgradeSlots(0, macro, upgradetype.type)) do
				-- convert index from lua to C-style
				menu.slots[upgradetype.type][j] = { currentmacro = "", possiblemacros = {} }
				menu.upgradeplan[upgradetype.type][j] = ""
			end
		end
	end
end

function menu.prepareComponentCrewInfo(object)
	local n = C.GetNumAllRoles()
	local buf = ffi.new("PeopleInfo[?]", n)
	n = C.GetPeople(buf, n, object)
	local numhireable = 0
	for i = 0, n - 1 do
		if buf[i].canhire then
			numhireable = numhireable + 1
			menu.crew.roles[numhireable] = { id = ffi.string(buf[i].id), name = ffi.string(buf[i].name), desc = ffi.string(buf[i].desc), total = buf[i].amount, wanted = buf[i].amount, tiers = {}, canhire = buf[i].canhire }
			menu.crew.total = menu.crew.total + buf[i].amount
		
			local numtiers = buf[i].numtiers
			local buf2 = ffi.new("RoleTierData[?]", numtiers)
			numtiers = C.GetRoleTiers(buf2, numtiers, object, menu.crew.roles[numhireable].id)
			for j = 0, numtiers - 1 do
				menu.crew.roles[numhireable].tiers[j + 1] = { skilllevel = buf2[j].skilllevel, name = ffi.string(buf2[j].name), total = buf2[j].amount, wanted = buf2[j].amount, npcs = {}, currentnpcs = {} }

				local numnpcs = buf2[j].amount
				local buf3 = ffi.new("NPCSeed[?]", numnpcs)
				numnpcs = C.GetRoleTierNPCs(buf3, numnpcs, object, menu.crew.roles[numhireable].id, menu.crew.roles[numhireable].tiers[j + 1].skilllevel)
				for k = 0, numnpcs - 1 do
					table.insert(menu.crew.roles[numhireable].tiers[j + 1].npcs, buf3[k])
					table.insert(menu.crew.roles[numhireable].tiers[j + 1].currentnpcs, buf3[k])
				end
			end
			if numtiers == 0 then
				menu.crew.roles[numhireable].tiers[1] = { skilllevel = 0, hidden = true, total = buf[i].amount, wanted = buf[i].amount, npcs = {}, currentnpcs = {} }
				local numnpcs = buf[i].amount
				local buf3 = ffi.new("NPCSeed[?]", numnpcs)
				numnpcs = C.GetRoleTierNPCs(buf3, numnpcs, object, menu.crew.roles[numhireable].id, 0)
				for k = 0, numnpcs - 1 do
					table.insert(menu.crew.roles[numhireable].tiers[1].npcs, buf3[k])
					table.insert(menu.crew.roles[numhireable].tiers[1].currentnpcs, buf3[k])
				end
			end
		end
	end

	menu.crew.capacity = C.GetPeopleCapacity(menu.object, menu.macro, false)
end

function menu.prepareMacroCrewInfo(macro)
	local n = C.GetNumAllRoles()
	local buf = ffi.new("PeopleInfo[?]", n)
	n = C.GetAllRoles(buf, n)
	local numhireable = 0
	for i = 0, n - 1 do
		if buf[i].canhire then
			numhireable = numhireable + 1
			menu.crew.roles[numhireable] = { id = ffi.string(buf[i].id), name = ffi.string(buf[i].name), desc = ffi.string(buf[i].desc), total = buf[i].amount, wanted = buf[i].amount, tiers = {}, canhire = buf[i].canhire }
			menu.crew.roles[numhireable].tiers[1] = { skilllevel = 0, hidden = true, total = buf[i].amount, wanted = buf[i].amount, npcs = {}, currentnpcs = {} }
			menu.crew.total = menu.crew.total + buf[i].amount
		end
	end

	menu.crew.capacity = C.GetPeopleCapacity(menu.object, menu.macro, false)
end

function menu.prepareModWares()
	menu.inventory = GetPlayerInventory()

	menu.modwares = {}
	local n = C.GetNumAvailableEquipmentMods()
	local buf = ffi.new("UIEquipmentMod[?]", n)
	n = C.GetAvailableEquipmentMods(buf, n)
	for i = 0, n - 1 do
		local entry = {}
		entry.ware = ffi.string(buf[i].Ware)

		local modclass, modquality, rawresources = GetWareData(entry.ware, "modclass", "modquality", "resources")
		entry.quality = modquality

		entry.resources = {}
		for _, resource in ipairs(rawresources or {}) do
			local resourcedata = menu.inventory[resource.ware]
			if resourcedata then
				local isprimarymodpart = GetWareData(resource.ware, "isprimarymodpart")
				local maxcraftable = math.floor(resourcedata.amount / resource.amount)
				entry.craftableamount = entry.craftableamount and math.min(maxcraftable, entry.craftableamount) or maxcraftable
				table.insert(entry.resources, isprimarymodpart and 1 or (#entry.resources + 1), { ware = resource.ware, data = { name = resourcedata.name, amount = resourcedata.amount, price = resourcedata.price, needed = resource.amount } })
			else
				local resourcename, resourcebuyprice, isprimarymodpart = GetWareData(resource.ware, "name", "buyprice", "isprimarymodpart")
				entry.craftableamount = 0
				table.insert(entry.resources, isprimarymodpart and 1 or (#entry.resources + 1), { ware = resource.ware, data = { name = resourcename, amount = 0, price = resourcebuyprice, needed = resource.amount } })
			end
		end

		if menu.modwares[modclass] then
			table.insert(menu.modwares[modclass], entry)
		else
			menu.modwares[modclass] = { entry }
		end
	end

	n = C.GetNumInventoryPaintMods()
	buf = ffi.new("UIPaintMod[?]", n)
	n = C.GetInventoryPaintMods(buf, n);
	for i = 0, n - 1 do
		local entry = {}
		entry.name = ffi.string(buf[i].Name)
		entry.ware = ffi.string(buf[i].Ware)
		entry.quality = buf[i].Quality
		entry.amount = buf[i].Amount

		if menu.modwares["paint"] then
			table.insert(menu.modwares["paint"], entry)
		else
			menu.modwares["paint"] = { entry }
		end
	end
end

function menu.determineNeededRepairs(ship)
	local damagedcomponents = {}

	Helper.ffiVLA(damagedcomponents, "UniverseID", C.GetNumSubComponents, C.GetDamagedSubComponents, ship)

	local hullpercent = GetComponentData(ConvertStringToLuaID(tostring(ship)), "hullpercent")
	if hullpercent < 100 then
		table.insert(damagedcomponents, 1, ship)
	end

	return damagedcomponents
end

function menu.findWareIdx(array, ware)
	for i, v in ipairs(array) do
		if v.ware == ware then
			return i
		end
	end
end

function menu.insertWare(array, category, ware, count, pricetype)
	local array2 = array
	if category then
		array[category] = array[category] or {}
		array2 = array[category]
	end
	local i = menu.findWareIdx(array2, ware)
	if i then
		array2[i].amount = array2[i].amount + count
	else
		local price
		if pricetype == "normal" then
			price = tonumber(C.GetBuildWarePrice(menu.container, ware))
		elseif pricetype == "software" then
			price = GetContainerWarePrice(ConvertStringToLuaID(tostring(menu.container)), ware, false)
		elseif pricetype == "crew" then
			local minprice, maxprice = GetWareData(ware, "minprice", "maxprice")
			price = maxprice - menu.crew.availableworkforce / menu.crew.maxavailableworkforce * (maxprice - minprice)
		end
		table.insert(array2, { ware = ware, amount = count, price = price })
	end
end

function menu.insertComponent(array, component, pricetype)
	local price
	if pricetype == "normal" then
		price = tonumber(C.GetRepairPrice(ConvertStringTo64Bit(component), menu.container))
	end
	table.insert(array, { component = component, price = price })
end

function menu.getLoadoutSummary(upgradeplan, crew, repairplan)
	local wareAmounts = {}

	for i, upgradetype in ipairs(Helper.upgradetypes) do
		local slots = upgradeplan[upgradetype.type]
		local first = true
		for slot, macro in pairs(slots) do
			if first or (not upgradetype.mergeslots) then
				first = false
				if upgradetype.supertype == "group" then
					local data = macro
					if data.macro ~= "" then
						local i = menu.findUpgradeMacro(upgradetype.grouptype, data.macro)
						if not i then
							break
						end
						local upgradeware = menu.upgradewares[upgradetype.grouptype][i]
						menu.insertWare(wareAmounts, nil, upgradeware.ware, (upgradetype.mergeslots and #slots or data.count))
					end
				elseif (upgradetype.supertype == "macro") or (upgradetype.supertype == "virtualmacro") then
					if macro ~= "" then
						local i = menu.findUpgradeMacro(upgradetype.type, macro)
						if not i then
							break
						end
						local upgradeware = menu.upgradewares[upgradetype.type][i]
						menu.insertWare(wareAmounts, nil, upgradeware.ware, (upgradetype.mergeslots and #slots or 1))
					end
				elseif upgradetype.supertype == "ammo" then
					local new = macro
					local macro = slot
					if new > 0 then
						local j = menu.findUpgradeMacro(upgradetype.type, macro)
						if not j then
							break
						end
						local upgradeware = menu.upgradewares[upgradetype.type][j]
						menu.insertWare(wareAmounts, nil, upgradeware.ware, new)
					end
				elseif upgradetype.supertype == "software" then
					local newware = macro
					if newware ~= "" then
						menu.insertWare(wareAmounts, nil, newware, 1)
					end
				end
			end
		end
	end

	-- Crew
	if (crew.total + crew.hired - #crew.fired) > 0 then
		menu.insertWare(wareAmounts, nil, crew.ware, crew.total + crew.hired - #crew.fired)
	end

	local summary = ReadText(1001, 7935) .. ReadText(1001, 120)
	for _, entry in ipairs(wareAmounts) do
		summary = summary .. "\n" .. entry.amount .. ReadText(1001, 42) .. " " .. GetWareData(entry.ware, "name")
	end

	-- Repair
	if repairplan then
		for componentidstring in pairs(repairplan) do
			if componentidstring ~= "processed" then
				summary = summary .. "\n" .. ReadText(1001, 4217) .. ReadText(1001, 120) .. " " .. ffi.string(C.GetComponentName(ConvertStringTo64Bit(componentidstring))) .. " (" .. (100 - GetComponentData(ConvertStringTo64Bit(componentidstring), "hullpercent")) .. "% " .. ReadText(1001, 1) .. ")"
			end
		end
	end

	return summary
end

function menu.getModUpgradeMode(upgradetype)
	for i, entry in ipairs(config.leftBarMods) do
		if entry.upgrademode == upgradetype then
			return entry.mode
		end
	end
end

function menu.sortAmmo(a, b)
	local atype, btype = "", ""
	if IsMacroClass(a, "satellite") then
		atype = "satellite"
	elseif IsMacroClass(a, "navbeacon") then
		atype = "navbeacon"
	elseif IsMacroClass(a, "resourceprobe") then
		atype = "resourceprobe"
	elseif IsMacroClass(a, "mine") then
		atype = "mine"
	elseif GetMacroData(a, "islasertower") then
		atype = "lasertower"
	end
	if IsMacroClass(b, "satellite") then
		btype = "satellite"
	elseif IsMacroClass(b, "navbeacon") then
		btype = "navbeacon"
	elseif IsMacroClass(b, "resourceprobe") then
		btype = "resourceprobe"
	elseif IsMacroClass(b, "mine") then
		btype = "mine"
	elseif GetMacroData(b, "islasertower") then
		btype = "lasertower"
	end

	if atype == btype then
		return Helper.sortMacroName(a, b)
	end
	return config.deployableOrder[atype] < config.deployableOrder[btype]
end


init()
